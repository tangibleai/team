# 2023-01-26 All Hands

## Hobson
- rose: Rori is going very well and we are adding new features to their plan
- bud: Maria coming back soon
- thorn: MOIA launch delayed

## Vlad
- Django - corey Schafer courses and fix bugs in lannister project (slackbot)
- Psycopg2 for Postgresql then supabase

## Maria
- thorn: medical leave
- rose: visiting Isreal, friends and family like Maya
- bud: slowly returning to some projects - see Meh-high-lah

## Rumman
- thorn: rough days personal life
- bud: figured out issues by tomorrow
- rose: started on tasks on nudger+faceofqary and found some bugs, websock

## Vish
- rose: vacation! recover from stress
- bud: Rori project starting today (entity extraction ideas)
- thorn: time zone: rural India (next week solved)

## Mikhaylo
- rose: conversation design internship presentation (Motty chatbot)
- bud: http API requests
- thorn: Sophia, Bulgaria

### Landbot
https://landbot.pro/v3/H-1411903-1V68PT23MKWPH2IS/index.html

### Telegram
https://t.me/My_Motty_bot


## Cetin
- rose: object storage on DO spaces
- bud: mongo
- thorn: Django

## Project Statuses

- Rori: Greg demonstrated NLU and sentiment analysis integrated with TurnIO Stacks
- MOIA: Sarah pentesting this month using Black Hat Python
- Nurse Nisa: Dashboard ready for DRC, more work to do for Kenya
- SFCG: Prototype has been user tested, new flows and activites being developed over the next few weeks.
