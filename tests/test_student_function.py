import os
import unittest
import pandas as pd

from .mathtext import text2int


STUDENT = os.path.dirname(__file__)

FILE = "data/test_text2int.all.csv"


class TestStringMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(FILE)

    @staticmethod
    def get_text2int(text):
        """Makes a post request to the endpoint"""
        r = None
        try:
            r = text2int(text)
        except:
            pass
        return r

    def test_acc_score_text2int(self):
        """Calculates accuracy score for endpoint"""

        self.df["text2int"] = self.df["input"].apply(func=self.get_text2int)
        self.df["score"] = self.df[["output", "text2int"]].apply(
            lambda row: row[0] == row[1],
            axis=1
        )

        correct = self.df["score"][self.df["score"] == True].count()
        incorrect = self.df["score"][self.df["score"] != True].count()
        total = self.df["score"].count()
        acc_score = round(correct/total, 2)



        self.df.to_csv(f"{STUDENT}/text2int_result.csv", index=False)
        with open(file=f"{STUDENT}/text2int_score.txt", mode="w") as file:
            file.write(f"Correct: {correct}\n")
            file.write(f"Incorrect: {incorrect}\n")
            file.write(f"Total: {total}\n")
            file.write(f"Acc score: {acc_score}\n")

        self.assertGreaterEqual(acc_score, 0.8, f"Accuracy score: '{acc_score}'. Value is too low!")


if __name__ == '__main__':
    unittest.main()
