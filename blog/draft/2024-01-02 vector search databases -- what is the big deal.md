# vector search databases

You've probably seen a lot of big deal anouncements for startups getting funding to build vector search database products.
Vector search databases are what you need to build a search engine for vectors, typically AI embedding vectors.
Think of a search engine that knows what word means and can find the perfect semantic match for your query, based on the natural language text on a web page.
That's called semantic search.
As you might imagine, semantic search is indeed a big deal.
But why is it such a hard problem mathematically.
You can find exact or fuzzy _string_ or _text_ matches in milliseconds on any popular search engine.
So what's the big deal?
Well, it turns out that searching dense vectors is exponentially hard (NP-hard) in the number of dimensions. 

To see why, start with some low dimensional dense vectors that you are already familiar with.
Imagine the 2-D vector for the latitiude and longitude of your current location on Open Street Maps (OSM).
Now imagine the OSM database of all the coffee shops in the world.
Now you want to find a cafe near your latitude and longitude so you don't have to walk too far to get coffee.
If you search OSM for all the cafe's nearby, it creates a _bounding box_ around your location and can quickly do the four binary comparisons of your location vector to _ALL_ of the vectors for cafes.
In Pandas this would happen in milliseconds.
So first create a million cafes and randomly scatter them around the globe:

```python
>>> df = np.random.rand(1_000_000, 2)  # values between 0 and 1
>>> df = pd.DataFrame(df,
...     columns=['lat', 'lon'])
>>> df
             lat       lon
0       0.007136  0.556329
1       0.369542  0.745079
...          ...       ...
999998  0.267349  0.914248
999999  0.986494  0.079663
>>> df = df * 2 - 1  # values between -1 and +1
>>> df['lat'] *= 90  # lat between -90 and +90
>>> df['lon'] *= 180 # lon between -180 and +180
>>> df
              lat         lon
0      -88.715433   20.278508
1      -23.482420   88.228473
...           ...         ...
999998 -41.877221  149.129388
999999  87.568872 -151.321429
```

Now imagine you have a GPS and it says that you are near at 33 deg latitude and -117 deg longitude (near Balboa Park in San Diego, California, USA). 

```python
>>> me = pd.Series([33., -117.], index=['lat', 'lon'])
# San Diego is 32.715736, -117.161087



```python
>>> r = .2
>>> df[(df.lat > me.lat-r) & (df.lat < me.lat+r) & (df.lon > me.lon-r) & (df.lon < me.lon+r)]
              lat         lon
99113   23.934082 -123.082979
330592  24.161822 -122.806391
560283  23.852930 -123.190718
951486  23.817220 -122.985195
993210  24.160510 -123.145876

```
search

### Commercial Vector DB products

In the past two years, since ChatGPT's release, there has been an explosion in startups focused on delivering managed vector search database services.

- Pinecone (OG)
- Postgres VectorField
- Astra DB (serverless/managed Casandra)
- Atlas (serverless/managed MongoDB) 

### References
- [RAG quickstart (LangChain + Mongo)](https://www.mongodb.com/developer/products/atlas/boosting-ai-build-chatbot-data-mongodb-atlas-vector-search-langchain-templates-using-rag-pattern/) - Serverless is $1/10M queries, shared hosting is free for ~5GB storage
- [Ragstack (LangChain + AstraDB)](https://pypi.org/project/ragstack-ai/) - Astra DB (serverless Cassandra), free 20M queries/mo
- [ANN Benchmarks](https://ann-benchmarks.com/)
- [Ethan Steininger's vectorsearch.dev notes](https://github.com/esteininger/vector-search) 
- [DataStax (Astra) Pinecone comparison on GigaOm](https://www.datastax.com/resources/report/gigaom-study-vector-databases-compared)