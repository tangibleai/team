# CORS Rabbithole

Our flowbite-svelte frontend can't fetch data from a separate public REST API on ConvoHub due to missing CORS Headers.
The Python requests package had no trouble accessing the DRF API at `https://staging.qary.ai/api/convo_list`. But using the javascript console on that page to type `fetch("https://staging.qary.ai/api/convo_list", {"method": "GET", "headers": {}})` fails, even when you provide headers.

Greg and I both tried to `fetch()` data from the Convo Hub public DRF API in javascriptt ways, but never got it to work.

Mozilla Developer Network forum suggests we [add support for CORS headers on the Django DRF server](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSMissingAllowOrigin?utm_source=devtools&utm_medium=firefox-cors-errors&utm_campaign=default) -- Reason: CORS header 'Access-Control-Allow-Origin' missing - HTTP - The response to the CORS request is missing the required
Access-Control-Allow-Origin header, which is used to determine whether or not the resource can be accessed by content operating within the current origin.

In Django-related QnAs, the main suggestion was to install and configure `django-cors-headers`:

- [StackOverflow 1: Enabling CORS (Cross Origin Request) in Django](https://stackoverflow.com/questions/38482059/enabling-cors-cross-origin-request-in-django)
- [StackOverflow 2: COR blocked in Django project](https://stackoverflow.com/questions/61443195/cross-origin-request-blocked-django-project)
- [StackOverflow 3 (2022): I have CORS whitelisted in Django but still blocked for OSM API](https://stackoverflow.com/questions/71715600/i-have-cors-whitelisted-in-django-and-the-cross-origin-request-is-still-blocked), [Django Forum](https://forum.djangoproject.com/t/django-cors-headers/15197/2)) -- `localhost:8080` is not treated the same as `127.0.0.1:8000/` -- try using the latter.
