# "Realtime Chat App using Django, RabbitMQ, Vue.js, and Websockets"

[Osaetin Daniel](https://danidee10.github.io/) put together an awesome blog series on how to build a Realtime Django chat app.
I'd love to build this and replace Vue.js with Svelte.js.
Blog was for Django 2.0 but seems to work for [3.1 as well](http://gitlab.com/tangibleai/osaetin)

### Django Backend

Create a Project on gitlab.com using the [+] at the top of your profile page.
We named ours after the author of the blog post that inspired all this: `osaetin` and we put it in our tangibleai organization account. Here's ours: [gitlab.com/tangibleai/osaetin](http://gitlab.com/tangibleai/osaetin)

If you want to be a **supercooperator**, don't forget to chose the `[ ] Public` radio button. And if you want to be an **boldcooperator**, you can add the [Hippocratic License file](https://firstdonoharm.dev/version/2/1/license.html). Here's our [v2.1 LICENSE](http://gitlab.com/tangibleai/osaetin/-/blob/master/LICENSE). And here's Mauricio Domingez's [v2.0 license in markdown format]( https://gitlab.com/evilpilaf/hippocratic-license).

Now that you have a project set up you can clone it and get to work fleshing it out. Make sure you've previously [set up your gitlab ssh keys](http://tangibleai.com/blog)

```
$ git clone git@gitlab.com:tangibleai/osaetin
$ cd osaetin
$ ls -a

.  ..  .git  LICENSE  README.md
```

You'll want to create a conda environment to hold all your python dependencies.

```
$ conda create -n osaetinenv 'python>=3.7.5,<3.8'
$ conda activate oseatinenv
$ pip install django djoser django-extensions djangorestframework
```

And then list those dependencies in an environment.yml or requirements.txt.

#### *`cat requirements.txt`*
```bash
Django==3.1.1
django-extensions==3.0.8
djangorestframework==3.12.2
djoser==2.1.0
django-notif
```

Now you can create a Django 3.1 project called `chatire` or whatever you like:

```bash
$ django-admin startproject chatire
$ cd chatire
$ python manage.py migrate
$ python manage.py runserver
```

Browsing to 127.0.0.1 you should see a shiny new Django app homepage:

![Django 3.1 -- The install worked successfully! Congratulations!](django-3-1-start-project---runserver.png)

You'll eventually create a webpack to hold all your static files for vue or svelte. So now's a good time to glance at your directory structure to get the lay of the land:

```console
$ cd chatire
$ tree -I '__pycache__'
.
├── chatire
│   ├── asgi.py
│   ├── __init__.py
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── db.sqlite3
└── manage.py
```

Like for most django apps, you probably want to create a superuser so you can examine the database with the django-admin GUI.

You can do it the old-fashined interactive way:

```console
$ python manage.py createsuperuser
```

**OR** you can automate it (espcially useful for deployment automation):

```console
$ echo "from django.contrib.auth import get_user_model; \
User = get_user_model(); User.objects.create_superuser('admin','admin@example.com','password')" | \
python manage.py shell
```

After logging into `http://127.0.0.1:8000/admin` you should be able to see the tables for your Groups and Users. Spoiler alert ;) you shouldn't see the table for **Tokens** just yet.

![](django-admin-interface-after-createsuperuser.png)

### REST Authentication

Now edit your `settings.py` to include those cool django packages that work together to let you authenticate new users over a REST api: `rest_framework` and `djoser`.

#### _`chatire/settings.py`_
```python
...
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',
    'rest_framework.authtoken',
    'djoser',
]
...
```

And you need to tell DRF how to authenticate connections to the REST API:

#### _`chatire/settings.py`_
```python
...
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}
...
```

Add the `urls.py` plumbing to expose those authentication endpoints:

#### _`chatire/urls.py`_
```python
from django.contrib import admin
from django.conf.urls import url, include

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^auth/', include('djoser.urls')),
]
```

#### test creating a new user
```console
$ ./manage.py runserver &
$ curl -X POST http://127.0.0.1:8000/auth/users/ --data 'email=notrequired@nowhere.com&username=examplenewusername2&password=newpassd2'
{"email":"notrequired@nowhere.com","username":"examplenewusername2","id":2}
$ curl -X POST http://127.0.0.1:8000/auth/users/ --data 'username=newuser&password=newpassd'
{"email":"","username":"newuser","id":3}
```

### Vue Frontend Scaffolding

If you've never installed node before here's how to do it right.

#### install Vue.js
```console
$ sudo apt remove npm nodejs node-vue node-stable libnode64
$ curl https://raw.githubusercontent.com/nvm-sh/nvm/master/nvm.sh -o ~/install_nvm.sh
$ echo 'source ~/install_nvm.sh' >> ~/.bashrc
$ source ~/.bashrc
$ nvm --version
0.37.2
$ node --version
v15.6.0
$ npm --version
7.4.0
$ npm install node
$ npm install --global @vue/cli
$ npm install --global @vue/cli-init
$ vue --version
@vue/cli 4.5.11
```

Now you can use `vue create` to build a template folder structure for your vue frontend:

```
~/code/tangibleai/osaetin/chatire$ vue create chatire-frontend

Vue CLI v4.5.11
? Please pick a preset: Default ([Vue 2] babel, eslint)
...
```

You need to tell the development server the IP address or hostname you want the serve script from.
Otherwise your development server will 404 on Linux.
Edit your `chatire-frontend/package.json` file to add `--host 0.0.0.0` after the `vue-cli-service serve` script:

#### _`chatire-frontend/package.json`_
```js
{
  "name": "chatire-frontend",
  "version": "0.1.0",
  "private": true,
  "scripts": {
    "serve": "vue-cli-service serve --host 0.0.0.0",
    "build": "vue-cli-service build",
...
```

Then your dev webserver should work just fine and launch the Vue homepage in your browser (`npm run serve`):

#### `http://127.0.0.1:8080`
![](vue-cli-app-boilerplate-welcome-page.png)

If you messed up and used the `vue init` command (like I did the first time) you may end up with an older unsupported version of the webpack compiler. Don't go down this rabbithole of `npm audit fix`!!!

#### Old Unsupported Version!!!
![Vue-CLI Boilerplate App for older unsupported version =500x200](npm-run-dev-vuejs-app-old-version.png)

Here's the directory structure for you django project.
Your new vue project for the `chatire-frontend` should sit right along side your `manage.py`.

- `chatire/chatire`: your django project, including any django apps you create.
- `chatire/chatire-frontend`: the node modules and config to compile the Vue

Within `chatire-frontend` are 4 main directories you'll need to put your code in:

- `build/`: scripts to run the webpack developement server and build the app. The `npm run dev` command is equivalent to `webpack-dev-server --inline --progress --config build/webpack.dev.conf.js`. The `--inline` option injects compiled static files into `index.html`.
- `config/`: configuration settings for development, testing and production
- `public/`: `index.html` and `favicon.ico` for your app
- `src/router/`: urls and api endpoint specifications in the index.js file
- `src/assets/`: images (icons, etc)
- `src/components/`: your single file `.vue` components
- `test/`: end-to-end (`npm run e2d`) tests with `Nightwatch` and unit (`npm run unit`) tests with `Jest`

Here's your directory structure for the entire project:

```console
$ tree -I 'node_modules|__pycache__'

├── chatire/
│   ├── chatire/
│   │   ├── asgi.py
│   │   ├── __init__.py
│   │   ├── settings.py
│   │   ├── urls.py
│   │   └── wsgi.py
|   |
│   ├── chatire-frontend/
│   │   ├── babel.config.js
│   │   ├── index.chatire.html
|   |   ├── node_packages/...
│   │   ├── package.json
│   │   ├── package-lock.json
│   │   ├── public
│   │   │   ├── favicon.ico
│   │   │   └── index.html
│   │   ├── README.md
│   │   └── src/
│   │       ├── App.vue
│   │       ├── assets/
│   │       │   └── logo.png
│   │       ├── components/
│   │       │   └── HelloWorld.vue
│   │       └── main.js
|   |
│   ├── db.sqlite3
│   └── manage.py
├── LICENSE
├── README.md
└── requirements.txt
```

### Customize Your Vue Frontend

Now that you have all the boilerplate in place, you can start building the business end of the app, the frontend. Make sure you've launched the dev servers for both django and the frontend in separate terminals or screens (`tmux` or `screen` windows).

You'll also want two tabs open to make sure both sides of your app are working properly. I like to keep an eye on the data using the admin interface in django:

```console
$ cd osaetin
$ source .venv/bin/activate || source .venv/Scripts/activate
$ cd chatire
$ python manage.py runserver &

# open in Firefox
$ xdg-open http://127.0.0.1:8000/admin/
```

There's only one page to worry about for you Vue app but it's on port 8080 instead of 8000. So open another terminal/screen and run your Vue dev server:

```console
$ cd chatire-frontend
$ npm run serve &

# open in Firefox
$ xdg-open http://127.0.0.1:8080/
```

 
### Resources

- [Part 1: Introduction and Django Setup](https://danidee10.github.io/2018/01/01/realtime-django-1.html)
- [Part 1: Introduction and Django Setup](https://danidee10.github.io/2018/01/03/realtime-django-2.html)
- [Part 3: Build a REST API with Django Rest Framework](https://danidee10.github.io/2018/01/07/realtime-django-3.html)
- [Part 4: Vue.js Frontend for django API](https://danidee10.github.io/2018/01/10/realtime-django-4.html)
- [Part 5: RabbitMQ and uWSGI Websockets](https://danidee10.github.io/2018/01/13/realtime-django-5.html)
- [Part 6: Extra features](https://danidee10.github.io/2018/03/12/realtime-django-6.html)
