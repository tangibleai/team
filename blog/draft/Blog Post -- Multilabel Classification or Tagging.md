# Multilabel classification (tagging)

A machine learning classifer is great for automatically sorting data into buckets or categories (classes).
In most cases there's no clear dividing line between two categories.
But the real world is rarely black and white.
Your machine learning dataset will often contain a lot of _gray_ or _brown_ examples that you think should be labeled as both black _and_ white.
So what should you do for a machine learning dataset if some of your data records can't be put into a single category?
Multilabel classifiers are the answer.

You'll probably run into this question when you are processing natural language text, especially for intent recognition, language classification, sentiment analysis or hashtag recommendation engines.
With a multilabel classifier you can label natural language text with several different labels or tags so you can also think of it as tagging or predicting hashtags for your text.[^2]
Whenever your categories or classes overlap, a Venn diagram of your categories will have overlapping circles.
Imagine you are trying to classify the letters according to their language.[^1]
Several letters are reused in English, Greek, and Ukrainean. 

[![Venn diagram of Greek, Latin, and Ukrainean alphabets](https://en.wikipedia.org/wiki/File:Venn_diagram_gr_la_ru.svg)](https://en.wikipedia.org/wiki/File:Venn_diagram_gr_la_ru.svg)


## Intent recognition

A multilabel classifier is a machine learning model that can automatically assign multiple labels or tags to your data.
If you've ever had to read through text messages from your users to try to understand what they are saying,  pipeline to try to understand what your users
In this lesson you'll create a multilabel machine learning model to recognize the intent or sentiment of what your users are saying.

## Scikit-Learn `OneVsRestClassifier`

Scikit-learn let's you build a custom multilabel classifier using any of several different approaches.
The simplest and most common approach is to assume that each label (class) is identified independently of all the others.
This is called the "one vs rest" or "one vs all" approach to multilabel classification.
Scikit-learn uses a `OneVsRestClassifier` to make this possible.
You can specify whatever binary classifier you would like for the underlying estimator within the  `OneVsRestClassifier` model.

## Advanced model optimization

If you need to deploy a multilabel natural language classifier to a production application, you can save a lot of computational resources by taking the time to optimize your model a bit.
Rather than relying on a monolithic Scikit-Learn `Pipeline` object you can break the Pipeline into modular pieces.
And for the logistic regression part of the pipeline you can even eliminate the Scikit-Learn dependency entirely by doing the math directly in NumPy.
You can see how to do this on a real world problem in Appendix E of [Natural Language Processing in Action, Second Edition](https://www.manning.com/books/natural-language-processing-in-action-second-edition ) and the `nlu-fastapi` [open source project](gitlab.com/tangibleai/community/nlu-fastapi).

[^1] [Creole languages](https://en.wikipedia.org/wiki/Lingua_franca#Creole_languages) and other lingua francas mix not only alphabets but also vocabulary and grammar rules from multiple languages.
[^2] In the academic world multilabel natural language classification is called "Aspect Category Detection (ACD)": [Multi-Label Few-Shot Learning for Aspect Category Detection by Hu et al](https://arxiv.org/pdf/2105.14174.pdf)



