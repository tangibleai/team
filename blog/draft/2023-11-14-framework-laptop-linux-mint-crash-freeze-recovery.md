# crash recovery

Framework kept freezing on Linux Mint (Cinnamon) even after moving the partition and repairing the boot settings and deb packages with bad checksums.

TLDR; Seems to be a hardware error triggered by typing on the integrated laptop keyboard.

## Quickfix
- in EFEI boot options turn off fastboot and all CPU turbo (overclocking) options
- uninstall copyQ plugin
- uninstall firefox plugins
- install Librewolf and start using it with only one gmail
- use Thunderbird for all e-mail and calendaring
- boot to external USB
- launch gparted or kpart or "disks" and resize partition containing old OS and data
- format new partition as ext4
- install linux on new partition from usb
- on other OS partitions use disks to automount other OS partitions (to move files but not apps)
- label old partition as ext4 and bootable
- run `sudo update-grub` to search for bootable partitions
- reboot

### Clues

Eliminated these background applications as the cause:

- copyq
- libreoffice
- firefox
- firefox plugins
- bitwarden app
- mozilla VPN
- Slack
- GMail (high memory)
- Thunderbird (downloading 100k+ messages)

Only comonality is that it happens within 15 minutes of boot and I'm in the middle of typing something in Slack, Sublime, or the Terminal.
But this may be because typing on the laptop keyboard may trigger a hardware (RF?) glitch.
Using an external keyboard and monitor seems to prevent it.

During boot dmesg shows these errors during boot:

#### _`dmesg | grep DPC`_
```console
pci 0000:00:07.0: DPC: RP PIO log size 0 is invalid
pci 0000:00:07.1: DPC: RP PIO log size 0 is invalid
pci 0000:00:07.2: DPC: RP PIO log size 0 is invalid
pci 0000:00:07.3: DPC: RP PIO log size 0 is invalid
```

#### _`dmesg | grep -i DPC -B5 -A5`_
```console
[    2.302296] ACPI: PM: Power Resource [PIN]
[    2.305891] ACPI: PCI Root Bridge [PC00] (domain 0000 [bus 00-fe])
[    2.305904] acpi PNP0A08:00: _OSC: OS supports [ExtendedConfig ASPM ClockPM Segments MSI EDR HPX-Type3]
[    2.326239] acpi PNP0A08:00: _OSC: platform does not support [AER]
[    2.366153] acpi PNP0A08:00: _OSC: OS now controls [PCIeHotplug SHPCHotplug PME PCIeCapability LTR DPC]
[    2.394233] PCI host bridge to bus 0000:00
[    2.394237] pci_bus 0000:00: root bus resource [bus 00-fe]
[    2.394240] pci_bus 0000:00: root bus resource [io  0x0000-0x0cf7 window]
[    2.394243] pci_bus 0000:00: root bus resource [io  0x0d00-0xffff window]
[    2.394245] pci_bus 0000:00: root bus resource [mem 0x000a0000-0x000bffff window]
--
[    2.462066] pci 0000:00:06.0: [8086:9a09] type 01 class 0x060400
[    2.462240] pci 0000:00:06.0: PME# supported from D0 D3hot D3cold
[    2.462323] pci 0000:00:06.0: PTM enabled (root), 4ns granularity
[    2.482170] pci 0000:00:07.0: [8086:9a23] type 01 class 0x060400
[    2.482291] pci 0000:00:07.0: PME# supported from D0 D3hot D3cold
[    2.482328] pci 0000:00:07.0: DPC: RP PIO log size 0 is invalid
[    2.482336] fbcon: Taking over console
[    2.505562] pci 0000:00:07.1: [8086:9a25] type 01 class 0x060400
[    2.505683] pci 0000:00:07.1: PME# supported from D0 D3hot D3cold
[    2.505719] pci 0000:00:07.1: DPC: RP PIO log size 0 is invalid
[    2.525953] pci 0000:00:07.2: [8086:9a27] type 01 class 0x060400
[    2.526074] pci 0000:00:07.2: PME# supported from D0 D3hot D3cold
[    2.526110] pci 0000:00:07.2: DPC: RP PIO log size 0 is invalid
[    2.546378] pci 0000:00:07.3: [8086:9a29] type 01 class 0x060400
[    2.549463] pci 0000:00:07.3: PME# supported from D0 D3hot D3cold
[    2.549499] pci 0000:00:07.3: DPC: RP PIO log size 0 is invalid
[    2.569819] pci 0000:00:08.0: [8086:9a11] type 00 class 0x088000
[    2.569838] pci 0000:00:08.0: reg 0x10: [mem 0x605d1d8000-0x605d1d8fff 64bit]
[    2.577910] pci 0000:00:0a.0: [8086:9a0d] type 00 class 0x118000
[    2.577927] pci 0000:00:0a.0: reg 0x10: [mem 0x605d1c0000-0x605d1c7fff 64bit]
[    2.577967] pci 0000:00:0a.0: enabling Extended Tags
```

A few minutes after boot dmesg shows several probes on Cafe public wifi at Subterranean and MayaMoon.
These are for MayaMoon on 2023-11-16.

#### _`dmesg`_
```console
[   14.489260] wlp170s0: authenticate with 98:9d:5d:15:37:6c
[   14.507168] wlp170s0: send auth to 98:9d:5d:15:37:6c (try 1/3)
[   14.592677] wlp170s0: authenticated
[   14.594542] wlp170s0: associate with 98:9d:5d:15:37:6c (try 1/3)
[   14.645836] wlp170s0: RX AssocResp from 98:9d:5d:15:37:6c (capab=0x411 status=0 aid=11)
[   14.657283] wlp170s0: associated
[   14.891972] IPv6: ADDRCONF(NETDEV_CHANGE): wlp170s0: link becomes ready
[   15.200485] kauditd_printk_skb: 20 callbacks suppressed
[   15.200491] audit: type=1400 audit(1700174072.203:32): apparmor="STATUS" operation="profile_load" profile="unconfined" name="docker-default" pid=1516 comm="apparmor_parser"
[   15.270449] bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
[   15.272789] Bridge firewalling registered
[   15.325253] Initializing XFRM netlink socket
[   15.876177] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=33816 PROTO=2 
[   15.876194] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=33831 PROTO=2 
[   21.898304] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=37836 PROTO=2 
[   44.522116] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:c6:a0:00:5d:2b:cb:08:00 SRC=192.168.0.236 DST=224.0.0.251 LEN=32 TOS=0x00 PREC=0x00 TTL=1 ID=15843 PROTO=2 
[   68.986163] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=60505 PROTO=2 
[   78.507633] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=1734 PROTO=2 
[  163.019692] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:1c:57:dc:82:c4:08:08:00 SRC=192.168.0.11 DST=224.0.0.251 LEN=32 TOS=0x00 PREC=0x00 TTL=1 ID=13376 PROTO=2 
[  174.704532] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=58236 PROTO=2 
[  174.994247] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=58406 PROTO=2 
[  182.039235] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:01:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.1 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=19441 PROTO=2 
[  183.983524] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=58517 PROTO=2 
[  242.970408] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=20074 PROTO=2 
[  251.569477] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:4a:83:57:05:84:05:08:00 SRC=192.168.0.59 DST=224.0.0.251 LEN=32 TOS=0x00 PREC=0x00 TTL=1 ID=34634 PROTO=2 
[  319.259820] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=49280 PROTO=2 
[  335.240441] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=56772 PROTO=2 
[  337.680156] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.251 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=58664 PROTO=2 
[  432.709261] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:01:98:9d:5d:15:37:6b:08:00 SRC=192.168.0.1 DST=224.0.0.1 LEN=36 TOS=0x00 PREC=0xC0 TTL=1 ID=47416 PROTO=2 
[  452.075301] [UFW BLOCK] IN=wlp170s0 OUT= MAC=01:00:5e:00:00:fb:c6:a0:00:5d:2b:cb:08:00 SRC=192.168.0.236 DST=224.0.0.251 LEN=32 TOS=0x00 PREC=0x00 TTL=1 ID=48664 PROTO=2 
```

#### _`ifconfig`_

```console
wlp170s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.183  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 2001:579:e0a8:c0:59cd:fc25:59d8:49f3  prefixlen 64  scopeid 0x0<global>
        inet6 2001:579:e0a8:c0:f97:2e66:ff69:268e  prefixlen 64  scopeid 0x0<global>
        inet6 2001:579:e0a8:c0:87d:881b:cfa:af65  prefixlen 128  scopeid 0x0<global>
        inet6 fe80::ba4f:2a2c:c112:dbea  prefixlen 64  scopeid 0x20<link>
        ether 4c:77:cb:ef:44:71  txqueuelen 1000  (Ethernet)
        RX packets 8001  bytes 5285433 (5.2 MB)
        RX errors 0  dropped 38  overruns 0  frame 0
        TX packets 5468  bytes 2498796 (2.4 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

Coworker was unable to use the Windows Desktop Slack app on the Subterranean Cafe WiFi and had to switch to her LTE phone internet hotspot.
I had to disconnect and reconnect to the Subt WiFi before I was able to get the Mozilla VPN to connect.
Even then, it would show an unstable connection.

### Apt package integrity

#### _`hist | grep debsums -A5 -B5`_
```console
diff .bashrc /etc/bash.bashrc
sudo apt-get uninstall libreoffice libreoffice-draw libreoffice-math
sudo apt-get remove libreoffice libreoffice-draw libreoffice-math
sudo apt-get remove libreoffice*
sudo apt-get remove yelp
sudo apt-get install debsums
sudo debsums_init
sudo debsums-init
sudo debsums init
sudo debsums -cs
sudo debsums -c | xargs -rd '\n' -- dpkg -S | cut -d : -f 1 | sort -u
xargs -rd '\n' -a <(sudo debsums -c | xargs -rd '\n' -- dpkg -S | cut -d : -f 1 | sort -u) -- sudo apt-get install -f --reinstall --
sudo debsums -c
apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)
sudo apt-get install --reinstall $(dpkg -S $(debsums -c) | cut -d : -f 1 | sort -u)
sudo su root
sudo debsums -c
sudo su root
sudo apt search Adwaita
sudo apt-get remove adwaita-qt
sudo apt-get remove adwaita-icon-theme-full
history | grep remove
```

#### _`sudo debsums --help`_
```console
debsums checks the MD5 sums of installed debian packages.

Usage: debsums [OPTIONS] [PACKAGE|DEB] ...

Options:
 -a, --all                    check configuration files (normally excluded)
 -e, --config                 check only configuration files
 -c, --changed                report changed files (implies -s)
 -l, --list-missing           list packages which don\'t have an md5sums file
 -s, --silent                 only report errors
 -m, --md5sums=FILE           read list of deb checksums from FILE
 -x, --report-mismatches      report errors and print the md5sums mismatch
 -r, --root=DIR               root directory to check (default /)
 -d, --admindir=DIR           dpkg admin directory (default /var/lib/dpkg)
 -p, --deb-path=DIR[:DIR...]  search path for debs
 -g, --generate=[all][,keep[,nocheck]]
                              generate md5sums from deb contents
     --no-locale-purge        report missing locale files even if localepurge
                              is configured
     --no-prelink             report changed ELF files even if prelink is
                              configured
     --ignore-obsolete        ignore obsolete conffiles.
     --help                   print this help, then exit
     --version                print version number, then exit
```

Need to chown root:root or chmod 700 these files:

```console
debsums: can't open blueman file /var/lib/polkit-1/localauthority/10-vendor.d/org.blueman.pkla (Permission denied)
debsums: can't open cups-browsed file /usr/lib/cups/backend/implicitclass (Permission denied)
debsums: can't open cups-filters file /usr/lib/cups/backend/cups-brf (Permission denied)
debsums: can't open flatpak file /var/lib/polkit-1/localauthority/10-vendor.d/org.freedesktop.Flatpak.pkla (Permission denied)
debsums: can't open fwupd file /var/lib/polkit-1/localauthority/10-vendor.d/fwupd.pkla (Permission denied)
debsums: can't open geoclue-2.0 file /var/lib/polkit-1/localauthority/10-vendor.d/geoclue-2.0.pkla (Permission denied)
debsums: can't open linux-image-5.15.0-79-generic file /boot/vmlinuz-5.15.0-79-generic (Permission denied)
debsums: can't open linux-image-5.15.0-83-generic file /boot/vmlinuz-5.15.0-83-generic (Permission denied)
debsums: can't open linux-modules-5.15.0-76-generic file /boot/System.map-5.15.0-76-generic (Permission denied)
debsums: can't open linux-modules-5.15.0-79-generic file /boot/System.map-5.15.0-79-generic (Permission denied)
debsums: can't open linux-modules-5.15.0-83-generic file /boot/System.map-5.15.0-83-generic (Permission denied)
debsums: can't open network-manager file /var/lib/polkit-1/localauthority/10-vendor.d/org.freedesktop.NetworkManager.pkla (Permission denied)
debsums: can't open packagekit file /var/lib/polkit-1/localauthority/10-vendor.d/org.freedesktop.packagekit.pkla (Permission denied)
debsums: can't open policykit-desktop-privileges file /var/lib/polkit-1/localauthority/10-vendor.d/com.ubuntu.desktop.pkla (Permission denied)
debsums: can't open smbclient file /usr/lib/x86_64-linux-gnu/samba/smbspool_krb5_wrapper (Permission denied)
debsums: can't open systemd file /var/lib/polkit-1/localauthority/10-vendor.d/systemd-networkd.pkla (Permission denied)
```

### packages to manually reinstall based on these corrupt files

Uninstalling and reinstalling these packages doesn't fix the checksum.

#### _`sudo debsums -c`_
```bash
$ (base) root@minty:/home/hobs# debsums -c
/usr/share/icons/Adwaita/16x16/places/start-here.png
/usr/share/icons/Adwaita/22x22/places/start-here.png
/usr/share/icons/Adwaita/24x24/places/start-here.png
/usr/share/icons/Adwaita/32x32/places/start-here.png
/usr/share/icons/Adwaita/48x48/places/start-here.png
/usr/share/cups/data/default-testpage.pdf
/usr/lib/firefox/distribution/distribution.ini
/usr/share/icons/HighContrast/16x16/places/start-here.png
/usr/share/icons/HighContrast/22x22/places/start-here.png
/usr/share/icons/HighContrast/24x24/places/start-here.png
/usr/share/icons/HighContrast/32x32/places/start-here.png
/usr/share/icons/HighContrast/48x48/places/start-here.png
/usr/share/icons/HighContrast/scalable/places/start-here.svg
/usr/share/icons/gnome/16x16/places/debian-swirl.png
/usr/share/icons/gnome/22x22/places/debian-swirl.png
/usr/share/icons/gnome/24x24/places/debian-swirl.png
/usr/share/icons/gnome/32x32/places/debian-swirl.png
/usr/share/icons/gnome/48x48/places/debian-swirl.png
/usr/share/icons/gnome/scalable/places/debian-swirl.svg
/usr/share/applications/im-config.desktop
/usr/share/applications/info.desktop
/usr/share/applications/libreoffice-draw.desktop
/usr/share/applications/libreoffice-math.desktop
/usr/share/applications/mate-color-select.desktop
/usr/share/sounds/freedesktop/stereo/device-added.oga
/usr/share/sounds/freedesktop/stereo/device-removed.oga
/usr/share/applications/yelp.desktop
```

After resizing and moving broken partition to create alternative Mint install partition (with more size than old one):

#### _`df`_
```console
Filesystem          1K-blocks      Used Available Use% Mounted on
tmpfs                 6562408      1908   6560500   1% /run
/dev/nvme0n1p4      433042336 379519280  31452224  93% /
tmpfs                32812024      1076  32810948   1% /dev/shm
tmpfs                    5120         4      5116   1% /run/lock
tmpfs                32812024         0  32812024   0% /run/qemu
/dev/nvme0n1p1         523248     13128    510120   3% /boot/efi
/dev/nvme0n1p2       38986396  34453508   2480316  94% /media/sda-40GB
tmpfs                 6562404       132   6562272   1% /run/user/1000
/home/hobs/.Private 433042336 379519280  31452224  93% /home/hobs
```

With Librewolf and one Gmail inbox tab (and a dozen others, including Tutanota) and Thunderbird open:

#### _`free -h`_
```console
               total        used        free      shared  buff/cache   available
Mem:            62Gi       3.7Gi        52Gi       1.1Gi       6.8Gi        57Gi
Swap:          2.0Gi          0B       2.0Gi
```