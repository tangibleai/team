
# 2023-08-08 AEP Podcast Panel Recording - _LLMs and AI_ - by Jessa Spainhower

* Social Impact - Hobson Lane, Tangible AI  
* Academia - Douglas Fisher, SDSU  
* Legal - Gayani Weerasinghe, Law Offices of Gayani R. Weerasinghe 
* Moderator - Corinne Lytle Bonine, AEP Board Vice President 
* Jessa Spainhower - behind the scenes organizer 

## Discussion Topics: 

* Overview & Definition: AI, LLM, Chat GPT 
  - LLM - language models have been around for decades, and they seem to get smarter the bigger they get and the larger they get
  - In 2018 Deep Mind released the Transformer architecture with BERT, the first *large* language model (trained on Billions of words)
  - In 2019 Tangible AI introduced LLMs to nonprofits
  - In 2020 we open sourced qary.ai for general question answering
  - one of the first things volunteers asked qary is what we should call a prosocial chatbot designed to be truthful, and kind
  - ChatGPT took this to the next level focused on generating responses to conversational prompts
* What is your personal viewpoint on AI/Chat GPT? 
  - It is being used by BigTech to monetize society rather than solve the world's problems
  - The leaders in the space, with access to the capital required to build and train these bots are training LLMs to be sychophants and stochastic parots, telling us what we want to hear, creating more division in society rather than bringing us together (the Like button problem).
* What are the ethics of using it in academia and/or work? 
  - teachers focused on cheating - adding to their workload rather than helping them
  - Imagine your school board wanted to hire a narcicistic, manipulative, lying teacher that claimed to understand everything on the Internet?
* What potential legal implications does it have in the workplace? 
  - Copyright infringement
  - Sharing of user data (illegal in EU)
* What is the environmental impact of Chat GPT/LLM? 
  - electricity is .3 / kwhr
  - About 5000 metric tons of carbon to train GPT-4 which is about 5 million international airline tickets, or almost a billion dollars.
  - ChatGPT is 5 wps 300 wpm and 1 cent per minute 
  - Much of the computational power is wasted during training, by training on garbage (Reddit, Twitter, Facebook)

    Can this even the playing field in terms of equity? 
    Does this, and if so - how, hinder critical thinking? 
    How are LLMs being used for ESG now? 
    How accurate are LLMs in various domains (general web search, writing software, STEM QA, creative writing assistance)? 
    What are the costs to host your own LLM? 
    What is the cost to customize an LLM for a domain-specific chatbot for your application or marketing campaign? 
    Life up responsible AI companies: which companies are researching the dangers of LLMs and using LLMs ethically? 
    What jobs are most likely to be devalued/replaced by LLMs? 
