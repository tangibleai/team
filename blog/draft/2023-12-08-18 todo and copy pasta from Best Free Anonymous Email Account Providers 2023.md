# Dark identity

You need a dark identity if you want to penetrate the dark web.
This article will focus on creating e-mail and social media accounts with a fake persona.

The primary reason for a dark identity is so that you want to fit in and gain the trust of others on the dark web.
Gone are the days of hopping on the right IRC channel and watching hackers explain what their up to.
Even back in 2015, before the FBI infiltrated dark web channels such as IRC, you would probably get blasted and shamed if you ever shared your IP address or other identifiable information.

The last thing you want is to get caught up in legal trouble by saying the wrong thing or sharing the wrong file online.
It is now illegal in the US to even possess data that has the same fingerprint as contraband data, such as databases of usernames, passwords, credit card numbers, some crypto account passwords, images/videos prohibited by law in your jurisdiction, books, articles, and audio files that violate DMCA or copyright law. Fair use now longer protects you from the law.

And even worse, you could be targeted by criminals if others can connect your online dark identity to your IRL identity.
Obviously they want to steal your money and identity for conventional fraud.
And they want to gain access to the computers, accounts and phones you have access to, so they can use them in attacks on others.
Or they may want to silence you simply because you've seen something they don't wany you to know.
Or they may want

## TODO: 
- Test each one to sign up for gitlab, yahoo, gmail, 
- Create table with score for various qualities
- write team/blogs/
- Record HPR
- strategize on creating a new identity photos
  - start with or 3D models or a full body images variety of poses and clothing
  - filter/osbsure/ 2D images within annonymized camera photos of real world
  - GIMP it to distort it, filter it, add/remove camera noise, low light, grainy, pixelated, etc
  - selfies transformed with web tools into cartoons/sketches
  - selfies inserted into action scenes from others' nonpublic posts within the networketc from scrambled/filtered stable diffusionand getting PIPL and Google to pick it up and recognize it as an 16 or 17 year old (adult with access to Tic Toc, Instagram, etc)

### **1\.** [**ProtonMail**](https://protonmail.com/) **– Secure & Private** Free Anonymous Email Service

![ProtonMail for anonymous email service provider](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-579.png)

![ProtonMail for anonymous email service provider](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-579.png)

ProtonMail is my top recommendation for those looking for free anonymous email service provider they can use over and over again. Here is what makes ProtonMail so great:

-   It does not require any personal information when signing up.
-   Located in Switzerland, ProtonMail is backed by some of the world’s most rigorous privacy laws. So your data isn’t just stored; it’s practically in a Swiss vault.
-   It does not log your IP address. Theoretically, you don’t even need a VPN, though you should probably still use one for extra security.
-   There is end-to-end encryption, which means that even ProtonMail can not access your messages.
-   Its free plan hooks you up with 1 email address, a generous daily limit of 150 messages, and a whopping 1GB of storage
-   There is a mobile app, so you can use it on your phone.

Proton has a calendar and cloud drive storage, which are also safe and secure.

### **2\.** [**Guerilla Mail**](https://www.guerrillamail.com/#)

![Guerilla Mail for free anonymous email accounts](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-580.png)

![Guerilla Mail for free anonymous email accounts](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-580.png)

Guerilla Mail is the second best email service provider for sending private, anonymous emails. You get a special email address that only you know — you will need to remember that email address to access your mail.

There is no signup process, so anyone who knows the email address will be able to read the messages that get sent to it. There is no password, name, or personal information required to access the mail sent to that address — it is entirely anonymous.

Since the email address is only a string of letters with a random domain name, as you can see in the screenshot, it will be pretty hard for people to guess that email address.

I’m sure you’re wondering, “Well, people will know my email address if I use it to sign up on a website or to email someone! Once they know my address, will they be able to access my mail?”

The answer is no — nobody will know your email address. That’s because you will be given a second email address, which is basically a scrambled version of the first address.

The second address is also a string of letters, numbers, and symbols. Even if someone has that second, scrambled email address because you gave it out or emailed them, it would be virtually impossible to guess the first email address (the one you use to view your inbox).

Save your first email address in a safe place. I would suggest writing it down on a piece of paper and storing it somewhere secure.

You can also memorize it, but it might be hard to memorize a string of random letters.

If you store your unscrambled email address on your computer, there is the possibility that someone will be able to hack your computer and see it. If you write it on a physical piece of paper, you will be truly anonymous, and the email account won’t be able to be traced back to you (provided you are using Tor or a VPN).

In addition, all messages sent to your inbox will only stay there for one hour. It doesn’t matter whether you see them or not — they will disappear after an hour.

That is a big downside — you will have to keep checking your inbox every hour. On the other hand, it also protects you in case anyone manages to figure out your address.

You can change your first email address at any time — you can set your own string of letters. Also, you can send a Forget Me request to the server to delete your email address; otherwise, it will last forever.

### **3\.** [**AnonAddy**](https://anonaddy.com/)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-581.png)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-581.png)

AnonAddy is a free and entirely open source anonymous email account provider. You can create an unlimited number of anonymous email aliases for free.

You might want several aliases for different purposes, such as when talking with different people, to make it harder for people to connect the dots and trace your activity back to you. It also helps confuse spammers.

Here’s how it works: First, you create your username and register it on the AnonAddy server.

For example, if your username is JohnDoe, your domain will be [JohnDoe.AnonAddy.com](mailto:JohnDoe@AnonAddy.com) or [JohnDoe.AnonyAddy.me](mailto:JohnDoe@AnonyAddy.me). Then, simply create any number of aliases and add them to the beginning of your domain to create a new email address.

For example, you can create [Alias1@JohnDoe.AnonAddy.com](mailto:Alias1@JohnDoe.AnonAddy.com) and [Alias2@JohnDoe.AnonAddy.com](mailto:Alias2@JohnDoe.AnonAddy.com).

You don’t have to do anything in the dashboard. Just enter a random alias when filling out a subscription form, for example, and when you get an email sent to that address, it will automatically be registered in your dashboard.

You can delete an alias at any time.

### **4\.** [**Lavabit**](https://lavabit.com/)

![Lavabit for anonymous email service](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-582.png)

![Lavabit for anonymous email service](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-582.png)

Looking for an email service that Edward Snowden actually used? Enter Lavabit.

Founded in 2004, this isn’t some run-of-the-mill email service provider. Lavabit takes your security seriously with its cutting-edge Dark Internet Mail Environment (DIME) platform.

But wait, before you jump in, know this: Lavabit isn’t a free ride anymore.

They ditched their free accounts back in 2017, so new users have to pay to play.

And if you’ve heard about the Lavabit Encrypted Proxy app, don’t get it twisted.

While it’s free, it’s not an email service; it’s a VPN proxy to shield your network traffic.

So, if emails are your game, Lavabit has the encryption fame, but you’ll have to open your wallet.

### **5\.** [**TrashMail**](https://trashmail.com/)

![TrashMail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-583.png)

![TrashMail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-583.png)

Want a disposable, anonymous email address that forwards messages to your regular email address? You can use TrashMail to set up a fake email address that will last for a specified amount of time.

Give out this email address to stay anonymous. There is no dashboard — all mail is routed to your real email address.

You don’t even need to create an account, though you can if you want to manage your email addresses.

To get started, create your custom email address on one of the domain options available. Enter the email address you want mail to be forwarded to and choose how long the disposable email address should last before being deleted.

You can also add the TrashMail Chrome extension to your browser and use it from there.

### **6\.** [**Temp Mail**](https://temp-mail.org/en/)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-584.png)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-584.png)

Temp Mail is another anonymous email account provider, designed for those looking for a temporary email address. I found it incredibly easy to use — it does not require you to sign up for an account.

As soon as you land on the site, you will see your temporary email address, which is a random string of letters and numbers on a random domain.

Your inbox is available as soon as you land on the page. You can clear the inbox, delete your temporary email address, or edit your temporary email address to create a new one.

Unlike Guerilla Mail, it does not seem like you can compose and send an email from this temporary address. Instead, it is best used to sign up anonymously for forums or sites that require email registration.

### **7\.** [**10 Minute Mail**](https://10minutemail.net/)

![10 Minute Mail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-585.png)

![10 Minute Mail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-585.png)

There are a number of 10 Minute Mail sites. I’m not sure if they are all owned by the same group or not.

I’m going to go with [10MinuteMail.net](https://10minutemail.net/), as that is the one I actually tested, and I verified that it works. I emailed the random address it gave me from my real email address — sure enough, the message arrived, and I was able to open it.

The downside is that there are a lot of ads, and you may need to refresh the page to view new mail. However, it is free.

I can’t promise that other 10 Minute Mail sites work as they are supposed to.

So, what’s the idea behind 10 Minute Mail? It’s simple, really: You get a temporary, anonymous email address that lasts for 10 minutes only.

You can use this email address to sign up for a site or forum that requires email registration or to get a free ebook without entering your real address. The email address consists of a random string of letters and numbers.

You can also give this email to someone to receive some information via email that you don’t want traced back to you.

After 10 minutes, your inbox and email address will expire.

### **8\.** [**Mailfence**](https://mailfence.com/en/)

![MailFence](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-586.png)

![MailFence](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-586.png)

MailFence is based in Belgium, so it is protected by Belgian privacy laws, which are better than US privacy laws. As such, it can offer better privacy and anonymity than an American competitor.

For example, only a local judge can request personal information from it — it can’t be a judge from another country. Also, court orders for such things are very hard to obtain.

Even if such a court order was obtained, all emails are end to end encrypted, so even Mailfence can not read your emails.

Mailfence has worked hard to make sure it is using only top-notch security. A free plan is available, and 15 percent of the premium plans are donated to support privacy rights in Europe.

### **9\.** [**Tutanota**](https://tutanota.com/)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-587.png)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-587.png)

Tutanota lets you sign up for an account with your phone number and without any personal information, thus allowing you to create a truly anonymous email account. Not only that, but all emails have built-in encryption, so nobody can access the content of your emails.

With the Tutanota mobile apps, you can send and manage your emails on your phone.

A free account is available, but it is somewhat limited. For example, you only get 1 GB of storage space.

### **10\.** [**Anonymous Email**](https://anonymousemail.me/)

![Anonymous Email](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-588.png)

![Anonymous Email](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-588.png)

Anonymous Email is a cool little site that lets you send and receive emails anonymously. No registration is required — just visit the website and start using the service immediately.

To send an anonymous email, simply visit the site and edit the sender’s address in the first field. You can add any string of letters in front of [noreply@anonymousmail.me](mailto:noreply@anonymousmail.me).

However, if you are using it for free, you can not receive replies anonymously. If you want to be able to get replies forwarded to your real email address without disclosing it, you have to sign up for the premium plan.

So, if you are using it for free, leave the reply address blank, so nobody sees your real address. The recipient will see the email, but they won’t be able to respond.

### **11\.** [**5Ymail**](https://www.5ymail.com/)

![5Ymail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-589.png)

![5Ymail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-589.png)

5Ymail is an excellent way to send emails anonymously. Simply go to the homepage, enter the recipient’s information, and compose your email!

You can also enter your own email address, so you can get replies and create an account. Everything will still be 100 percent anonymous, but you will now be able to log in and manage your account, as well as receive notifications when you get new emails.

### **12\.** [**AnonEmail**](http://anonymouse.org/anonemail.html)

![AnonEmail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-590.png)

![AnonEmail](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-590.png)

AnonEmail is one of the quickest ways to send an email anonymously. You can’t receive emails, however, — it’s a one-way thing.

Just visit the website, enter the recipient’s email address, create a subject line, write your email, fill out the captcha, and send your email!

### **13\.** [**myTrashMailer**](https://www.mytrashmailer.com/)

myTrashMailer is a great service for those who want a temporary email address to receive emails anonymously. Whether you want to sign up for a website anonymously or receive some important information without it being traced back to you, you can do it with myTrashMailer.

No account registration is required. When it says to enter your email, it means to enter the custom email address you want to create temporarily.

You can create a new email address using a random string of letters or a fake name — it doesn’t matter.

### **14\.** [**Craigslist**](https://www.craigslist.org/)

![Craigslist](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-592.png)

![Craigslist](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-592.png)

I’m sure you’re wondering, “Craigslist? How is that an anonymous email provider?”

Well, it’s not… but you can still use it to set up an anonymous email address. You see, when you create a listing on Craigslist, your real email address will be hidden on the listing, replaced with a random string of letters.

Emails sent to that randomly generated address will be forwarded to your email account. That way, buyers can not see the personal information of sellers — it helps protect sellers from scammers.

So, to get an anonymous email account via Craigslist, all you have to do is create a list. It could be a simple posting pretending to sell an oven, for example.

Then, find that posting and click on the Reply button. Then, click on “Show Email.”

You will see a randomly generated email address. You can then give that email address to someone, so they can email you without them knowing your real email address.

This is a cool little hack that will usually work. There are more straightforward methods available, but it is still a valid option.

### **15\.** [**TempInbox**](https://www.tempinbox.xyz/)

![TempInbox](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-593.png)

![TempInbox](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-593.png)

TempInbox is a free and easy way to get a totally anonymous, private email address. You create your own email address — it can be any string of letters.

### **16\.** [**Tempr.email**](https://tempr.email/)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-594.png)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-594.png)

Tempr.email, like TempInbox, provides you with a customized temporary email address that is entirely anonymous. You can create a custom address followed by @tempr.email, @discard.email, discardmail.com, and a variety of other domains — you get to choose!

### **17\.** [**Gmail**](https://gmail.com)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-595.png)

![](https://rigorousthemes.com/blog/wp-content/uploads/2021/08/word-image-595.png)

Finally, I’m going to show you how you can create an anonymous email address with Gmail and most other popular email providers (though it does depend on the provider and how much information they request from you).

Here are the steps you need to take to create an anonymous account on Gmail:

1.  Make sure you [always use a VPN](https://rigorousthemes.com/blog/best-tunngle-alternative/) or the Tor browser! That way, your IP address will not be exposed, and Gmail won’t be able to know where you are from.
2.  Write a fake name. Use a name that sounds realistic, not something like John Doe.
3.  When choosing your email address, don’t include any personal information. In addition, don’t use anything that you have used as a username somewhere else.
4.  If Gmail asks you for a phone number, don’t give them your real phone number! Instead, get a phone number using the Text+ app. The reason I like Text+ is that you don’t need to add an email address to sign up for a number. That way, you become even less traceable. You do need a username for Text+, so choose one you have never used before, and never use it again.
5.  If Gmail asks you for your birthdate (which it will, at some point), give them a fake birthday.
6.  Be careful! Don’t add your real email as a recovery email in Google.
7.  When accessing Gmail in the future, whether on your desktop or from your phone, always use a VPN! Otherwise, your IP address will be disclosed later.

This method would theoretically work with any email account provider.

### **18\.** [StartMail](https://www.startmail.com/)

StartMail is not free. It is a privacy-focused email provider that offers a 7-day free trial.

Hailing from the privacy-loving Netherlands, StartMail is the email provider that doesn’t want to know you—that is, your data.

Kicking off in 2013, StartMail has one mission: secure email without the fluff.

Let’s break it down.

You get server-side PGP encryption to secure your conversations, but know this: it’s not true end-to-end like some competitors.

You can also whip up unlimited custom email addresses to dodge spam and keep your main address pristine.

StartMail lets you send and receive emails from your very own custom domain. Plus, with 20GB of storage, saying “my inbox is full” is a thing of the past.

Oh, and for the app aficionados out there, it plays nice with third-party apps like Apple Mail and Outlook.

It kicks off with a 7-day free trial, followed by a $59.95 annual fee.

In the world of secure emails, it holds its own with a user-friendly interface and features that make it a solid pick for both personal and professional use.

## A Note About Anonymous Email Account Providers

Some email account providers have promised to keep user information secret, but they did not keep their side of the deal. For example, Hushmail is a popular “secret” account provider.

However, when the Canadian government served them a court order with regard to steroid smugglers, Hushmail was quick to hand over CDs full of personal data and private information. Hushmail showed they can’t be trusted.

Instead, you want a service provider like Lavabit, which actually shut down instead of complying with government requests for user data. Consider researching an email account provider to see whether they have complied with court orders and handed over data before using them.

Another interesting choice is SAFe-mail, or Safe-Mail.net. Safe-Mail.net became the preferred choice for users of the dark web after TorMail was seized by the FBI.

If the dark web users who used to use TorMail are switching to Safe-Mail.net, you would assume it is safe and private. However, there are [reports](https://www.reddit.com/r/onions/comments/1xce9e/psa_safemail_isnt_safe/?) that it isn’t that safe and will disclose information if it has a legal requirement to do so.