
## Natural language

If you use your IDE to take notes, write blog posts, or even edit ebook manuscripts, you will want to to turn on spell checking and add a grammar checker.

### Grammarly in Sublime text 4

When you install the Grammarly plugin (package) it doesn't give your a README or any instructions.
The file you need to edit is called `LSP-Grammarly.sublime-settings` and it should be in your User settings for `Sublime Text`:

#### _`~/.config/sublime-text/Packages/User/LSP-Grammarly.sublime-settings`_

```json
{
  "selector": "text.asciidoc | text.html.markdown | text.tex.latex ",

  "initializationOptions": {
    "clientId": "client_BaDkMgx4X19X9UxxYRCXZo"
  },

  "settings": {
    // Specific variety of English being written.
    // See [this article](https://support.grammarly.com/hc/en-us/articles/115000089992-Select-between-British-English-American-English-Canadian-English-and-Australian-English)
    // for differences.
    // possible values: american, australian, british, canadian, auto-text
    // "grammarly.config.documentDialect": "auto-text",
    "grammarly.config.documentDialect": "american",  // HL
```

I changed my language to "american", but this may cause problems when editing Wikipedia articles in "international" English.

Unfortunately, I couldn't figure out a way to get Grammarly to ignore code blocks in markdown and asciidoc.