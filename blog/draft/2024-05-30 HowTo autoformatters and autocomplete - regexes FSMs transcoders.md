# DRAFT: Building an autoformatter from scratch

In this tutorial you will learn how to build an autoformatter from scratch.
But first you need to reconnect to an old friend that you probably had a falling out with in school -- regular expressions.

Another name for regular expressions is finite state automatta or finite state machine.
That name alone is probably enough to send you running for the door.


## References

- [Kyle Gorman's paper on PyNinni Chapter 9](https://aclanthology.org/W16-2409/ "aclanthology.org/W16-2409")
- [Build a regular expression engine from scratch](https://rhaeguard.github.io/posts/regex/ "rhaeguard.github.io/posts/regex")
- [Let’s Build a Regex Engine | kean.blog](https://kean.blog/post/lets-build-regex)
- [psf/black: Uncompromising Python code formatter](https://github.com/psf/black "github.com/psf/black")
- [black.readthedocs.io/en/stable/index.html](https://black.readthedocs.io/en/stable/index.html "black.readthedocs.io/en/stable/index.html")
- [lobste.rs/.../ruff\_formatter\_extremely\_fast\_black](https://lobste.rs/s/0ruefy/ruff_formatter_extremely_fast_black "lobste.rs/.../ruff_formatter_extremely_fast_black")
- [pypi.org/project/black](https://pypi.org/project/black/ "pypi.org/project/black")
- [kean.blog/post/lets-build-regex](https://kean.blog/post/lets-build-regex "kean.blog/post/lets-build-regex")
- [rhaeguard.github.io/posts/regex](https://rhaeguard.github.io/posts/regex/ "rhaeguard.github.io/posts/regex")
- [medium.com/@rhaeguard/how-to-build-a-regex...](https://medium.com/@rhaeguard/how-to-build-a-regex-engine-from-scratch-743ea6e16909 "medium.com/@rhaeguard/how-to-build-a-regex...")
- [nickdrane.com/build-your-own-regex](https://nickdrane.com/build-your-own-regex/ "nickdrane.com/build-your-own-regex")
- [deniskyashif.com/2019/02/17/implementing-a...](https://deniskyashif.com/2019/02/17/implementing-a-regular-expression-engine/ "deniskyashif.com/2019/02/17/implementing-a...")
