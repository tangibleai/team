# Svelte interactive diagram library comparison

## SVG vs Canvas

In this [stackoverflow discussion](https://stackoverflow.com/questions/12310024/fast-and-responsive-interactive-charts-graphs-svg-canvas-other) Bostock says that you should use SVG for better debuging and move to canvas for speed later, and only in the "inner loop".

- SVG is more accessible
- SVG prints to PDF as vectors rather than raster (Canvas)
- Canvas is faster on most browsers


## Svelvet

- https://www.svelvet.io/
- https://github.com/open-source-labs/Svelvet/
- https://www.svelvet.io/


```bash
npm init vite@latest
# demo-svelvet
# svelte (instead of react, vue, angular, etc)
# javascript (instead of typescript or sveltekit)
npm install @xyflow/svelte
```

- easy to get started (import coponents, default props useful)
- includes Svelte components
- intuitive property values


## XYFlow

```bash
npm init vite@latest
# demo-xyflow
# svelte (instead of react, vue, angular, etc)
# javascript (instead of typescript or sveltekit)
npm install @xyflow/svelte
```

## GoJS

- uses Canvas for drawings which are inaccessible
- less modern

## D3.js

SVG or Canvas drawing are both possible.

- hundreds of examples and tutorials on bl.oks.org

## Cubism.js

- http://square.github.io/cubism/

Only for time-series charts.

