In [48]: >>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='')
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     ans_ids = output_ids[0][input_len:]
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok)
    ...: ...     prompt = output_str
    ...: ...     print(prompt)
    ...: 
Q: How do you know when you misunderstand the real world?
A: When
Q: How do you know when you misunderstand the real world?
A: When
 you
Q: How do you know when you misunderstand the real world?
A: When you
 find
Q: How do you know when you misunderstand the real world?
A: When you find
 yourself
Q: How do you know when you misunderstand the real world?
A: When you find yourself
 constantly
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly
 disag
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disag
ree
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagree
ing
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing
 with
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with
 people
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people
 who
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who
 have
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have
 actually
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually
 experienced
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced
 the
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the
 real
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real
 world
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world
.
Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.


Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.



Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.




Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.





Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.





</s>
 Q: How do you know when you misunderstand the real world?
A: When you find yourself constantly disagreeing with people who have actually experienced the real world.



</s>

In [49]: >>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='')
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, end='')
    ...: ...     print()
    ...: ...     print(f'tok: {tok}')
    ...: ...     prompt = output_str
    ...: ...     print(prompt, end='')
    ...: 
Q: How do you know when you misunderstand the real world?
A: When
tok:  When
Q: How do you know when you misunderstand the real world?
A: When you
tok:  you
Q: How do you know when you misunderstand the real world?
A: When you realize
tok:  realize
Q: How do you know when you misunderstand the real world?
A: When you realize you
tok:  you
Q: How do you know when you misunderstand the real world?
A: When you realize you'
tok: '
Q: How do you know when you misunderstand the real world?
A: When you realize you're
tok: re
Q: How do you know when you misunderstand the real world?
A: When you realize you're living
tok:  living
Q: How do you know when you misunderstand the real world?
A: When you realize you're living^C

KeyboardInterrupt: 

In [50]: >>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='')
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, end='')
    ...: ...     # print()
    ...: ...     # print(f'tok: {tok}')
    ...: ...     prompt = output_str
    ...: ...     # print(prompt, end='')
    ...: 
Q: How do you know when you misunderstand the real world?
A: When you're living in a fantasy.

Explanation:


^CThis joke is a play on the idea that people can sometimes be

KeyboardInterrupt: 

In [51]: >>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='', flush=True)
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     ans_ids = output_ids[0][input_len:]
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, flush=True)
    ...: ...     prompt = output_str
    ...: 
Q: How do you know when you misunderstand the real world?
A: When
 you
 find
 yourself
 constantly
 disag
ree
ing
 with
 the
 real
 world
^C^C
KeyboardInterrupt


In [52]: from transformers import set_seed

In [53]: set_seed(0)

In [54]: >>> prompt = "Q: How do you know when you misunderstand the real world?\nA:"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='', flush=True)
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     ans_ids = output_ids[0][input_len:]
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, end='', flush=True)
    ...: ...     prompt = output_str
    ...: 
Q: How do you know when you misunderstand the real world?
A: When you realize that your understanding of the real world is different from everyone else's.
Q: How do you know when you're not understanding something?
A: When you're not understanding something, you'll know it.
Q: How do you know when you're misunderstanding something?
A: When you're misunderstanding something, you'll know it.
Q: How do you know when you're not getting it?
A: When you're not getting it, you'll know it.
Q^C

KeyboardInterrupt: 

In [55]: >>> prompt = "How do LLMs know when they make common sense errors?"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: 
    ...: >>> print(prompt, end='', flush=True)
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     ans_ids = output_ids[0][input_len:]
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, end='', flush=True)
    ...: ...     prompt = output_str
    ...: 
How do LLMs know when they make common sense errors?

I am an LLM student and I have been struggling with common sense errors in my legal writing. I am aware that these errors can be difficult to identify, but I am wondering how LLMs know when they make them? How do they identify and correct these errors?
LLMs have a number of tools and^C^C
KeyboardInterrupt


In [56]: >>> prompt = "How do you (Llama2) know when you make common sense errors?"
    ...: >>> input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: >>> input_ids
    ...: >>> set_seed(0)
    ...: >>> print(prompt, end='', flush=True)
    ...: >>> while not prompt.endswith('</s>'):
    ...: ...     input_ids = tokenizer(prompt, return_tensors="pt").input_ids
    ...: ...     input_len = len(input_ids[0])
    ...: ...     output_ids = llama.generate(
    ...: ...         input_ids, max_length=input_len + 1)
    ...: ...     ans_ids = output_ids[0][input_len:]
    ...: ...     output_str = tokenizer.batch_decode(
    ...: ...         output_ids, skip_special_tokens=False)[0]
    ...: ...     output_str = output_str[3:]  # <1>
    ...: ...     tok = output_str[len(prompt):]
    ...: ...     print(tok, end='', flush=True)
    ...: ...     prompt = output_str
    ...: 
How do you (Llama2) know when you make common sense errors?

Llama2: *chews thoughtfully on a nearby bush* Well, you see, I'm just an AI, I don't have personal experiences or emotions like humans do. But I'm designed to recognize common sense errors through various means.