# Linux command line fundamentals

TODO: find bash skills list and training in team/

Developing your Linux (POSIX) command line skill will improve your productivity immensely.
It's the only command line "language" or syntax that you can count on being available wherever you work and play.
It is your most important skill because it is the foundation for all your other computer skills.
Learning Linux in the 21st century is like learning Windows and DOS in the 90's, it's vital to your success.

Virtually all production servers run on a Linux (POSIX) server and all software languages have compilers and intepretters that are launched from a Linux shell.
So what exactly is the Linux or POSIX language?
POSIX is an open standard for the Linux operating system and command line applications.
This means if you learn a POSIX shell (such as Bash), you can use it on virtually any computer on the planet (even an Android Phone, or Mac laptop).

## Find files

The `find` command is one of the most sophisticated tools in your toolbox.

The `-prune` arg will ignore any file or directory named `.snapshot` and print all the paths that don't end in `~` to stdout with a null (0) character separating them, so you can pipe the output into a `cp` or move command.

```bash
find . -name .snapshot -prune -o \( \! -name '*~' -print0 \)
```

If you just want to find all the `*.dic` files (`hunspell` and `enchant` dictionaries), and ignore all your python virtual environments in `.venv` you can print them to your terminal, one path on each line:

```bash
find . -name .venv -prune -o \( -name '*.dic' -print \)
```

```text
./.local/share/TelegramDesktop/tdata/dictionaries/utf_helper.dic
./.local/share/TelegramDesktop/tdata/dictionaries/en_US/en_US.dic
./.config/enchant/en.dic
./.config/enchant/en_US.dic
./.config/libreoffice/4/user/wordbook/standard.dic
```


## Apt and dpkg

## Grep (full text search)

## Sed (search & replace)

## .config

### References

* Bash, zsh, tcsh, fish, dash, ash [shell comparison](https://www.howtogeek.com/68563/htg-explains-what-are-the-differences-between-linux-shells/)
* Bash [error codes demystified](https://www.redhat.com/sysadmin/exit-codes-demystified)


