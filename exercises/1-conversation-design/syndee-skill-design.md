# Designing a tool for Syndee chatbot
In this exercise, you'll be adding to the toolset of one of our chatbots, Syndee. 

## Background
Syndee ([https://syndee.ai](https://syndee.ai)) is Tangible AI's open project to help underserved individuals cope with thoughts that hold them back, so that they can go for their dreams. Syndee will always be free and open to everyone, and is community-driven - it grows through contributions of people like you! 

## Step 0: Learn more about conversation design and chatbots

Hear a beginner lecture about conversational design (Maria's): https://drive.google.com/drive/folders/16mRieOLhUE4wMVPeC0ry-TabYXtZp1cz?usp=sharing

Hear a beginner lecture about conversational design (Effective Copywriting for Chatbots): https://www.youtube.com/watch?v=49G58PQWO7w 

Task: Play with at least 2-3 bots from https://tangibleai.com/social-impact-chatbot-database/. Answer the following questions for yourself: 
* Which chatbot did you like the best?
* What is one thing you would improve about each chatbot?

## Step 1: Play with Syndee and choose your tool

* Explore Syndee ([link](https://syndee.ai/app.html)) and the different tools that already exist. 
* We have done some preliminary research and collected some ideas for Syndee's tools in [this repository](https://gitlab.com/tangibleai/syndee-documentation/-/tree/main/tools). All the tools that are not in `design-in-progress` and `development-completed` folders are up for grabs! 

## Step 2: Research the psychology behind the 
* If you chose a tool from among the concepts suggested in the repo, read the articles in "additional reading" section of the tool description, and try to do some additional research to understand the psychology behind it. If you're working on your own tool, find psychology research that supports your tool concept! We aim for all Syndee's tools to be based on scientific research.


## Step 3: Write a happy conversation
A "Happy conversation" is a script of the bot's conversation with the user if everything goes well. 

To keep your user engaged, limit your "happy conversations" to up to 8-10 bot turns and 8-10 user turns - about 10-15 turns in total.

We recommend using the Google Sheets template we use for conversational design. (You can see an example usage of the template [here](https://docs.google.com/spreadsheets/d/1gEET_NQblhfokO4lLZz4XHWbHvZj_d5WX8hEZXR22tU/edit?usp=sharing)) 

General notes about creating a successful Syndee tool:

1. Most Syndee tools are directly linked to a thought - you shouldn't ignore it! You can assume you have the text of the thought to use it in your dialogue. 
1. We should aim the tool to end with an **affirmation**, like other Syndee tools. Think how this affirmation can leverage the information users gives the bot during the conversation. 
1. Asking a lot of questions in a row can make the user feel interrogated. If you need to ask several questions to make the tool work, think how you can mix them with providing information and value to the user.
1. If you need to explain certain concepts to a user, feel free to use Syndee as an example! You can create a fictional story to illustrate your ideas. 
1. Remember to keep an empathetic and caring tone throughout your conversation! 

## Step 4: Simulate the conversation with a friend
Before you implement the tool in the chatbot, it might be a good idea to try and simulate your chatbot conversation with a friend, colleague, spouse or anyone who wants to support you on your conversational design journey! 

The idea is simple - use the script you create and any messaging app (such as Telegram, Whatsapp, or Slack) to see how your conversation goes when a real human participates in it. Play the bot's role (if you use Telegram/whatsapp web, you can copy-paste from your script) and if your user chooses an option you haven't foreseen, do your best to improvise! We assure you you will learn loads from this exercise. 

## Step 5: Complete the skill design
Once the flow of your bot is clear, try to create a few branches that deal with most common deviations from the happy script. If your skill is starting to look complicated, it might be a good idea to create a flow outline - a visual chart summarizing the main steps of the flow using tools like [draw.io](https://app.diagrams.net/). Don't worry about covering all the cases - this is not the final version, and belive us you're going to do so many changes during the user interaction.  

## Step 6: Build your prototype! 
In your next step, you'll be building your skill on the Landbot platform. Ask Maria for Landbot credentials, and soon your skill would come to life! 




