# Kubernetes

### CLI-History

Install and start minikube so that it will download and start all the containers it needs.

```bash
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
minikube start
minikube kubectl -- get po -A
```
```text
AMESPACE              NAME                                         READY   STATUS    RESTARTS      AGE
default                nginx-depl-6777bffb6f-jmdsn                  1/1     Running   0             30m
kube-system            coredns-5dd5756b68-5vg9f                     1/1
```

Since `kubectl` is install internally to the minikube VM you want to create an alias to use the `kubectl` command directly.

```bash
alias kubectl='minikube kubectl -- '
echo "alias kubectl='minikube kubectl -- '" >> ~/bin/.bash_aliases
```

Now you can create a boilerplate deployment yaml file:

```bash
mkdir kubenetes-exercise
cd kubernetes-exercise
kubectl create deployment nginx-depl --image=nginx
```

You can check out all the containers running in your minikube with the `get po` command.

```bash
kubectl get po -A
```
```text
AMESPACE              NAME                                         READY   STATUS    RESTARTS      AGE
default                nginx-depl-6777bffb6f-jmdsn                  1/1     Running   0             30m
kube-system            coredns-5dd5756b68-5vg9f                     1/1     Running   0             38m
kube-system            etcd-minikube                                1/1     Running   0             38m
kube-system            kube-apiserver-minikube                      1/1     Running   0             38m
kube-system            kube-controller-manager-minikube             1/1     Running   0             38m
kube-system            kube-proxy-bch47                             1/1     Running   0             38m
kube-system            kube-scheduler-minikube                      1/1     Running   0             38m
kube-system            storage-provisioner                          1/1     Running   1 (37m ago)   38m
kubernetes-dashboard   dashboard-metrics-scraper-7fd5cb4ddc-fm827   1/1     Running   0             32m
kubernetes-dashboard   kubernetes-dashboard-8694d4445c-j5kbk        1/1     Running   0             32m
```

Check out the deployment that you just created:

```bash
kubectl get deployment
```
```text
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
nginx-depl   1/1     1            1           29m
```

You can also check out the pods associated with that deployment:


```bash
kubectl get pod
```
```text
NAME                          READY   STATUS    RESTARTS   AGE
nginx-depl-6777bffb6f-jmdsn   1/1     Running   0          36m
```

Duplicates or replicas of a k8s pod are created by a load balancer (deployment) automatically, so you won't manage the replicaset directly.
But sometimes you just want to see how many replicas are running and maybe interfere with the normal k8s management of those replicas.

```
kubectl get replicaset
```

If you don't want to use `vim` to edit your Kubernetes deployment config files (`yaml` format) then you want to change the default editor used by minikube.
I set mine to use Sublime Text:

```bash
echo 'export KUBE_EDITOR=subl' >> ~/.bashrc
```

kubectl edit deployment nginx-depl
KUBE_EDITOR=subl
kubectl edit deployment nginx-depl
ls -hal
rm minikube_latest_amd64.deb 
workon team
export KUBE_EDITOR=subl
kubectl edit deployment nginx-depl
mkdir exercises/4-kubernetes
cd exercises/4-kubernetes/
```


### References

1. [YouTube Kubernetes course by TechWorld with Nana](https://www.youtube.com/watch?v=X48VuDVv0do)
1. [Install minikube and kubectl on Linux](https://minikube.sigs.k8s.io/docs/start/)
1. [KUBE_EDITOR setting](https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-tanzu/GUID-DC2BB6E0-A327-4DB8-9A87-5F3376E70033.html)
1. [Your local Kubernetes Dashboard](http://127.0.0.1:42205/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/workloads?namespace=default)