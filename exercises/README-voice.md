# README-voice.md


Here are some TTS (speech synthesis) APIs you could use to generate voice output from your

- name, pricing,                     freeness, quality, open source, self-hostable (private)
- Huggingface SpeechT5, free with rate limits, 5,       5,                 5,                       5, footnote:[Tutorial on HuggingFace (https://huggingface.co/blog/speecht5)] footnote:[Demo on HuggingFace (https://huggingface.co/spaces/Matthijs/speecht5-tts-demo)]
- Microsoft TTS, 500k char/mo
- Google TTS, 1M chars/mo free,             4,       0,                 0,                       1, footnote:[Intro to Google TTS for Python on GitHub (https://github.com/GoogleCloudPlatform/cloud-shell-tutorials/blob/master/ml/cloud-tts-intro/tutorial.md)]
- Amazon Poly, 5M char/mo,
- OpenTTS, no free API available,           2,       4,           4,                       5,
- Coqui TTS, only 30 minutes free,          3,       ?,          0,


Here are 3 open source STT libraries you could host on your own server.

[[listing-tts-wer]]
.TTS word error rate
[source,text]
----
|             | AI   | Phone   | Meeting   | Video   | Finance   | Mean   |
|:------------|:-----|:--------|:----------|:--------|:----------|:-------|
| Kaldi       | 66%  | 78%     | 54%       | 69%     | 35%       | 60%    |
| wav2vec 2.0 | 33%  | 41%     | 39%       | 26%     | 17%       | 31%    |
| Whisper     | 6%   | 20%     | 19%       | 9%      | 5%        | 12%    |
----

Wav2Vec2 is built into PyTorch (`torchaudio`) so this is probably your best bet for a voice assistant where you will likely need to fine tune the TTS model with your own data.footnote:[STT tutorial in PyTorch docs (https://pytorch.org/audio/stable/tutorials/speech_recognition_pipeline_tutorial.html)]
If you want a state-of-the-art accuracy in an open source model, then Whisper is your best bet.
You can download the latest Whisper models and even transcribe your own voice recordings using the Hugging Face Spaces page for Whisper. footnote:[Whisper demo on Hugging Face (https://huggingface.co/spaces/openai/whisper)]
In a resource constrained environment, the more efficient (but less accurate) Kaldi model may be all you need. footnote:[Kaldi source code on GitHub (https://github.com/kaldi-asr/kaldi)]

If you don't want to host the STT model yourself, there are several commercial services that give you an API that you can plug up to your chatbot.

.STT
[source,yaml]
----
- Microsoft STT, 5 hr/mo free, footnote:[tutorial (https://www.pragnakalp.com/speech-recognition-speech-to-text-python-microsoft-azure-aws-houndify/)]
- Google ASR, ?
- Amazon Transcribe, 1 hour/mo free
- Mozilla Deep Speech, self hosted, footnote:[Docs on GitHub (https://github.com/mozilla/DeepSpeech)]
- Kaldi, self-hosted, footnote:[Docs on GitHub (http://kaldi-asr.org/doc/index.html)]
- Houndify, 100 requests/mo
- OpenVINO
----
