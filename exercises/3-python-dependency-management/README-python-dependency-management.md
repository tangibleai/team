# Python Dependency management

If you've came up with the concept of _hoisting_ which means to optimize the javascript dependency tree by eliminating unused dependencies.
Similarly the Debian package manager `apt` issues warnings to help you manually optimize your `apt` package dependencies.
And in Python there is a tool called `pipdeptree` (https://pypi.org/project/pipdeptree/) that works a little like `pip freeze` but provides the dependency tree instead of an exhausted list of the Python packages installed in your current Python environment.

Here's an example for running `pipdeptree` on the Convo Python+Django+Svelte+Flowbite project.

#### _`pipdeptree`_
```bash
django-cors-headers==4.3.1
├── asgiref [required: >=3.6, installed: 3.7.2]
└── Django [required: >=3.2, installed: 4.2.8]
    ├── asgiref [required: >=3.6.0,<4, installed: 3.7.2]
    └── sqlparse [required: >=0.3.1, installed: 0.4.4]
django-debug-toolbar==4.2.0
├── Django [required: >=3.2.4, installed: 4.2.8]
│   ├── asgiref [required: >=3.6.0,<4, installed: 3.7.2]
│   └── sqlparse [required: >=0.3.1, installed: 0.4.4]
└── sqlparse [required: >=0.2, installed: 0.4.4]
django-extensions==3.2.3
└── Django [required: >=3.2, installed: 4.2.8]
    ├── asgiref [required: >=3.6.0,<4, installed: 3.7.2]
    └── sqlparse [required: >=0.3.1, installed: 0.4.4]
django-rangefilter==0.9.1
└── Django [required: Any, installed: 4.2.8]
    ├── asgiref [required: >=3.6.0,<4, installed: 3.7.2]
    └── sqlparse [required: >=0.3.1, installed: 0.4.4]
django-rest-framework==0.1.0
└── djangorestframework [required: Any, installed: 3.14.0]
    ├── Django [required: >=3.0, installed: 4.2.8]
    │   ├── asgiref [required: >=3.6.0,<4, installed: 3.7.2]
    │   └── sqlparse [required: >=0.3.1, installed: 0.4.4]
    └── pytz [required: Any, installed: 2023.3.post1]
Pillow==10.1.0
pip==23.3.1
pipdeptree==2.13.1
python-dotenv==1.0.0
setuptools==68.1.2
wheel==0.41.2
whitenoise==6.6.0
```

#### _`(.venv) hobs@minty:~/code/tangibleai/community/convo$ pip freeze`_
```bash
asgiref==3.7.2
Django==4.2.8
django-cors-headers==4.3.1
django-debug-toolbar==4.2.0
django-extensions==3.2.3
django-rangefilter==0.9.1
django-rest-framework==0.1.0
djangorestframework==3.14.0
Pillow==10.1.0
pipdeptree==2.13.1
python-dotenv==1.0.0
pytz==2023.3.post1
sqlparse==0.4.4
whitenoise==6.6.0
```

After Nov 1, 2023 (pip version 23.3.1 and Python 3.11) `pip` got a lot faster.
But with these newer versions of `pip` you may see warning messages about "invalid Python package names" for paths such as `**/site-packages/-jango/` or `**/site-packages/-jango/`.
Package names that begin with tilde "~" or dash "-" cannot be imported and are not allowed in Python.
These directories are temporary directories created by `pip` whenever you run `pip install` for a package like Django.
Normally `pip` will automatically rename or delete this temporary directory after the installation has succeeded.
But if you cancel the `pip install` command with *[CTRL]-[C]* or if the download or build fails for some reason, then that file will still be there.
So it is safe to delete any of the `site-packages/-*` directories that you find.
That will remove the warning messages complaining about invalid Python package names. This [Stack Overflow answer](https://stackoverflow.com/a/64939049/623735) provides more detail.

### Side note

If you tried to use `tree` to find all the installed packages, you'd get even worse results than if you used `pip freeze`.

#### _`tree .venv/lib/python3.11/site-packages/ -d -L 1`_
```bash
├── asgiref
├── asgiref-3.7.2.dist-info
├── corsheaders
├── debug_toolbar
├── _distutils_hack
├── django
├── Django-4.2.8.dist-info
├── django_cors_headers-4.3.1.dist-info
├── django_debug_toolbar-4.2.0.dist-info
├── django_extensions
├── django_extensions-3.2.3.dist-info
├── django_rangefilter-0.9.1.dist-info
├── django_rest_framework-0.1.0.dist-info
├── djangorestframework-3.14.0.dist-info
├── dotenv
├── empty
├── PIL
├── Pillow-10.1.0.dist-info
├── Pillow.libs
├── pip
├── pip-23.3.1.dist-info
├── pipdeptree
├── pipdeptree-2.13.1.dist-info
├── pkg_resources
├── __pycache__
├── python_dotenv-1.0.0.dist-info
├── pytz
├── pytz-2023.3.post1.dist-info
├── rangefilter
├── rest_framework
├── setuptools
├── setuptools-68.1.2.dist-info
├── sqlparse
├── sqlparse-0.4.4.dist-info
├── wheel
├── wheel-0.41.2.dist-info
├── whitenoise
└── whitenoise-6.6.0.dist-info```
```