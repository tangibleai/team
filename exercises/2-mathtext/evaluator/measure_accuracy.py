from pathlib import Path
import importlib
import pandas as pd

excludes = ['evaluator']
df = pd.read_csv('evaluator/consolidated_test_text2int.unique.csv')

paths = [p for p in Path.cwd().glob('*/') if p.is_dir() if p.name not in excludes]
names = [p.name for p in paths]

mathtext_modules = {}
for name in names:
    mathtext_modules[name] = importlib.import_module(f'{name}.mathtext')
    module = mathtext_modules[name]
    try:
        df[name] = [module.text2int(str(inp)) for inp in df['input']]
    except Exception as e:
        print(name, e)
        answers = []
        for i, (inp, out) in enumerate(df[['input', 'output']].values):
            print(i, inp, out)
            try:
                answers.append(module.text2int(str(inp)))
            except Exception as e:
                answers.append('error')
                print(f'{name}:{i:03d}: {repr(inp)}=>{repr(out)}\n  {e}')
        df[name] = answers


accuracies = {n: (df[n].apply(str) == df['output'].apply(str)).mean() for n in names}
