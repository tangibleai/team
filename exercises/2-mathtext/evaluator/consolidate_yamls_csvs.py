import pandas as pd
import yaml
from pathlib import Path
import re

FORMATS = {'c': 'csv', 'y': 'yaml', 't': 'doctest', 'd': 'doctest', 'j': 'json', 'x': 'xml'}


def stripquotes(s):
    s = str(s).strip()
    if s[0] in '"\'':
        if s[-1] == s[0]:
            s = s[1:-1]
        else:
            raise ValueError(f'Text: {s}\ncontains mismatched quotes')
    return s


def is_integer(s, linenum=None):
    try:
        return str(int(str(s)))
    except (TypeError, ValueError) as e:
        print(e)
        print(f"Skipping line {linenum}: {s}")


def dedupe(df, subset='input output'.split()):
    return df.drop_duplicates(subset=subset)


if __name__ == '__main__':
    errors = []

    # YAML
    paths = list(Path.cwd().glob('**/test_*.y*ml'))
    dfs = []
    for p in paths:
        dfs.append(pd.DataFrame(yaml.full_load(p.open())))
        dfs[-1]['source'] = p.parent.name
        dfs[-1]['format'] = FORMATS.get(p.suffix.strip('.')[0], 'yaml')
    df = pd.concat(dfs)

    # CSV
    paths = list(Path.cwd().glob('**/test_*.csv'))
    dfs = []
    for p in paths:
        header = [None, 0][int(p.open().readlines(1)[0].strip().endswith('output'))]
        dfs.append(pd.read_csv(p.open(), header=header))
        # dfs[-1] = dfs[-1][[0, 1]]
        dfs[-1].columns = 'input output'.split()
        dfs[-1]['source'] = p.parent.name
        dfs[-1]['format'] = FORMATS.get(p.suffix.strip('.')[0], 'csv')
    df = pd.concat([df] + dfs)

    # TEXT (doctest)
    paths = list(Path.cwd().glob('**/test_*.t*xt'))
    dfs = []
    for p in paths:
        test_examples = []
        lines = p.open().readlines()
        for i, line in enumerate(lines[:-1]):
            line = line.strip()
            if not line:
                continue
            match = re.match(r'>>>\ text2int\(([^\)]+)\)', line)
            if match:
                num_text, num_int = match.groups()[0].strip()[1:-1], stripquotes(lines[i + 1])
                if not is_integer(num_int, linenum=i):
                    errors.append(f'{p.parent.name}/{p.name}:{i}:\n{line}\n{lines[i+1]}')
                    print(errors[-1])
                    continue
                test_examples.append({
                    'input': stripquotes(num_text),
                    'output': stripquotes(num_int)
                })
        dfs.append(pd.DataFrame(test_examples))
        dfs[-1]['source'] = p.parent.name
        dfs[-1]['format'] = FORMATS.get(p.suffix.strip('.')[0], 'doctest')
    df = pd.concat([df] + dfs)

    # convert 1-length tuples and lists as well as floats and ints to str:
    df['input'] = df['input'].apply(lambda x: isinstance(x, (str, int, float)) * str(x) or x[0])
    df['output'] = df['output'].apply(stripquotes)

    df = df.reset_index()
    fn = 'consolidated_test_text2int.all.csv'
    df.to_csv(fn, index=False)
    print(f'RAW DATA: {fn}')
    print(df)
    fn = 'consolidated_test_text2int.unique.csv'
    df.to_csv(fn, index=False)
    print(f'UNIQUE EXAMPLES: {fn}')
    df_unique = dedupe(df)
    print(df_unique)
    print(f'******** {len(errors)} ERRORS *******************')
    for e in errors:
        print(str(e).strip())
        print()
    fn = 'consolidated_test_text2int.errors'
    with open(fn, 'w') as fout:
        fout.write('\n'.join(errors))
