def text2int(text):
    """ Convert an English str containing number words into an int

    >>> text2int("nine")
    9
    >>> text2int("forty two")
    42
    >>> text2int("1 2 three")
    123
    """
    >>> text2int("nine")
    9
    >>> text2int("forty two")
    42
    >>> text2int("1 2 three")
    123
    """
    words = {
        "zero": 0,
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9,
        "ten": 10,
        "eleven": 11,
        "twelve": 12,
        "thirteen": 13,
        "fourteen": 14,
        "fifteen": 15,
        "sixteen": 16,
        "seventeen": 17,
        "eighteen": 18,
        "nineteen": 19,
        "twenty": 20,
        "thirty": 30,
        "forty": 40,
        "fifty": 50,
        "sixty": 60,
        "seventy": 70,
        "eighty": 80,
        "ninety": 90
    }

    # Split the text into a list of individual words
    text_words = text.split()
    result = 0
    for word in text_words:
        if word in words:
            result += words[word]
        else:
            # If the word is not in our dictionary, it may be a number in digits
            try:
                result += int(word)
            except ValueError:
                # If it is not a number in digits, it is an invalid word and we raise an exception
                raise ValueError("Invalid word: " + word)
    return result
