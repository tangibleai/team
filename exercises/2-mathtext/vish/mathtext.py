from mathtext import nlutils


def text2int(text):
    """ Convert an English str containing number words into an int

    >>> text2int("nine")
    9
    >>> text2int("forty two")
    42
    >>> text2int("1 2 three")
    123
    """
    return nlutils.robust_text2int(text)
