# Nim

In this exercise you will get to know the `nim` (formerly known as `nimble`) programming language.

## Read up on Nim

- https://en.wikipedia.org/wiki/Nim_(programming_language)
- nim-lang.org

### Quiz

- Who created the nim language?
- Where does financial support for the language come from?
- What software license does the nim language and toolset use?
- Are there any power corporations that could corrupt the language for their own purposes in the near future?
- What are some advantages of nim compared to Python?
- What are some advantages of Python over nim?
- What are the latest features that have been incorporated into the development version of nim?
- What is the version number for the latest stable release of nim?
- What languages does nim compile too?
- Where is is possible to run an app built with nim?
- Is it possible to incorporate a nim function into Python as an extension or module?
- Is it possible to machine-translate pure Python code (no imports) into nim? 

## Install

### Ubuntu

```bash
sudo apt update
sudo apt upgrade
sudo apt install nim
```

That last command didn't work for me.
So I used another approach mentioned in the nim docs:

```bash
curl https://nim-lang.org/choosenim/init.sh -sSf | sh
echo 'export PATH=/home/hobs/.nimble:$PATH' >> ~/.bashrc
```

Alternatively you can manually edit your ~/.bashrc to include this addition to your PATH variable.  


```bash
nano ~/.bashrc
```

### Status.im


You might also enjoy install the Status.im messaging app and Ethereum wallet.
Visit the status.im/get page and right click on the Download link for your operating system (Linux!).
Then use your link below so you can get the latest version:

```bash
cd ~/code/
mkdir status/
curl -O https://status-im-files.ams3.cdn.digitaloceanspaces.com/StatusIm-Desktop-v0.3.0-beta-a8c37d.tar.gz
```


## Build a small app online

- https://narimiran.github.io/nim-basics/
- https://nim-lang.org/docs/tut1.html

### Confirm your binary installation

```bash
~$ tree ~/.nimble
/home/hobs/.nimble
├── bin
│   ├── choosenim
│   ├── nim
│   ├── nimble
│   ├── nim-gdb
│   ├── nimgrep
│   ├── nimpretty
│   ├── nimsuggest
│   └── testament
└── pkgs
```

## Advanced

Would you like to have access to the latest and greatest nim language features and standard libraries?
Thinking about contributing to the documentation or comments or help text within the compiler?
If so then you want to install the nim compiler tools from source.

### Install from source

Find the download link for the latest version of nim source code at https://nim-lang.org/download/

```bash
curl -O https://nim-lang.org/download/nim-1.6.6.tar.xz
tar -xvf ./nim-1.6.6.tar.xz
cd nim-1.6.6/
ls -hal
sh build.sh
bin/nim c koch
./koch boot -d:release
./koch tools
./koch tools
```


### Confirm your installation from source

Make sure your PATH variable is set up correctly.
You 

```bash
~$ ls -hal ~/.nimble/bin
choosenim
nim
nimble
nim-gdb
nimgrep
nimpretty
nimsuggest
testament
```

```
~$ which choosenim
~/.nimble/bin/choosenim

~$ which nim
~/code/other-peoples/nim/nim-1.6.6/bin/nim
```
