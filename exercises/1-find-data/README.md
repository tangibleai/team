# 1-find-data

The first step in any Data Science or Machine Learning project is to find data.
When you're getting started **benchmark** datasets are especially helpful, because the you can find examples the best possible models (after you've exhausted all your own ideas).

You can find well-defined problems and benchmark datasets on paperswithcode.com and kaggle.com.
The Hugging Face `transformers` package and the `pytorch` package both have really helpful datasets and problems in their `examples/` directories.
Google and AWS provide public S3 buckets that contain large datasets for computer vision and NLP.

If you want to jump right into feature engineering, there's a Jupyter Notebook for prediction Housing Prices at [./predict-housing-prices.ipynb](./predict-housing-prices.ipynb).

## Some Tangible AI dataset catalogs:

* [qary CLI repository](gitlab.com/tangibleai/community/qary/):
    * `src/qary/data/datasets.yml`
* [nlpia2 repository](gitlab.com/tangibleai/community/nlpia2/):
    * `src/nlpia2/data/datasets.yml`
    * `src/nlpia2/data/`
* [team repository](gitlab.com/tangibleai/community/team/):
    * `exercises/2-data/resources-datasets-data-sources.yml`

## Public dataset catalogs:

You can start at the top of this list and work your way down to find almost any ML dataset on any topic.

1. https://huggingface.co/ 
A great collection of various NLP datasets and models, but with a focus on NLP and not much new uses for NLP and datasets that do not really have to do with NLP. Has a great and easy to use interface.

2. https://kaggle.com/
A great diverse collection of datasets and models with its Kaggle kernels/notebooks on all topics like Computer Vision and Natural Language Processing and time series predictions.

3. https://github.com/awesomedata/awesome-public-datasets
Huge catalog of public datasets arranged by topic and problem type (by `awesomedata` on GitHub).

4. https://data.gov/ 
A collection of high quality US government collected datasets.

5. https://paperswithcode.com/ 
A collection of various research papers in the overall area of computer science and AI.

6. https://arxiv.org/
A collection of basically every research paper that exists from many different fields. Not well organized as in there is no way to search for datasets or models so you have to visit each paper that looks like it might have what you need.

7. https://opendata.cern.ch/
A collection of datasets made by CERN (a nuclear research organization)

8. https://datasetsearch.research.google.com/
A way to search datasets by Google.

9. https://datacatalog.worldbank.org/home
A collection of economic data by World Bank.

## Specialized datasets

### Bigdata
- 1TB free limit https://cloud.google.com/bigquery/public-data
- Amazon/AWS public datasets on S3?

### Scraped NLP corpora (not for commercial use):
- https://annasarchive.org
- https://z-lib.is/

### Specialized data
- api for paperswithcode.com
- arxiv.org (scrapable academic articles)
- github/gitlab api to retrieve commit messages and code changes on popular repos
- api? https://stats.wikimedia.org/
- NLP data? https://www.bls.gov/data/
- api? https://www.seattle.gov/cityarchives
- scrapeable? https://newspaperarchive.com/search/publication/
- stack overflow questions?
- wikipedia edit history?
- google ngram viewer for article names?
- historical reddit archives

### Low priority

* https://www.tensorflow.org/datasets
Tensorflow maintains some toy problem datasets such as MNIST (**use pytorch instead**).
