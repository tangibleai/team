from tqdm import tqdm
import gzip
import pandas as pd
from pathlib import Path
import networkx as nx
import plotly.graph_objects as go

"""
Entity: Canonical name of the entity part of the 'entity->relation->value' triple. NOT the literal string of NL seen by NELL in the text
Relation: The canonical relation name between the entity and value. Category relations are named "generalizations".
Value: Canonical name of the object or value in the 'entity->relation->value' triple. For category relations, this is the name of the category, otherwise it's an entity (noun phrase).
Iteration of Promotion: The point in NELL's life at which this category or relation instance was promoted to one that NELL beleives to be true. This is a non-negative integer indicating the number of iterations of bootstrapping NELL had gone through.
Probability: A confidence score for the belief. Note that NELL's scores are not actually probabilistic at this time.
Source: A summary of the provenance for the belief indicating the set of learning subcomponents (CPL, SEAL, etc.) that had submitted this belief as being potentially true.
Entity literalStrings: The set of actual textual strings that NELL has read that it believes can refer to the concept indicated in the Entity column.
Value literalStrings: For relations, the set of actual textual strings that NELL has read that it believes can refer to the concept indicated in the Value column. For categories, this should be empty but may contain something spurious.
Best Entity literalString: Of the set of strings in the Entity literalStrings, column, which one string can best be used to describe the concept.
Best Value literalString: Same thing, but for Value literalStrings.
Categories for Entity: The full set of categories (which may be empty) to which NELL belives the concept indicated in the Entity column to belong.
Categories for Value: For relations, the full set of categories (which may be empty) to which NELL believes the concept indicated in the Value column to belong. For categories, this should be empty but may contain something spurious.
Candidate Source: A free-form amalgamation of more specific provenance information describing the justification(s) NELL has for possibly believing this category or relation instance. 
"""

DATA_DIR = Path.home() / '.nlpia2-data' / 'nell'
DATA_DIR.mkdir(exist_ok=True, parents=True)
DEFAULT_PATH = DATA_DIR / 'NELL.08m.1115.esv.csv.gz'


def read_nell_tsv(path=DEFAULT_PATH, total=None):
    """ Read 13-column TSV containing facts/knowledge for a NELL triple, return DataFrame

    entity -> relation -> value(object)

    This will sometimes work (slowly, invisibly):    
    df = pd.read_csv(
        'http://rtw.ml.cmu.edu/resources/results/08m/NELL.08m.1115.esv.csv.gz',
        encoding='latin', sep='\t')
    """
    lines = []
    with gzip.open(path) as fin:
        for line in tqdm(fin, total=total):
            line = line.decode('latin')
            # print(i, len(line.split('\t')))
            lines.append(line.split('\t'))
    return pd.DataFrame(lines,
        columns=('entity relation value iteration prob source entities values '
            'best_entity_str best_value_str entity_categories value_categories '
            'candidate_source').split())


def df_to_nx(df, columns='entity relation value prob'.split()):
    triples = df[columns].T.values
    G = nx.DiGraph()
    # G.add_nodes_from(triples[0])
    # G.add_nodes_from(triples[2])
    # if len(columns) > 3:
    #     attrs = dict(zip(columns[3:], triples[3:]))
    G.add_edges_from(zip(triples[0], triples[2]), relation=triples[1])
    return G


def filter_triples_df(df, entities=None, relations=None, max_nodes=None, max_edges=None):
    return df


def nx_plotly(df, filter_kwargs=None):
    """ Plot a scatter plot of nodes in a NetworkX graph object """
    if filter_kwargs:
        df = filter_triples_df(df, **filter_kwargs)
    edge_trace = plotly_edge_trace(df)
    node_trace = plotly_node_trace(df)


def plotly_edge_trace(G, width=0.5, color='#888', hoverinfo='none', mode='lines', **scatter_kwargs):
    edge_x = []
    edge_y = []
    for edge in G.edges():
        x0, y0 = layout[edge[0]]
        x1, y1 = layout[edge[1]]
        edge_x.append(x0)
        edge_x.append(x1)
        edge_x.append(None)
        edge_y.append(y0)
        edge_y.append(y1)
        edge_y.append(None)

    return go.Scatter(
        x=edge_x,
        y=edge_y,
        line=dict(width=width, color=color),
        hoverinfo=hoverinfo,
        mode=mode
        **scatter_kwargs)


DEFAULT_NODE_MARKER = dict(
    showscale=True,
    # colorscale options
    #'Greys' | 'YlGnBu' | 'Greens' | 'YlOrRd' | 'Bluered' | 'RdBu' |
    #'Reds' | 'Blues' | 'Picnic' | 'Rainbow' | 'Portland' | 'Jet' |
    #'Hot' | 'Blackbody' | 'Earth' | 'Electric' | 'Viridis' |
    colorscale='YlGnBu',
    reversescale=True,
    color=[],
    size=10,
    colorbar=dict(
        thickness=15,
        title='Node Connections',
        xanchor='left',
        titleside='right'
        ),
    line_width=2
    )


def plotly_node_trace(G, marker=DEFAULT_NODE_MARKER): 
    node_x = []
    node_y = []
    for node in G.nodes():
        x, y = layout[node]
        node_x.append(x)
        node_y.append(y)
    node_trace = go.Scatter(
        x=node_x, y=node_y,
        mode='markers',
        hoverinfo='text',
        marker=marker)
    return node_trace


if __name__ == '__main__':
    df = read_nell_tsv(total=3_000_000)
    G = df_to_nx(df)
    G.size()
dfsmall.shape
Gsmall = df_to_nx(dfsmall)
layout = nx.circular_layout(G) # draw graph
nx.draw(G, layout, node_color='b', width=2, with_labels=True)  # draw edge attributes
dftiny = df.iloc[:200]
Gtiny = df_to_nx(dftiny)
layout = nx.circular_layout(Gtiny) # draw graph
nx.draw(Gtiny, layout, node_color='b', width=2, with_labels=True)  # draw edge attributes
plt.show()
from matplotlib import pyplot as plt
plt.show()
layout = nx.circular_layout(Gtiny) # draw graph
nx.draw(Gtiny, layout, node_color='b', width=2, with_labels=True)  # draw edge attributes
plt.show()
layout = nx.spring_layout(Gtiny)
nx.draw(Gtiny, layout, node_color='b', width=2, with_labels=True)
plt.show()
Gbig = G
G = Gtiny

DEFAULT_ANNOTATION = dict(
            text="_Natural Language Processing in Action_ - [NLPiA2](https://pypi.org/project/NLPiA2)",
            showarrow=False,
            xref="paper",
            yref="paper",
            x=0.005, y=-0.002 
            )


def plotly_graph(edge_trace, node_trace, path=None,
        title='Network (Graph)',
        titlefont_size=16,
        showlegend=False,
        hovermode='closest',
        margin=dict(b=20,l=5,r=5,t=40),
        annotations=[DEFAULT_ANNOTATION],
        xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
        yaxis=dict(showgrid=False, zeroline=False, showticklabels=False)):
    fig = go.Figure(data=[edge_trace, node_trace],
        layout=go.Layout(
            title='Network (Graph)',
            titlefont_size=16,
            showlegend=False,
            hovermode='closest',
            margin=dict(b=20,l=5,r=5,t=40),
            annotations=[ dict(
                text="Python code: <a href='https://plotly.com/ipython-notebooks/network-graphs/'> https://plotly.com/ipython-notebooks/network-graphs/</a>",
                showarrow=False,
                xref="paper", yref="paper",
                x=0.005, y=-0.002 ) ],
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False))
            )
    if path:
        path = Path(path)
        if path.is_file():
            path.open('a').write(fig.to_html(html=False))
        else:
            path.open('a').write(fig.to_html())
    if show:
        fig.show()
        # !firefox knowledge.plotly.html
    return fig.to_html()

isethics = df['entities'].apply(lambda x: 'AI ethics' in ' '.join(x))
df[isethics].shape
isethics = df['entities'].apply(lambda x: 'AI' in ' '.join(x))
df[isethics].shape
df['joined_entities'] = df['entities'].apply(lambda x: ' '.join(x))
df.head()
isethics = df['entities'].apply(lambda x: 'ai ethics' in x)
df[isethics].shape
isethics = df['entities'].apply(lambda x: 'ai' in x)
df[isethics].shape
isethics = df['entity'].apply(lambda x: 'concept:ai' in x)
df[isethics].shape
isethics = df['entity'].apply(lambda x: startswith('concept:ai:'))
isethics = df['entity'].apply(lambda x: x.startswith('concept:ai:'))
df[isethics].shape
isethics = df['entity'].apply(lambda x: x.startswith('concept:ai'))
df[isethics].shape
df[isethics].head()
df.drop(columns=['joined_entities'])
df = _
df.head()
df.head().T
df.drop(index=0)
df = df.drop(index=0)
df
pd.options.display.max_columns = 100
df.head()
df.head().T
df[isethics].head().T
isethics = df['entity'].apply(lambda x: 'artificial' in x)
df[isethics].head().T
isethics = df['entity'].apply(lambda x: 'artificial_intelligence' in x)
df[isethics].head().T
isethics = df['entity'].apply(lambda x: ':artificial_intelligence' in x)
df[isethics].head().T
