## for plotting
import matplotlib.pyplot as plt  #3.3.2
import seaborn as sns  #0.11.1

from nlpia2_wikipedia import wikipedia as wiki

import spacy  #3.5.0
from spacy import displacy
import textacy  #0.12.0

## for graph
import networkx as nx  # 3.0 (also pygraphviz==1.10)

from grounder.spacy_language_model import nlp


# import plotly.graph_objs as go  # 5.1.0

## for timeline
# import dateparser # 1.1.7


wiki.set_lang('en')
from nlpia2.spacy_language_model import nlp
nlp.name
wiki.search('large language models'
)
page = wiki.page('large language model')
page.text
page.content
page.content.splitlines()
print(page.content)
print(page.content[:1000])
print(page.content[:2000])
print(page.content[:1500])
print(page.content[:1550])
print(page.content[:1510])
text = page.content[:1510]
print(text)
text
def tag_sentence(sentence, tags='pos_ dep_'.split()):
    if isinstance(sentence, str):
        sentence = nlp(sentence)
    tokens = [t for t in sentence]
    if not tags or isinstance(tags, str):
        if tags in ('all', '*', '', None):
            tags = [
                label for label in dir(tokens[0]) 
                if (label.endswith('_') and not label[0] =='_') or label == label.strip('_')
                ]
        else:
            tags = tags.split()
    tags = ['text'] + list(tags)
    return pd.DataFrame(
        [[getattr(tok, tag) for tag in tags] for tok in tokens],
        columns=tags)
sentences = list(nlp(text).sents)
sentences[0]
sent = sentences[0]
tag_sentence(sent)
import pandas as pd
tag_sentence(sent)
sent.entities
sent.ents
sent.ents_
sent.ents
def extract_entities_spacy(docs, label=None):
    ents = []
    for i, d in enumerate(docs):
        for e in d.ents:
            if not label or e.label_ == label:
                ents += [[i, e.text, e.label]]
    return pd.DataFrame(ents, columns='sentence entity label'.split())
def extract_triples(sentences):
    """ Extract [subject verb object] triples (subject-predicate-object) using textacy"""
    triples = []
    for i, sent in enumerate(sentences):
        sent_triples = textacy.extract.subject_verb_object_triples(sent)  
        for t in sent_triples:
            triples.append(dict(
                id=i, text=t.text,
                ent="_".join(map(str, t.subject)),
                rel="_".join(map(str, t.verb)),  # predicate
                obj="_".join(map(str, t.object)),
                ))

    return pd.DataFrame(triples)
extract_entities(sentences)
extract_triples(sentences)
def extract_triples(sentences):
    """ Extract [subject verb object] triples (subject-predicate-object) using textacy"""
    triples = []
    for i, sent in enumerate(sentences):
        sent_triples = textacy.extract.subject_verb_object_triples(sent)  
        for t in sent_triples:
            print(t)
            # triples.append(dict(
            #     id=i, text=t.text,
            #     ent="_".join(map(str, t.subject)),
            #     rel="_".join(map(str, t.verb)),  # predicate
            #     obj="_".join(map(str, t.object)),
            #     ))

    return pd.DataFrame(triples)
extract_triples(sentences)
sent_triples = textacy.extract.subject_verb_object_triples(sent)
sent_triples
triples = list(sent_triples)
triples
sent_triples = textacy.extract.subject_verb_object_triples(sentences[1])
triples = list(sent_triples)
triples
for s in sentences:
    sent_triples = list(textacy.extract.subject_verb_object_triples(s))
    if sent_triples:
        break
sent_triples
sent_triples[0]
dir(sent_triples[0])
t = sent_triples[0]
t.subject
t.object
t.verb
t.index
t.index()
t.index('This')
t.count
t.count()
t.count('This')
t._asdict
t._asdict()
type(t.subject)
type(t.subject[0])
sent
s
def extract_triples(sentences):
    """ Extract [subject verb object] triples (subject-predicate-object) using textacy"""
    triples = []
    for i, sent in enumerate(sentences):
        sent_triples = textacy.extract.subject_verb_object_triples(sent)  
        for t in sent_triples:
            triples.append(t._asdict())

    return pd.DataFrame(triples)
extract_triples(sentences)
extract_triples?
extract_triples??
from textacy.extract import subject_verb_object_triples as get_tripples
get_tripples(s)
list(get_tripples(s))
get_tripples??
trips = get_tripples(nlp(text))
trips
list(trips)
get_tripples??
nodes = []
relations = []# iterate over the triples
for triple in triples:    # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))    nodes.append(node_subject)
    nodes.append(node_object)    # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))print(nodes)
# ['to_extract_SVO', 'I']print(relations)
# [('I', 'to_extract_SVO', {'action': 'am_going'})]
nodes = []
relations = []  # iterate over the triples
for triple in triples:  # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))
    nodes.append(node_subject)
    nodes.append(node_object)  # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))print(nodes)
nodes = []
relations = []  # iterate over the triples
for triple in triples:  # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))
    nodes.append(node_subject)
    nodes.append(node_object)  # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))print(nodes)
nodes = []
relations = []  # iterate over the triples
for triple in triples:  # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))
    nodes.append(node_subject)
    nodes.append(node_object)  # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))
print(nodes)
print(relations)
triples
triples = get_triples(nlp(text))
get_triples = get_tripples
triples = get_triples(nlp(text))
triples
list(triples)

nodes = []
relations = []  # iterate over the triples
for triple in triples:  # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))
    nodes.append(node_subject)
    nodes.append(node_object)  # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))
nodes
triples = get_triples(nlp(text))

nodes = []
relations = []  # iterate over the triples
for triple in triples:  # extract the Subject and Object from triple
    node_subject = "_".join(map(str, triple.subject))
    node_object  = "_".join(map(str, triple.object))
    nodes.append(node_subject)
    nodes.append(node_object)  # extract the relation between S and O
    # add the attribute 'action' to the relation
    relation = "_".join(map(str, triple.verb))
    relations.append((node_subject,node_object,{'action':relation}))# remove duplicate nodes
nodes = list(set(nodes))
nodes
relations
import matplotlib.pylab as plt
import networkx as nx
plt??
G = nx.DiGraph()
G.add_nodes_from(nodes)
G.add_edges_from(relations)
G
# extract the attribute 'action' from edges
edge_attribute = nx.get_edge_attributes(G, 'action')
edges, weights = zip(*edge_attribute.items())# resize figure
plt.rcParams["figure.figsize"] = [5, 2]
plt.rcParams["figure.autolayout"] = True# set figure layout
pos = nx.circular_layout(G)# draw graph
nx.draw(G, pos, node_color='b', width=2, with_labels=True)# draw edge attributes
nx.draw_networkx_edge_labels(G, pos,edge_attribute, label_pos=0.75 )
plt.show()
