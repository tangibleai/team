#

- login digital ocean account
- create droplet with litespeed wordpress from Marketplace
- install certbot
- create and install certificate with certbot
- add floating IP to droplet
- watch

## Create Cert with certbot

```bash
./certbot-auto certonly — manual — preferred-challenges=dns — email saurabh@erpnext.com — server https://acme-v02.api.letsencrypt.org/directory — agree-tos -d *.erpnext.xyz

## Critical

```bash
cat "0 */12 * * * root test -x /usr/bin/certbot -a \! -d /run/systemd/system && perl -e 'sleep int(rand(43200))' && certbot -q renew -->"
sudo nano /etc/cron.d/certbot
sudo cat /etc/cron.d/certbot
certbot renew --deploy-hook "systemctl restart lsws"
systemctl restart lsws
exit
ls /usr/local/lsws/conf
ls /usr/local/lsws/conf/cert
more /usr/local/lsws/conf/cert
```

Edit httpd_config.conf for litespeed web server to include both floating and temporary IP address from digital ocean.

#### _`grep 'proai\.org' -B1 -A1 /usr/local/lsws/conf/httpd_config.conf`_
```bash

```

Restart the lightspeed web server after editing the httpd_conf file:

```bash
systemctl restart lsws
```

To autorenew your certificates each month:

#### _`cat /etc/cron.d/certbot`_
```bash
# /etc/cron.d/certbot: crontab entries for the certbot package
#
# Upstream recommends attempting renewal twice a day
#
# Eventually, this will be an opportunity to validate certificates
# haven't been revoked, etc.  Renewal will only occur if expiration
# is within 30 days.
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# 0 */12 * * * root test -x /usr/bin/certbot -a \! -d /run/systemd/system && perl -e 'sleep int(rand(43200))' && certbot -q renew
0 */12 * * * root test -x /usr/bin/certbot -a \! -d /run/systemd/system && perl -e 'sleep int(rand(43200))' && certbot -q renew --deploy-hook "systemctl restart lsws"
```
