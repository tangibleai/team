# Databases

There are essentially three kinds of databases:

1: relational (SQL)
2: graph (no-SQL)
3: document store

This exercise will teach you a little SQL and help you take advantage of relational databases to speed up your work.

## Relational Databases

A relational database is just a collection of tabular data. 
You can think of it like a directory containing CSV files.
That's what you're going to learn about with these exercises.

The most common and useful database for managing datasets for data science.
Before your machine can learn anything from your data, it needs to have tabular data.
In the past you have loaded data from CSV files. 
This creates a database in the memory (RAM) of your PC.
But your RAM can only hold so much.

This is one of the main reasons to work with a database rather than "flat files" like the CSV files. 
Databases allow you to work with "big data", data that won't fit in the RAM of your machine.
And a database uses very clever algorithms to find the piece of data you need very very fast.
Things like full text search can run in milliseconds on a massive database containing all of Wikipedia, for example.
To do this with CSV files would take hours.

### Example Fake News Database

A conversation, text message dialog dataset might be organized into 3 tables.

- messages.csv
- users.csv
- sessions.csv

The `messages.csv` file might have columns for `id`, `userid`, and `text`.
The `users.csv` file might have columns for `id`, `email`, `username`.
Finally the `sessions.csv` might contain columns for `id`, `channel`, `start_datetime`.


The most common relational databases:

1. SQLite
2. PostgreSQL
3. MariaDB (MySQL's open source version)

SQLLite is used all over the place in Python. For example, your ipython `%history` [is stored in a SQLLite database](https://stackoverflow.com/a/55766379/623735).
There are lots of python packages that work with databases like SQLlite, but we're going to use a Firefox browser extension to make it even easier.
Make sure you have [Firefox installed](https://www.mozilla.org/en-US/firefox/new/). 
It's the only browser you can trust with your privacy anymore).

Once you have Firefox up and running install the [SQLite Manager plugin](https://addons.mozilla.org/en-US/firefox/addon/sqlite-manager-webext/) for Firefox.
Click on the `[OPEN]` button at the bottom of your browser and browse to your home directory to find your iPython history file here: `~/.ipython/profile_default/history.sqlite`.
