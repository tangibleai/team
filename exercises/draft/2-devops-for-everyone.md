# "Devops for Everyone"

The takeaway from the Human Centered AI Podcast interview with Jenn Gambel, chief data scientist at Very (verypossible.com) is that "Devops is for everyone": [Agile for ML Products](https://www.deus.ai/podcast/how-do-you-feel-about-agile-building-ml-products). What Jenn means by this is that everyone working on a customer facing product needs to know how to test and deploy their modifications to it. This includes setting up their local development environment where they can run a copy of the code or setting up a CI-CD pipeline for their part of the project. Even the product manager, project manager, data scientists, and data engineers need to know devops. It's the shared skill everyone needs to know. That's why we ask about bash and python during our interviews of new employees and interns at Tangible AI. 

Jenn Gambel spends most of the interview explaining her take on Agile. The aspects of Agile that she emphasizes are

1. "release" early and often
2. your feature spec should include instructions on how to test it
3. test driven development helps developers write better code
4. no requirements are set in stone, everyone can influence product decisions

These best-practices help your team iterate on your product quickly.
