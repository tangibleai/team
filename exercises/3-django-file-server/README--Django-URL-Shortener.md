# Django URL Shortener project

## References:

* [Build and deploy a Django url shortner](https://www.docker.com/blog/how-to-build-and-deploy-a-django-based-url-shortener-app-from-scratch/) - create hash of URLs and redirect within Django urls.py


### URLs to shorten:

* [Intro to NLP by Rachel Yang](https://www.youtube.com/live/fyo1mqu2RKI?si=oVNFwirvHfK0h4ch&t=1876)
* [Introduction to Stock Prediction](https://www.youtube.com/live/fyo1mqu2RKI?si=XIO8TBDiF2iWomhK&t=3866) by Sebastian Larson

### Short URLs to reshorten:

* 
