# DNS Records

## Host IP (A-record)

## Canonical name (CNAME)

## E-mail (MX)

TangibleAI.com e-mails (hosted by Google Workspace for business) were being rejected for gmail.com email recipients.
This is because our DNS records did not specify a DKIM key as a text record.
To fix this you need to go to your Google Workspace (admin.google.com).
Make sure you log in with your business e-mail address (hobson@tangibleai.com).
Then visit https://admin.google.com/u/2/ac/apps/gmail/authenticateemail and click "Start Authentication" and "Generate New Record".
Copy the TXT Record value to your DNS provider (in our case Digital Ocean and/or namecheap).