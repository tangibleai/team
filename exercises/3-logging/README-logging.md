# Logging

Python's logging framework is great for testing of web services or background processes when you don't have access to the terminal.

But logging usually sends error and warning messages to a file on the server where the Python process is running.
So for production web services it's better to use an external logging service such as PaperTrail (by Solar Winds) or Sentry.

But the free accounts on Sentry and PaperTrail usually run out of storage/transfer limits.
So we've disabled it for our Team on Render.com.
If you need to add it back, go to [Team Settings](https://dashboard.render.com/t/settings) on your Render Dashboard, or your [personal Render account settings](https://dashboard.render.com/u/settings), and scroll down to ["Log Streams" section](https://dashboard.render.com/t/tea-/settings#log-streams).

Sentry is probably your best bet for getting up to speed quickly on logging web services.
They have decent limits for their free tier (pricing).
And they have code snippets with your tokens ready to go for pasting into Javascript or Python.
Of course you'll probably want to protect your tokens.

## FOSS Self-hosting

### Graylog (Java)

- [github](https://github.com/Graylog2/graylog2-server)
- [server images](https://www.graylog.org/downloads/)
- [docs](https://community.graylog.org/)
- [installation docs](https://go2docs.graylog.org/5-0/downloading_and_installing_graylog/installing_graylog.html)

### Mezmo (nee LogDNA)

https://github.com/jettero/ldogger

### LogESP (Django)

https://github.com/dogoncouch/LogESP

### LogTail

[Render.com/docs/log-streams](https://render.com/docs/log-streams)

1. create account on logtail.com and enable TFA
2. "create source" on logtail with the type "Render"
3. copy the token for the logtail source () 
4. go to your Team Settings or Org Settings in your Render Dashboard
5. find Log Streams tab in lefthand drawer and create a log stream
6. paste the token from step 3 in the token for your new log stream
7. type in the endpoint url for this logstream: `in.logtail.com:6514`




## Syslog servers

NAME | OS | Free Trial | URL
---- | -- | ---------- | ---
Kiwi (SolarWinds) | Unix, Linux, Windows | 14 days | 
PRTG | Windows | 30 days | https://www.paessler.com/free_syslog_server
Syslog Watcher | Unix, Linux, Windows | Per request
The Dude | Linux, macOS, Windows   |  /
Visual Syslog Server | Unix, Linux  | /
Datagram | Windows Unlimited trial version
ManageEngine EventLog Analyzer | Linux, Windows  |  /
Icinga  |  Unix, Linux  | /
GrayLog  | Linux   |  /
WinSyslog  |   Windows  | /
Nagios  |  Linux, Windows  30 days
Splunkbase  |  Unix, Linux, Windows | 14 days
Progress WhatsUp Gold  |   Windows  | 14 days
Logstash | Unix, Linux, Windows | 14 days
Loggly   | Cloud-based  | 30 days
Site24x7 | Cloud-based  | 30 days
