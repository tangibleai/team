# Git Resources & Content

## Challenge

1. [ ] Create or log in to a GitLab account (don't click on any paid account upgrades)
2. [ ] Find an interesting open source project on gitlab.com (Here are [Tangible AI](#tangible-ai-repositories)'s).
3. [ ] Give it a star, and fork it to your gitlab account.
4. [ ] Clone your *fork* onto your local machine.
5. [ ] Edit the README file or a documentation string somewhere in the repository.
6. [ ] `git status`, `commit -am` and `push` your changes up to GitLab.
7. [ ] Create a **Merge Request** on GitLab.com for the changes you made to the documentation.

### Stretch Goal

1. [ ] Create a new project (repository) within your user account on [GitLab](https://gitlab.com)
2. [ ] Clone your new empty repository to your local machine to your `~/code/` directory
3. [ ] Create a `README.md` file on your local machine using [Sublime Text 3](http://www.sublimetext.com/3).
4. [ ] Edit `README.md` to describe a personal project you are working on including any URLs that helped you get started.
5. [ ] `git status`, `add`, `commit -am` and `push` the new `README.md` file to your repository.
6. [ ] Find the code and notes you need for this project on your local machine (use the `find` command if you can).
7. [ ] Use the `mv` command to put all your project files into this repository
8. [ ] Use `git status` and `add`, `commit`, `push` to get the files into your repository.
9. [ ] Confirm that everything is like you want it on GitLab.com.


## HowTo Videos

- [`git branch` and `qary` (15 min)](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-git-branch-qary-hobson-winston-30min-2021-02-05.mp4): `git branch`, `checkout -b` by Hobs & Winston
- [HowTo `git` (46 min)](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-git-config-editor-clone-team-repo-pull-add-push-una-hobson-2021-04-01.mp4): `git config editor` to switch `vim` to `nano` plus `add`, `push`, `pull` by Hobs & Una
- [HowTo `git clone` Part 1 (12 min)](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-install-sublime-text-and-start-new-project-mac-osx-add-ssh-key-to-gitlab-13min.mp4): new Sublime project (Project->Add Folder), `ssh-keygen` on Mac
- [HowTo `git clone` Part 2 (55 min)](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-install-git-clone-commit-push-pull-sublime-text-by-hobs-hanna-2020.mp4): `git clone`, `conda env update -f environment.yml`, `pip install -e`, `git push` and _Sublime Text 3_ by Hobs & Hanna

##  What is Git? 

Git was created by Linus Torvalds in a few weeks to help him manage all the code flowing into his Linux kernel development project.
He was personally curate 100s of merges a month, and the existing software version control systems (VCSes) were too slow.
At the time VCSes all relied on a central server to maintain the "ground truth" for all of the files.
That meant uploading and downloading files to compare them to each other.

So Torvalds architected a **distributed** version control system.
This allowed each and every developer to maintain a complete copy of the entire software repository history on their personal laptop.
This meant that uploads and downloads were only required to coordinate merges with other developers.
And he worked with the founders of GitHub and GitLab to help them create a web interface that would allow everyone to share their open source code with the world using this approach.

Here's a video with more information about `git`:

[@imageg01](https://www.youtube.com/watch?v=2ReR1YJrNOM)

## Tangible AI repositories

We host all our code on Git**Lab** rather than GitHub, because GitHub (now Microsoft) is decidedly antisocial.
Microsoft uses their inside advantage to train machine learning models that can write code, whether or not your license for your software allows that.
This is legal, questionable ethically, and certainly exploitative and extractive.

And GitLab is 100% open source!
Even their core product, the gitlab.com website, and all their busienss process documentation is open source.
This means you can download the code for gitlab.com onto your own server and host your own GUI for maintaining your own private code base for your company!
You can basically download their entire business model.
They are probably one of the most transparent and prosocial companies I've every heard of.
It's probably no mystery that they are not headquartered in the US, but rather in Helsinki.
And even the CEO works from home nearly 100% of the time.

TangibleAI also defaults to open, you can find all our open source repositories here: 
[TangibleAI git repository](https://gitlab.com/tangibleai)

- [qary](https://gitlab.com/tangibleai/qary): A virtual assistant to save the world
- [team](https://gitlab.com/tangibleai/qary): Training and onboarding documentation
- [NLPIA](https://gitlab.com/tangibleai/nlpia): _Natural Language Processing in Action_ code
- [Tanbot](https://gitlab.com/tangibleai/tanbot): Slack & email chatbot for the quarterly internship
- [django-api](https://gitlab.com/tangibleai/django-api): Dockerized recommendation engine API

## Installation

[Here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) are instructions for downloading and installing git (including a bash shell) on virtually any system... even Windoze.
