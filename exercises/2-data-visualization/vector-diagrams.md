# Drawing vectors (arrows) with  matplotlib (seaborn)

Drawing vectors to explain linear algebra is tricky.


#### 2D term frequency vector examples
```python
>>> from matplotlib import pyplot as plt
>>> import seaborn as sns

>>> palette = sns.color_palette("muted")  # <1>
>>> sns.set_theme()  # <2>

>>> vecs = pd.DataFrame([[1, 0], [2, 1]], columns=['x', 'y'])
>>> vecs['color'] = palette[:2]
>>> vecs['label'] = [f'vec{i}' for i in range(1, len(vecs)+1)]

>>> fig, ax = plt.subplots()
>>> for i, row in vecs.iterrows():
...     ax.quiver(0, 0, row['x'], row['y'], color=row['color'],
...         angles='xy', scale_units='xy', scale=1)
...     ax.annotate(row['label'], (row['x'], row['y']),
...         color=row['color'], verticalalignment='top'
...         )
>>> plt.xlim(-1, 3)
>>> plt.ylim(-1, 2)
>>> plt.xlabel('X (e.g. frequency of word "vector")')
>>> plt.ylabel('Y (e.g. frequency of word "space")')
>>> plt.show()
```
<1> Other pallets: pastel, bright, colorblind, deep, and dark.
<2> Possible themes: notebook, whitegrid, darkgrid
1. `<1>` Other pallets: pastel, bright, colorblind, deep, and dark.
2. `<2>` Possible themes: notebook, whitegrid, darkgrid

[#figure-2d-vectors, reftext={chapter}.{counter:figure}]
2D vectors
 [![image alt text](image URL link)](anchor link)

[![2D Vectors pointing from the origin (0, 0) to the right. Vec1 is pointed directly to the right with the head ending at the coordinates (1, 0). Vec2 points up and to the right with its head at (2, 1).](https://gitlab.com/hobs/nlpia-manuscript/-/raw/master/manuscript/images/ch03/2x1and1x0vectors.png)](https://gitlab.com/hobs/nlpia-manuscript/-/raw/master/manuscript/images/ch03/2x1and1x0vectors.png){ width=80% }

A vector diagram of Euclidean distance.

#### Vec1 minus Vec2 in 2D with matplotlib
```python
>>> from matplotlib import pyplot as plt
>>> import seaborn as sns

>>> palette = sns.color_palette("muted")
>>> sns.set_theme()  # ('notebook', 'whitegrid', palette)

>>> vecs = pd.DataFrame([
...     [0, 0, 1, 0],
...     [0, 0, 2, 1],
...     [2, 1, -1, -1]], columns=['x0', 'y0', 'x', 'y'])
>>> vecs['color'] = palette[:3] 
>>> vecs['label'] = [f'vec{i}' for i in range(1, len(vecs))] + [" "]
>>> vecs['ls'] = 'solid solid dashed'.split()
>>> fig, ax = plt.subplots()
>>> for i, row in vecs.iterrows():
...     ax.quiver(row['x0'], row['y0'], row['x'], row['y'], ec=row['color'],
            linewidth=2, fc='none',
...         angles='xy', scale_units='xy', scale=1, ls=row['ls'])
...     ax.annotate(row['label'], (row['x'], row['y']),
...         color=row['color'], verticalalignment='top'
...         )
>>> plt.xlim(-1, 3)
>>> plt.ylim(-1, 2)
>>> plt.xlabel('X (e.g. frequency of word "vector")')
>>> plt.ylabel('Y (e.g. frequency of word "space")')
>>> plt.show()
```

[![2D Vectors pointing from the origin (0, 0) to the right. Vec1 is pointed directly to the right with the head ending at the coordinates (1, 0). Vec2 points up and to the right with its head at (2, 1).](https://gitlab.com/hobs/nlpia-manuscript/-/raw/master/manuscript/images/ch03/2x1and1x0vectors.png)](https://gitlab.com/hobs/nlpia-manuscript/-/raw/master/manuscript/images/ch03/2x1and1x0vectors.png){ width=80% }

