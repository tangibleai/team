nlp = spacy.load("en_core_web_md")
import spacy
nlp = spacy.load("en_core_web_md")
>>> sentence = ('It has also arisen in criminal justice, healthcare, and '
...     'hiring, compounding existing racial, economic, and gender biases.')
sentence
>>> doc = nlp(sentence)
>>> tokens = [token.text for token in doc]
>>> tokens
>>> from collections import Counter
>>> bag_of_words = Counter(tokens)
>>> bag_of_words
bag_of_words
import pandas as pd
pd.Series(bag_of_words)
>>> bag_of_words.most_common(3)  # <2>
counts = pd.Series(dict(bag_of_words.most_common()))
counts
counts / counts.sum()
>>> sentence = "Algorithmic bias has been cited in cases ranging from " \
...     "election outcomes to the spread of online hate speech."
>>> tokens = [tok.text for tok in nlp(sentence)]
>>> counts = Counter(tokens)
>>> import requests
>>> url = ('https://gitlab.com/tangibleai/nlpia2/'
...        '-/raw/main/src/nlpia2/ch03/bias_intro.txt')
>>> response = requests.get(url)
response
bias_intro = response.content.decode()
bias_intro
docs = [nlp(s) for s in bias_intro.split_lines() if s.strip()]
docs = [nlp(s) for s in bias_intro.split('\n') if s.strip()]
docs

>>> counts = []
>>> for doc in docs:
...     counts.append(Counter([t.text.lower() for t in doc]))  # <2>
>>> df = pd.DataFrame(counts)
>>> df = df.fillna(0).astype(int)  # <3>
>>> df.head()
for i in range(len(df)): ((df.iloc[0] - df.iloc[i])**2).sum()**.5
dists = []
for i in range(len(df)):
    dist = ((df.iloc[0] - df.iloc[i])**2).sum()**.5
    dists.append(dist)
dists
df.shape
cosdists = []
for i in range(len(df)):
    cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
    cosdists.append(dist)
import numpy as np
cosdists = []
for i in range(len(df)):
    cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
    cosdists.append(dist)
dotprods = []
for i in range(len(df)):
    dotprod = df.iloc[0].dot(df.iloc[i])
    dotprods.append(dist)
dotprods
dotprods = []
for i in range(len(df)):
    dotprod = df.iloc[0].dot(df.iloc[i])
    dotprods.append(dotprod)
cosdists = []
for i in range(len(df)):
    cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
    cosdists.append(cosdist)
dotprods
df
df.head()
clear
df.head()
dotprods[:5]
dotprods
cosdists = []
for i in range(len(df)):
    cosdist = df.iloc[0].dot(df.iloc[i]) / np.linalg.norm(df.iloc[0]) / np.linalg.norm(df.iloc[i])
    cosdists.append(cosdist)
df.head()
cosdists[:5]
mkdir /home/hobs/code/tangibleai/team/exercises/1-nlpia-ch3-count-vectors
hist -o -p -f /home/hobs/code/tangibleai/team/exercises/1-nlpia-ch3-count-vectors/count-vectors-dot-product-cosine-similarity.hist.ipy
hist -f /home/hobs/code/tangibleai/team/exercises/1-nlpia-ch3-count-vectors/count-vectors-dot-product-cosine-similarity.hist.py
