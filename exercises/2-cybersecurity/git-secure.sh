#!/usr/bin/env bash

git status || git init

# Find the
while [[ ! -d .git ]] ; do
    cd ..
done

pre-commit --version || pip install pre-commit

pre-commit sample-config >> .pre-commit-config.yaml

pre-commit install
