# Ubuntu tips and tricks

Here are some things I needed to do to my Ubuntu installation to optimize my environment for software development.

In addition, here are some Linux setup guides you might want to check out:

* https://www.tecmint.com/initial-ubuntu-server-setup-guide/
*

## forensics

* Volatility - open source tool for dumping RAM to external drive (useful for network controllers/gateways) during an ongoing attack
* Wireshark - open source for dumpping network traffic
* Prometheus - hoover up log files?

## Monitoring

You should probably know about these log files and what's in them, in case you ever need to diagnose something [15 import log files](https://www.eurovps.com/blog/important-linux-log-files-you-must-be-monitoring/)

#### **_`/var/log/`_**
- `messages|syslog`: noncritical messages for startup applications
- `secure|auth.log`: user authentication
- `/var/log/boot.log`:


#### **_`$ tail /var/log/syslog`_**
```text
May 26 12:31:04 frm gnome-shell[2545]: Window manager warning: ... keysym 38 with keysym 38 (keycode 11).
May 26 12:31:11 frm systemd[1]: NetworkManager-dispatcher.service: Deactivated successfully.
May 26 12:32:53 frm systemd[1]: Started Run anacron jobs.
May 26 12:32:53 frm anacron[3857]: Anacron 2.3 started on 2022-05-26
May 26 12:32:53 frm anacron[3857]: Normal exit (0 jobs run)
May 26 12:32:53 frm systemd[1]: anacron.service: Deactivated successfully.
May 26 12:35:01 frm CRON[3897]: (root) CMD (command -v debian-sa1 > /dev/null && debian-sa1 1 1)
May 26 12:35:59 frm systemd[1]: Starting Cleanup of Temporary Directories...
May 26 12:35:59 frm systemd[1]: systemd-tmpfiles-clean.service: Deactivated successfully.
May 26 12:35:59 frm systemd[1]: Finished Cleanup of Temporary Directories.
```

#### **_`$ tail /var/log/dmesg`_**
```
[    4.968402] kernel: NET: Registered PF_ALG protocol family
[    5.005343] kernel: Bluetooth: RFCOMM TTY layer initialized
[    5.005355] kernel: Bluetooth: RFCOMM socket layer initialized
[    5.005363] kernel: Bluetooth: RFCOMM ver 1.11
[    6.639262] kernel: kauditd_printk_skb: 23 callbacks suppressed
[    6.639266] kernel: audit: type=1400 audit(1653592861.984:34): apparmor="STATUS" operation="profile_load" profile="unconfined" name="docker-default" pid=1122 comm="apparmor_parser"
[    6.943363] kernel: bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
[    6.945663] kernel: Bridge firewalling registered
[    7.073395] kernel: Initializing XFRM netlink socket
[    7.412639] kernel: rfkill: input handler disabled
```

#### **_`$ sudo tail /var/log/boot.log`_**
```
[  OK  ] Started LSB: automatic crash report generation.
[  OK  ] Finished resolvconf-pull-resolved.service.
[  OK  ] Started A high performance web server and a reverse proxy server.
         Starting Process error reports when automatic reporting is enabled...
[  OK  ] Started Thunderbolt system service.
[  OK  ] Started Dispatcher daemon for systemd-networkd.
[  OK  ] Started Modem Manager.
[  OK  ] Started GNOME Display Manager.
[  OK  ] Started Accounts Service.
[  OK  ] Started Disk Manager.
```

#### **_`$ tail /var/log/dmesg`_**
```
[    4.968402] kernel: NET: Registered PF_ALG protocol family
[    5.005343] kernel: Bluetooth: RFCOMM TTY layer initialized
[    5.005355] kernel: Bluetooth: RFCOMM socket layer initialized
[    5.005363] kernel: Bluetooth: RFCOMM ver 1.11
[    6.639262] kernel: kauditd_printk_skb: 23 callbacks suppressed
[    6.639266] kernel: audit: type=1400 audit(1653592861.984:34): apparmor="STATUS" operation="profile_load" profile="unconfined" name="docker-default" pid=1122 comm="apparmor_parser"
[    6.943363] kernel: bridge: filtering via arp/ip/ip6tables is no longer available by default. Update your scripts to load br_netfilter if you need this.
[    6.945663] kernel: Bridge firewalling registered
[    7.073395] kernel: Initializing XFRM netlink socket
[    7.412639] kernel: rfkill: input handler disabled
```

### Custom logging

You can log your own messages to a custom /var/log/custom_file_name.log using [this
reference](https://stackoverflow.com/questions/15045946/write-to-custom-log-file-from-a-bash-script).


```bash
BAT=BAT1
status=$(cat /sys/class/power_supply/BAT1/status)
datetime=$(date)
timestamp=$(date +"%Y-%m-%d_%H-%M-%S.%s")
rfctimestamp=$(date --rfc-3339=ns)
capacity=$(cat /sys/class/power_supply/$BAT/capacity)       # 95
charge=$(cat /sys/class/power_supply/$BAT/charge_now)       # 2511000
charge_full=$(cat /sys/class/power_supply/$BAT/charge_full) # 2617000)
voltage=$(cat /sys/class/power_supply/$BAT/voltage_now)     # 16686000
# can only use FUNCNAME and other system RAM variables if the script is defined within a function (e.g. bash_functions.sh)
# calling_funcname="${FUNCNAME[1]}"
echo "$timestamp battery:$status capacity:$capacity% charge:$charge/$charge_full voltage:$voltage" >> /var/log/battery_status.log
```

Also, `chmod +x ~/bin/log_battery_level.sh`
For testing you might also want to open up the permissions on the `/var/log/*.log` files:

```bash
sudo chmod a+rw /var/log/battery_status.log
```

If you want to check your battery every 5 minutes and shutdown when it reaches critical level you can create this crontab file:

#### _`sudo nano /etc/cron.d/suspend-if-battery-low`_
```bash
* * * * * root /home/hobs/bin/suspend_if_battery_low.sh
*/5 * * * * root /home/hobs/bin/log_battery_level.sh
```

OR

#### _`sudo nano /etc/cron.d/log-battery-level`_
```bash
*/5 * * * * root /home/hobs/bin/log_battery_level.sh
```

Either way, you should start seeing log entries like this showing up in `/var/log`:

#### _`cat /var/log/battery_status.log`_
```bash
2024-01-02_18-45-01.1704249901 battery:Charging capacity:95% charge:2511000/2617000 voltage:16672000
2024-01-02_18-50-01.1704250201 battery:Charging capacity:95% charge:2511000/2617000 voltage:16670000
2024-01-02_18-55-01.1704250501 battery:Charging capacity:95% charge:2511000/2617000 voltage:16667000
```

I have my bios set to stop charging at 95% charge, to improve the life of my battery.
Ideally I should write scripts that maintain the charge between 60% and 80% by alerting the user when the charger should be plugged in or unplugged.
It's probably not possible to modulate the internal power supply easily from bash.

## Zoom screenshare

Zoom screenshare won't work with the Wayland display manager.
Screenshare only works with the legacy Xorg/X11 display manager/server.

```bash
sudo apt-get install xorg openbox
sudo dpkg-reconfigure xorg
```

#### `sudo nano /etc/default/grub`
```bash
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_backlight=vendor intel_idle.max_cstate=1"
# GRUB_CMDLINE_LINUX=""
```

Make sure GRUB_CMDLINE_LINUX is commented out so the default is accepted

**DONT FORGET**: `sudo update-grub`

#### `nano /etc/gdm3/custom.conf`

```bash
[daemon]
# Uncomment the line below to force the login screen to use Xorg
WaylandEnable=false
```

### Anaconda

SEE ALSO: https://stackoverflow.com/a/76972747/623735

Download the Linux installer (shell script) from [Anaconda.com](https://www.anaconda.com/download).
If you have an encrypted home directory (typical for Linux Mint), then you will need to install Anaconda using sudo:


```bash
sudo bash $(ls -1 --sort=time ~/Downloads/Anaconda3-*-Linux-x86_64.sh | head -n 1) 
```

I got "Filea path too long" error on Linux Mint because I had an encrypted $HOME, but I was able to install Anaconda in `/root/` using `sudo`:

```bash
sudo bash ~/Downloads/Anaconda3-*.sh
```

But when I tried to copy it to my encrypted `$HOME` directory I had the same problem that [kricki](https://stackoverflow.com/users/12311771/kricki) had ('ERROR: File name too long').

So you should symlink to `/root/anaconda3` from `/opt/anaconda3` and make both `anaconda3` dirs read/write/execute/search-able by normal users:

```bash
sudo chmod -R ugoa+Xrw /opt/anaconda3
sudo chmod 755 /root/
sudo chmod 755 /root/anaconda3
sudo echo "Make sure **$USER** is you and not **root** before running chown"
sudo chown -R $USER:$USER /root/anaconda3
chmod -R ugoa+Xrw /root/anaconda3
sudo ln -s /root/anaconda3 /opt/anaconda3
sudo chmod 755 /opt/anaconda3
sudo chown $USER:$USER /opt/anaconda3
chmod ugoa+Xrw /opt/anaconda3
```

Now you should be able to add `conda` to your $PATH in `.bashrc` by running `conda init`:

```bash
/opt/anaconda3/bin/conda init
```

Now when you close your terminal and relaunch it, you should see `(base)` at the beginning of your prompt which indicates that you've automatically activated the base conda environment in your `.bashrc` file.

## TODO after fresh mint install

- [x] disable middle mouse button on touchpad
- [x] install meld git python3.10 vscode sublime
- [ ] install python3.9 (for anaconda install script?)
- [x] install zoom slack telegram
- [x] log into gmail (3) tutanota (1) outlook (SDCCD.edu)
- [x] log into sdccd intructure (Canvas)
- [x] log into zoom
- [x] clone tangibleai/community, hobs/bin, hobs/nlpia-manuscript
- [x] log firefox mozilla sync
- [x] symlink ~/bin to ~/code/hobs/bin/ (gitlab) and push/pull changes
- [x] link ~/.bashrc to ~/bin/bashrc.debian 
- [x] link ~/.bash_aliases to ~/bin/bash_aliases.sh
- [ ] log into slack
- [ ] log into telegram
- [ ] log into whatsapp/web
- [ ] log into Springboard
- [ ] install Mozilla VPN
- [x] make sure grub and BOOT options don't cause Intel processor (Wayland? or Bay View or < 2013 Pentium/Celeron/Atom?) Intel CPU bug (freeze duriing attempted idling/suspend)

### Python 3.9

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt update
sudo apt install -y software-properties-common
```
### mozilla VPN

```bash
sudo add-apt-repository ppa:mozillacorp/mozillavpn
sudo apt-get update
sudo apt-get install -y resolvconf mozillavpn
mozillavpn
```

## Security Holes

Ubuntu comes with a lot of bloatware that creates security vulnerabilities.
If you want Ubuntu to run on a server handling sensitive or personal data (including your laptop), you probably want to clean it up using some of the recommendations here:

https://www.tecmint.com/initial-ubuntu-server-setup-guide/

```bash
sudo apt update
sudo apt -y upgrade
sudo apt -y autoremove
sudo apt -y clean
```

Often you want a sudo user with a regular home directory:

```bash
sudo adduser me
sudo chage -d0 me
sudo usermod -a -G sudo me
````

Now log in as this new "me" user and create a password:

```bash
su - me
```

```bash
hostnamectl
```

```text
   Static hostname: hobson-framework
         Icon name: computer-laptop
           Chassis: laptop
        Machine ID: 0184974928894112abc779753ae9dc52
           Boot ID: ded0c8a2fee54ea9abe3e420374413ae
  Operating System: Ubuntu 20.04.3 LTS
            Kernel: Linux 5.13.0-30-generic
      Architecture: x86-64
```

For most servers you want to use short memorable, recognizable names.
I've seen people use the names of movies or superheros, but I like to have a list of [real world heroes](https://en.wikipedia.org/wiki/Lists_of_activists) or [technology pioneers](https://en.wikipedia.org/wiki/EFF_Pioneer_Award):

* [Assassinated Human Rights Activists](https://en.wikipedia.org/wiki/List_of_assassinated_human_rights_activists
, and I check them off as I create servers)
* [Lists of Activists]https://en.wikipedia.org/wiki/Lists_of_activists
* [EFF Pioneer Award Winners](https://en.wikipedia.org/wiki/EFF_Pioneer_Award#Winners)

- [x] musk
- [x] king
- [x] parks
- [ ] swartz

### Firewall

Make sure the firewall is enabled and blocking SSH requests:

```bash
sudo ufw enable
sudo ufw deny ssh  # `deny 22` and `deny 22 proto ipv6`
```

#### Ping problem
The `ufw` (uncomplicated firewall) command line app does not have any option for disabling incoming ICMP Internet protocol requests. ICMP is used mainly by `ping` to discover IP addresses of servers on the Internet or LAN. For security, I'd like to hide my server from ping requests. How should I do that on Ubuntu (preferably using the built-in firewall)?

#### Ping solution
These `sed` commands will ensure UFW blocks all ICMP requests (including `ping`):

```bash
sudo sed -i -E 's/^\s*-A\ ufw-before-input\ -p\ icmp\ --icmp-type.*ACCEPT\s*/#\ \0/g' /etc/ufw/before.rules
sudo sed -i -E 's/^\s*-A\ ufw-before-forward\ -p\ icmp\ --icmp-type.*ACCEPT\s*/#\ \0/g' /etc/ufw/before.rules
```

#### Explanation
By default, `ufw` blocks all ICMP requests _except_ `ping`s. So you need to disable these ping exceptions (``ACCEPT``s) in `/etc/ufw/before.rules`:

```bash
# ok icmp codes for INPUT
-A ufw-before-input -p icmp --icmp-type destination-unreachable -j ACCEPT
-A ufw-before-input -p icmp --icmp-type time-exceeded -j ACCEPT
-A ufw-before-input -p icmp --icmp-type parameter-problem -j ACCEPT
-A ufw-before-input -p icmp --icmp-type echo-request -j ACCEPT

# ok icmp code for FORWARD
-A ufw-before-forward -p icmp --icmp-type destination-unreachable -j ACCEPT
-A ufw-before-forward -p icmp --icmp-type time-exceeded -j ACCEPT
-A ufw-before-forward -p icmp --icmp-type parameter-problem -j AfCCEPT
-A ufw-before-forward -p icmp --icmp-type echo-request -j ACCEPT
```

## python dev environment

```bash
sudo apt install --upgrade git build-essential gcc python3-dev python3-gpg python3-virtualenv python3-poetry python-is-python3
echo "python --version: $(python --version)"
echo "    which python: $(which python)"
echo "       which pip: $(which pip)"
```

### Disable CUPS service

The cups service allows printers to automatically add themselves back whenever you add them.
The cafe where I work (Coffee N Talk downtown San Diego) was hacked and their printer kept connecting to my machine until I disabled CUPS

```bash
sudo systemctl stop cups-browsed
sudo systemctl disable cups-browsed

```

## Frame.work Laptop 13

### Freezes and crashes

```text
[    6.928165] Bluetooth: hci0: Firmware timestamp 2022.5 buildtype 1 build 38020
[    7.002370] NET: Registered PF_ALG protocol family
[    7.424882] ACPI BIOS Error (bug): Could not resolve symbol [\_TZ.ETMD], AE_NOT_FOUND (20210730/psargs-330)

[    7.424889] No Local Variables are initialized for Method [_OSC]

[    7.424890] Initialized Arguments for Method [_OSC]:  (4 arguments defined for method invocation)
[    7.424891]   Arg0:   00000000e3aacb92 <Obj>           Buffer(16) 5D A8 3B B2 B7 C8 42 35
[    7.424897]   Arg1:   000000007d8e863a <Obj>           Integer 0000000000000001
[    7.424899]   Arg2:   000000002a2220f9 <Obj>           Integer 0000000000000002
[    7.424901]   Arg3:   000000000396a7ad <Obj>           Buffer(8) 00 00 00 00 05 00 00 00

[    7.424906] ACPI Error: Aborting method \_SB.IETM._OSC due to previous error (AE_NOT_FOUND) (20210730/psparse-529)
[    8.747099] Bluetooth: RFCOMM TTY layer initialized
[    8.747105] Bluetooth: RFCOMM socket layer initialized
```
#### Ubuntu specs

Ubuntu: Ubuntu 22.04.1 LTS
Windowing system: X11
Gnome: 42.4

#### Hardware specs

CPU: 11th Gen Intel® Core™ i7-1165G7 @ 2.80GHz × 8
GPU: Mesa Intel® Xe Graphics (TGL GT2)
RAM: 64 GB
Storage: 1 TB

#### BIOS install

- BIOS guide/docs on frame.work https://community.frame.work/t/bios-guide/4178
- bios & driver downloads: https://knowledgebase.frame.work/en_us/bios-and-drivers-downloads-rJ3PaCexh
- https://knowledgebase.frame.work/framework-laptop-bios-releases-S1dMQt6F


### brightness control

#### Tried these articles

- https://askubuntu.com/a/749371/31658
- https://allthings.how/how-to-fix-brightness-keys-not-working-in-ubuntu/


## Audio

When I updated the kernel my sound stopped working. Speakers were completely dead and when I turned up the volume on my headset they displayed the "Muted" icon on the desktop for my laptops.

I reinstalled alsa and pulseaudio and that seemed to reactivate the left speaker.
Then I installed PulseAudio Volume Control using the Ubuntu Software GUI.
With that I was able to "lock" the left and right balance, and that seemed to disable the System Settings for Sound until I rebooted.
Once I rebooted both speakers worked and my headset was at normal volume.

### References

- https://www.maketecheasier.com/fix-no-sound-issue-ubuntu/
- https://itsfoss.com/fix-sound-ubuntu-1304-quick-tip/


## Framework-specific (Intel chipsets)

### Intel GPU drivers

Check to see what kind of video card (GPU) you have:

```bash
lspci | grep VGA
```

#### _`hobs@framework-laptop output`_:
00:02.0 <span style="color:red">**VGA**</span> compatible controller: Intel Corporation Device 9a49 (rev 01)

Install the Intel repo PGP keys:

```bash
sudo echo && sudo apt-get install -y gpg-agent wget
wget -qO - https://repositories.intel.com/graphics/intel-graphics.key |   sudo apt-key add -
```

Install the GPU tool binary and source code (`-dev`) packages:

```bash
sudo apt-get update
# binaries
sudo apt-get install \
  intel-opencl-icd \
  intel-level-zero-gpu level-zero \
  intel-media-va-driver-non-free libmfx1
# dev tools
sudo apt-get install \
  libigc-dev \
  intel-igc-cm \
  libigdfcl-dev \
  libigfxcmrt-dev \
  level-zero-dev
```




Add your user to the "render" group to be able to run the GPU tools.

```bash
gpu_username=$(stat -c "%G" /dev/dri/render*)
sudo usermod -a -G $gpu_username $USER
```

For me that works to create the `render` group and add my `hobs` user to that `render` group:

```bash
groups $USER
```

#### _`hobs@framework-laptop output`_:
```text
hobs : hobs adm cdrom sudo dip plugdev input render lpadmin lxd sambashare docker
```


### Freezing (CPU Lock-Ups)

After an upgrade to Ubuntu 20.04 or later it may repeatedly lock up and fails to respond.
Try holding [CTRL] + [PRT_SCR] while typing [R] [E] [I] [S] [U] [B] (reboot even if system utterly broken).
But if it fails to respond to any keyboard or mouse input you may have an Intel process that is incompatible with recent linux kernels.
This causes the Intel processor to spontaneously go to sleep.

To fix this you can wait for the kernel fix (could take years) or disable the CPU sleeping feature with a kernel option in your boot program (Grub).
So you'll need to power down the old fashioned way (hold the power button down).
Then you can power your computer back on and quickly fix the grub boot options.
This [Ask Ubuntu](https://askubuntu.com/a/4412/31658) has all the details:

```bash
sudo sed -i s/quiet splash/quiet splash intel_idle.max_cstate=1/g /etc/default/grub
sudo update-grub
sudo shutdown
```

Next time you power it back on, all should be fine.

### Keyboard shortcuts

Before 18.04 the left super key opened the Application menu with a search bar. 
After that you have to type [Left-Super]-[F10]. 
To restore the old behavior see this [stack overflow answer](https://askubuntu.com/a/1071393/31658):

```bash
sudo apt install -y xcape
gsettings set org.gnome.mutter overlay-key ''
sudo echo << EOF > xcape.desktop
[Desktop Entry]
Categories=Utility;
Comment=Map super key
Exec=sh -c 'sleep 0.4 ; xcape -e "Super_L=Super_L|space"' 
Icon=keyboard
Name=xcape
StartupNotify=false
Type=Application
X-GNOME-Autostart-enabled=true
EOF
sudo mv xcape.desktop $HOME/.config/autostart/xcape.desktop
```

### The Intel WiFi card driver is borked

If your WiFi drops unexpectedly and you have an Intel WiFi card you'll need to replace it with a Qualcomm which has working Linux drivers.

## Invisible USB Drives

Sometimes no matter how much you unplug and replug a USB drive from the USB port, Ubuntu fails to mount it with the `Files` application or detect it in the `Disks` application.
Even `ls /dev` won't list any `/dev/sda` or `/dev/sdb` drives for it.
The following commands seem to wake it up:

```bash
lsblk
lsusb
```

That should reveal `/dev/sda` and create an icon for it in the `Files` application.

## Battery life

**WARNING! THIS MAY NOT WORK AND CAUSE UNEXPECTED CRASHES/FREEZES**

You can reduce wear and tear on your Li Ion battery in any laptop by minimizing the time the battery is charged above 60% or discharges below 40%.
This means that if you leave your laptop plugged in all day and keep it charged to 100% its capacity will decline much faster than if you only let it charge to 50% or 60%.
Ubuntu can do this automatically for you if you set the maximum charge % in your Ubuntu system settings.
Unfortunately this setting is reset every time you boot your computer.
So you probably want to create a systemd process that resets this setting to your desired charge level each time you reboot.
And when you stop this service you can have it reset to 100%, say before you go on a roadtrip.

Here's the systemd file:

```bash
[Unit]
Description=Set the battery charge threshold
After=multi-user.target

[Service]
Type=oneshot
ExecStart=/bin/bash -c 'echo 60 > /sys/class/power_supply/BAT1/charge_control_end_threshold'
ExecStop=/bin/bash -c 'echo 100 > /sys/class/power_supply/BAT1/charge_control_end_threshold'

[Install]
WantedBy=multi-user.target
```

The `BAT0/` and `BAT1/` directories contain files that are all read-only on my Framework laptop.
And these directories are symlinked elsewhere so any `chmod` you attempt may not seem to work and you may end up messing up the file permissions on files and folders you didn't intend to.

#### References
- https://askubuntu.com/a/1373229

You may want to use notify-send to alert the user before a low-batter shutdown or suspend:

#### _`/usr/share/dbus-1/services/org.freedesktop.Notifications.service`_
```bash
[D-BUS Service]
Name=org.freedesktop.Notifications
Exec=/usr/lib/notification-daemon/notification-daemon
```

Then when you say `sudo notify-send "Battery is low!!` a small icon will appear in the toolbar (next to the clock and other status icons) with this notification.

## Touchy Laptop Touchpad

If I don't have a mouse plugged in, the trackpad corrupts what I type nearly every time I pause. It happens in Sublime Text 3, nano, bash, and GMail, everywhere I use the keyboard. I can't keep my hands far enough away from the keyboard to get any work done at all without corrupting files, closing windows, sending unfinished e-mails, etc. I might as well have a corrupted keylogger and mouse hijacker (gremlin). As I type this, I'm going to have to g home to my KVM switch and mouse to finish this article. I just plugged in a mouse and that wasn't enough.

This has been a perpetual ever since I moved 100% to Ubuntu - each and every time I've ever installed Ubuntu on a Laptop. And it typically takes months for me to solve it.

The track pad driver settings are just too sensitive. My finger or palm can be almost half a cm above it and  it will register a weird drag/click that moves the cursor, selects and deletes text. Thank God for ctrl-Z.

This tool helped for a bit, until I upgraded Ubuntu and it overwrote my settings:

```bash
sudo apt install aptitude
sudo aptitude install gnome-tweaks gnome-tweak-tool
gnome-tweaks
```

After launching `gnome-tweaks` I then clicked on Keyboard & Mouse and scrolled down to "Mouse Click Emulation" at the bottom.
I left it turned on and confirmed that 2-finger right click was already turned on.
Also two-finger natural scrolling was already turned on.
Neither of these options were working until I did the next "combination" to unlock this feature.

0. Turn on "Show Extended Input Sources (increases the choice of input...)"
1. Turn on "enable additional input device options"
2. Plug in a mouse
3. Completely power down for 10 seconds.
4. Turn back on with mouse still plugged in.
5. Unplug mouse.

Turning on "Disable Touchpad while typing" does not immediately take effect.
Fortunately the , but this seems to have no affect anymore, after the upgrade.
Also, plugging in a mouse doesn't help either.

However if I plug in a mouse and reboot , everything seems fine. Even after I unplug the mouse and use the touchpad.
The touchpad now actually moves the cursor and does what I want without garbling everything I do.

Here is how to list your input devices and find your trackpad (Touchpad) xinput ID number:

```bash
$ xinput list | grep -i touch
⎜   ↳ PIXA3854:00 093A:0274 Touchpad            id=11 [slave  pointer  (2)]
```

And then you can look at all the settings (properties or props) for your device:

```bash
$ xinput list-props "PIXA3854:00 093A:0274 Touchpad" | grep -i hand
$ xinput list-props "PIXA3854:00 093A:0274 Touchpad" | grep -i sens
$ xinput list-props "PIXA3854:00 093A:0274 Touchpad"
```

Interestingly there's a 3-D rotation matrix for your trackpad coordinates. It's currently set to the Identity matrix on my trackpad, but I bet it might be more intuitive if I rotated the output coord frame a small amount counter-clickwise, so that my right hand and fingers can stroke up and to slightly to the left to move the pointer directly upward. But I'm not sure what the 3rd Z coordinate represents in this matrix.

```bash
Coordinate Transformation Matrix (180): 1.000000, 0.000000, 0.000000, 0.000000, 1.000000, 0.000000, 0.000000, 0.000000, 1.000000
```

### Disable middle click

Annoyingly, Firefox/Librewolf browser tabs kept closing when I clicked too close to the middle bottom of the trackpad.

You need just the xinput ID string to change the settings of an input device.
So here one way to store that in a variable using the bash `cut` command to avoid the formatting characters around it.
Or you can copy/paste it into the variable, if you like.
There's probably even an `xinput --list` option to display only the ID of a particular kind of device.
Or a better regex (`grep` pattern).

You need to include the word "Touchpad" or select the touchpad by its id=12 entry:

```bash
inputdev=$(xinput list | grep -i touch | head -n 1 | cut -c11-40)
echo $inputdev
```
```text
PIXA3854:00 093A:0274 Touchpad
```

And for the mouse you could do the same thing:

```bash
$ mousedev=$(xinput list | grep -i mouse | head -n 1 | cut -c11-37)
$ echo $mousedev

PIXA3854:00 093A:0274 Mouse
```

Once you have the xinput id this [SO answer](https://unix.stackexchange.com/a/466333/33948) shows one way to customize your trackpad to disable some button clicks:

```bash
# this fails if you don't include all 3 pieces of the string PIX...Touchpad
$ xinput get-button-map "$inputdev"

1 2 3 4 5 6 7 
```

The middle button is probably number 2 in the list.
So you want to replace the two with a one in order to cause the right button click (1) to be triggered whenever the middle button is clicked.
Must use inputdev () rather than input id (12)

```bash
xinput set-button-map "$inputdev" 1 1 3 4 5 6 7
xinput get-button-map "$inputdev"
```
```text
1 1 3 4 5 6 7 
```

You could, of course set it to 1 3 3 4 5 6 7 if you wanted your middle clicks to trigger the right-hand mouse button.


## Bootable USB

If you create a bootable USB using 'Startup Disk Creator' or similar tools, that creates a FAT (old DOS) disk so that it can run on any PC.
But that means it will only use up around 3.2 GB of space on your flash drive.
If you open it in the "Disks" app, you might see many Gigabytes of unused, unformated disk space on your USB stick.
But if you try to format it, no matter which disk format you choose (NTFS, EXT4, LVM) it will always give you an error message like this:

``` 
Error creating partition on /dev/sda failed add partition to device dev/sda too many primary partitions
```

The only way to make use of that space is to create a "bootable USB with persistent storage."
SearX.be is your friend.
Here's a good article on creating a Persistent Live (bootable) USB: https://www.howtogeek.com/howto/14912/create-a-persistent-bootable-ubuntu-usb-flash-drive/
 
## Headset not detected

You can't use the aux audio jack output with your 3.5 mm headset/headphones because it doesn't appear as an output device in Zoom Audio Settings or Ubuntu Sound Settings.

### 1. alsactl restore

```bash
sudo alsactl restore
sudo shutdown now
```

1. Open Settings -> Sound
2. Look for the the output pulldown that lists Builtin Speakers.
3. Insert and remove the headset jack plug several times watching the pulldown.

### 2. change pulseaudio conf

The `[Element Speaker]` needs to be turned on within `analog-output-headphones.conf`

#### _`/usr/share/pulseaudio/alsa-mixer/paths/analog-output-headphones.conf`_
```
cd /usr/share/pulseaudio/alsa-mixer/paths/
diff -c1 analog-output-headphones.conf.fixed analog-output-headphones.conf.bak
*** analog-output-headphones.conf.fixed	2022-02-14 14:35:38.102373917 -0800
--- analog-output-headphones.conf.bak	2022-02-14 14:33:00.125151674 -0800
***************
*** 119,122 ****
  [Element Speaker]
! switch = on
! volume = ignore

--- 119,122 ----
  [Element Speaker]
! switch = off
! volume = off
```

### 3. Try `alsactl restore` again

```bash
sudo alsactl restore
sudo shutdown now
```

1. Open Settings -> Sound
2. Look for the the output pulldown that lists Builtin Speakers.
3. Insert and remove the headset jack plug several times watching the pulldown.

## `dd` destroys disks

I destroyed my hard drive by copying, moving, deleting and resizing partitions.
Here are my notes from my phone when I was trying to recover:

#### `/dev` names
nvme0n1
nvme0n1p1
nvme0n1p2 mint copy
nvme0n1p3 
nvme0n1p4 mint orig

#### boot recovery shell
```console
ls -hl
ls -hl (hd2,4)/
cat (hd2,4)/etc/issue
set root=(hd2,4)
linux /boot/vmlinuz-5.15.0-91-generic \
  root=/dev/nvme0n1p2
initrd /boot/vmlinuz-5.15.0-91-generic
boot
```





## GUI T

