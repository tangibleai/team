# Tailwind CSS framework

### 1. Existing Django project

Find an existing Django project that you want to improve the UX for.
Here's how I did it for ConvoHub (replace `convohub` with your project):

```bash
$ git clone git@gitlab.com:tangibleai/community/convohub.git
$ cd convohub
```

You should now see your `.git` directory and `manage.py` as well as your django staticfiles directories.

#### _`ls -hal`_
```bash
faceofqary/
frontend/
.git/
.gitignore
hub/
manage.py
pyproject.toml
pytest.ini
README.md
scripts/
static/
staticfiles/
templates/
tests/
.workon
```

### 2. Create a vite frontend project

Next, make sure the Node Package Manager (`npm`) is up-to-date and create a frontend project.
It's OK if you already have another frontend project, like the `faceofqary/` project that we have in our ConvoHub project.
We used the svelte framework for our `frontend/` project because we want to eventually move the `faceofqary` Svelte widgets (`App.js`, `Door.js`, `Room.js`) into the new `frontend/` project.

```bash
$ npm install -g npm@latest
$ npm create vite@latest frontend -- --template svelte
$ cd frontend
```

Now you should see your package.json that defines all your javascript dependencies, like vite, tailwind, and svelte.
The `frontend/src/` directory contains a new boilerplate Svelte app.
The `vite.config.js` file is where you will configure your build process by telling vite where to put your javascript and CSS bundles.
This is similar to what `python manage.py collectstatic` does in Django.

#### _`tree -d -L 2 -I node_modules`_
```bash
├── index.html
├── jsconfig.json
├── node_modules
├── package.json
├── package-lock.json
├── postcss.config.js
├── package.json
├── package-lock.json
├── postcss.config.js
├── public
│   └── vite.svg
├── README.md
├── src
│   ├── app.css
│   ├── App.svelte
│   ├── assets
│   ├── lib
│   ├── main.js
│   └── vite-env.d.ts
├── svelte.config.js
├── tailwind.config.js
└── vite.config.js
```

### 3. Add tailwindcss as a dependency

Use `npm install` to add tailwind to your `package.json` files where your dependencies are listed.
You need to use `npx` rather than `npm` with tailwind projects, because tailwind uses the `.jsx` language rather than vanilla `.js`.
The JSX (JavaScript + XML) language extends the javascript syntax to allow you to include HTML inside of JavaScript files.
The `.svelte` language goes the other way around, allowing you to put Javascript inside of HTML.

Use `npx tailwindcss init -p` to create a `tailwind.config.js` file.
The `tailwind.config.js` is where you will configure your Tailwind CSS build process.

```bash
$ npm install -D tailwindcss postcss autoprefixer
$ npx tailwindcss init -p
```

#### _`cat tailwind.config.js`_
```bash
/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

### 4. Configure TailwindCSS

Add your content files to `tailwind.config.js` so that it looks something like this:

#### _`cat tailwind.config.js`_
```js
/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './src/**/*.{html,js,svelte,ts}',
    // './src/**/*.{html,jsx,svelte,tsx},' 
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
```

### 5. Import tailwind in your `app.css`

Your `src/app.css` file only contains the example Svelte App styles so you need to import Tailwind at the top of that file:

#### _`cat src/app.css`_
```css
@tailwind base;
@tailwind components;
@tailwind utilities;

:root {
  font-family: Inter, system-ui, Avenir, Helvetica, Arial, sans-serif;
  line-height: 1.5;
  font-weight: 400;

// ...
}
```
Your `src/main.js` should already have an app.css import at the top of the file:

#### _`cat src/main.js`_
```js
import './app.css'
import App from './App.svelte'

const app = new App({
  target: document.getElementById('app'),
})

export default
```

Before you move on make sure everything is working.

```bash
$ npm run dev
```


### 6. Configure vite to build Svelte



#### _`cat svelte.config.js`_
```js
import adapter from '@sveltejs/adapter-auto';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';
/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter()
  },
  preprocess: vitePreprocess()
};
export default config;
```
$ npm run dev
```

[^1]: [Install Tailwind with Vite](https://tailwindcss.com/docs/guides/vite)
[^2]: [Install Tailwind with Svelte+Vite](https://dev.to/altierii/how-to-install-tailwind-css-with-svelte-vite-1kj6)
[^2]: [Intro to CSS and the Tailwind CSS framework](https://www.freecodecamp.org/news/what-is-tailwind-css-a-beginners-guide/)
[^3]: [Getting to know the tailwind "language" using Tailwind Play](https://www.codeinwp.com/blog/tailwind-css-tutorial/)
