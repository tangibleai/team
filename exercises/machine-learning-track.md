# Syllabus

This is a 10 week syllabus for a software developer that is already familiar with Python and a little bit of Django.

## Prerequisites

You should have already...

- Set up a Python virtual environment (one of: `virtualenv`, `venv`, or `conda`)
- Know how to install packages in a virtual environment (one of: `pip install`, `poetry add`, `conda install`)
- Know how to use `git` and `GitLab.com` to clone a repository, create Issues and merge requests.
- Understand spreadsheets and how to export them to CSVs or load them from CSVs
- Know what a vector and matrix is and how to multiply them in Python: 
  - `A = np.array([1, 2],[3, 4]]); x = [[5], [10]]; y1 = A * x ; y2 = A.dot(np.array(x)) ;`
  - What does `y1` equal? What about `y2`?
- Know what a polynomial is and how to evaluate one in python: `y = w0 + w1 * x1 + + w1_2 * x1 ** 2`
  - If you plotted `y` vs `x1` what would it look like?
  - How would the shape of the plot change if you changed `w0`, `w1` and `w1_2`?

### Week 1 - Feature engineering

Set up a virtual environment for machinelearning jupyter notebooks and ipython consoles. You will probably need `jupyter`, `scikit-learn`, `spacy`, `tqdm`, `matplotlib`, `seaborn`, and `pandas`.

Run the following notebooks and "make them yours" by breaking them and fixing them until they are very different from the original ones that I gave you:

1. [basics of data wrangling and linear regression](https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/1-find-data/predict-housing-prices.ipynb?ref_type=heads)
2. [dealing with categories and NaNs](https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/2-feature-engineering/portland_housing_price_linear_regression.ipynb)
3. Bonus: [dealing with location](https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/2-feature-engineering/portland_housing_price_linear_regression.ipynb)

### Week 2 - Overfitting and regularization

1. split your data into training and test sets using a `np.random.rand()` or `random.choice`
2. compute a baseline accuracy (RMSE) for the training set and the test set (which is better? why?)
3. Engineer 1000s of features programmatically using `sklearn.preprocessing.PolynomialFeatures`.
4. Create a hyperparameter table that shows how the training set accuracy improves as you add more and more features.
5. How many features do you need to "engineer" before the test accuracy starts to decline (RMSE starts to grow)?
6. How can you find the features that are helping your model and the features that might be hurting your model
7. Remove the useless features and see if you can find a model that has better test set accuracy and worse training set accuracy. Is this a better model or a worse model? What do your stakeholders (customers) care about for this model?
8. Search for "regularization" and "Lasso" in sci-kit learn and see how their approach compares to yours. Research "overfitting" and be prepared to talk about it.

### Week 3 - Natural language processing (embedding vectors)

1. Engineer 300 more features using the SpaCy medium language model on any natural language text feature in your dataset.
