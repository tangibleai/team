# Finding an efficient model on Hugging Face

1. Visit the model search page on Hugging Face: https://huggingface.co/models
2. Look for your task on the left-hand side and click on it. To get vector encodings of natural language text, you want to select "Feature Extraction" or "Sentence Similarity" (https://huggingface.co/models?pipeline_tag=feature-extraction&sort=trending).
3. You can't sort the results based on size, but recent models are usually more efficient, so for the knowt app we sorted on "recently created" (https://huggingface.co/models?pipeline_tag=feature-extraction&sort=created)
4. As you scroll through the list of models, use your browser to search to highlight words such as "mini", "tiny", "efficient", "small", and "prune" or any names you can think of. The name "mBERT" was our best guess.
5. If model names contain r"-\d.\d[Bb]" you can probably skip them (they are multi-Gigabyte models)
6. Click on the model you like and then click on the Files tab: https://huggingface.co/morten-j/fine_tuned_mBERT/tree/main
7. Look for the LFS files and see how large they are before downloading and testing for memory consumption


## Llama.cpp

Llama.cpp and other efficient models use a file format called GGUF.
Here's an example interaction with a couple different models downloaded from Hugging Face (https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGUF/tree/main) after searching for "GGUF" and sorting by most downloads.

### Python-llama-cpp package

See aliases and functions in ~/bin/bash_functions

### Compiled CPP code
Here are some vocabulary(?) models that come with the `llamma.cpp` [source code by Georgi Gerganov](https://github.com/ggerganov/llama.cpp):

```bash
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ ls -hal models/
-rw-rw-r--  1 hobs hobs 4.7M Mar 21 16:01 ggml-vocab-aquila.gguf
-rw-rw-r--  1 hobs hobs 1.3M Mar 21 16:01 ggml-vocab-baichuan.gguf
-rw-rw-r--  1 hobs hobs 2.5M Mar 21 16:01 ggml-vocab-falcon.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-gpt2.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-gpt-neox.gguf
-rw-rw-r--  1 hobs hobs 707K Mar 21 16:01 ggml-vocab-llama.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-mpt.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-refact.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-stablelm-3b-4e1t.gguf
-rw-rw-r--  1 hobs hobs 1.7M Mar 21 16:01 ggml-vocab-starcoder.gguf
```


Here are some llama-2-7b quantized models and their file sizes:

```bash
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ ls -hal models/llama*.gguf
-rw-rw-r-- 1 hobs hobs 2.7G Mar 21 19:42 models/llama-2-7b-chat.Q2_K.gguf
-rw-rw-r-- 1 hobs hobs 3.4G Mar 21 19:41 models/llama-2-7b-chat.Q3_K_L.gguf
-rw-rw-r-- 1 hobs hobs 3.1G Mar 21 20:07 models/llama-2-7b-chat.Q3_K_M.gguf
-rw-rw-r-- 1 hobs hobs 3.9G Mar 21 20:10 models/llama-2-7b-chat.Q4_K_M.gguf
-rw-rw-r-- 1 hobs hobs 4.5G Mar 21 20:11 models/llama-2-7b-chat.Q5_K_M.gguf
```

### `models/llama-2-7b-chat.Q5_K_M.gguf`

```bash
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ workon llama.cpp
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ export MODEL=models/llama-2-7b-chat.Q5_K_M.gguf 
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ ./main -m "$MODEL" -p "Write a Python function to compute the factorial of a positive integer: " -n 400 -e
```

```text
Log start
main: build = 2490 (be07a032)
main: built with cc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0 for x86_64-linux-gnu
main: seed  = 1711077716
llama_model_loader: loaded meta data with 19 key-value pairs and 291 tensors from models/llama-2-7b-chat.Q5_K_M.gguf (version GGUF V2)
llama_model_loader: Dumping metadata keys/values. Note: KV overrides do not apply in this output.
llama_model_loader: - kv   0:                       general.architecture str              = llama
llama_model_loader: - kv   1:                               general.name str              = LLaMA v2
llama_model_loader: - kv   2:                       llama.context_length u32              = 4096
llama_model_loader: - kv   3:                     llama.embedding_length u32              = 4096
llama_model_loader: - kv   4:                          llama.block_count u32              = 32
llama_model_loader: - kv   5:                  llama.feed_forward_length u32              = 11008
llama_model_loader: - kv   6:                 llama.rope.dimension_count u32              = 128
llama_model_loader: - kv   7:                 llama.attention.head_count u32              = 32
llama_model_loader: - kv   8:              llama.attention.head_count_kv u32              = 32
llama_model_loader: - kv   9:     llama.attention.layer_norm_rms_epsilon f32              = 0.000001
llama_model_loader: - kv  10:                          general.file_type u32              = 17
llama_model_loader: - kv  11:                       tokenizer.ggml.model str              = llama
llama_model_loader: - kv  12:                      tokenizer.ggml.tokens arr[str,32000]   = ["<unk>", "<s>", "</s>", "<0x00>", "<...
llama_model_loader: - kv  13:                      tokenizer.ggml.scores arr[f32,32000]   = [0.000000, 0.000000, 0.000000, 0.0000...
llama_model_loader: - kv  14:                  tokenizer.ggml.token_type arr[i32,32000]   = [2, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, ...
llama_model_loader: - kv  15:                tokenizer.ggml.bos_token_id u32              = 1
llama_model_loader: - kv  16:                tokenizer.ggml.eos_token_id u32              = 2
llama_model_loader: - kv  17:            tokenizer.ggml.unknown_token_id u32              = 0
llama_model_loader: - kv  18:               general.quantization_version u32              = 2
llama_model_loader: - type  f32:   65 tensors
llama_model_loader: - type q5_K:  193 tensors
llama_model_loader: - type q6_K:   33 tensors
llm_load_vocab: special tokens definition check successful ( 259/32000 ).
llm_load_print_meta: format           = GGUF V2
llm_load_print_meta: arch             = llama
llm_load_print_meta: vocab type       = SPM
llm_load_print_meta: n_vocab          = 32000
llm_load_print_meta: n_merges         = 0
llm_load_print_meta: n_ctx_train      = 4096
llm_load_print_meta: n_embd           = 4096
llm_load_print_meta: n_head           = 32
llm_load_print_meta: n_head_kv        = 32
llm_load_print_meta: n_layer          = 32
llm_load_print_meta: n_rot            = 128
llm_load_print_meta: n_embd_head_k    = 128
llm_load_print_meta: n_embd_head_v    = 128
llm_load_print_meta: n_gqa            = 1
llm_load_print_meta: n_embd_k_gqa     = 4096
llm_load_print_meta: n_embd_v_gqa     = 4096
llm_load_print_meta: f_norm_eps       = 0.0e+00
llm_load_print_meta: f_norm_rms_eps   = 1.0e-06
llm_load_print_meta: f_clamp_kqv      = 0.0e+00
llm_load_print_meta: f_max_alibi_bias = 0.0e+00
llm_load_print_meta: f_logit_scale    = 0.0e+00
llm_load_print_meta: n_ff             = 11008
llm_load_print_meta: n_expert         = 0
llm_load_print_meta: n_expert_used    = 0
llm_load_print_meta: causal attn      = 1
llm_load_print_meta: pooling type     = 0
llm_load_print_meta: rope type        = 0
llm_load_print_meta: rope scaling     = linear
llm_load_print_meta: freq_base_train  = 10000.0
llm_load_print_meta: freq_scale_train = 1
llm_load_print_meta: n_yarn_orig_ctx  = 4096
llm_load_print_meta: rope_finetuned   = unknown
llm_load_print_meta: ssm_d_conv       = 0
llm_load_print_meta: ssm_d_inner      = 0
llm_load_print_meta: ssm_d_state      = 0
llm_load_print_meta: ssm_dt_rank      = 0
llm_load_print_meta: model type       = 7B
llm_load_print_meta: model ftype      = Q5_K - Medium
llm_load_print_meta: model params     = 6.74 B
llm_load_print_meta: model size       = 4.45 GiB (5.68 BPW) 
llm_load_print_meta: general.name     = LLaMA v2
llm_load_print_meta: BOS token        = 1 '<s>'
llm_load_print_meta: EOS token        = 2 '</s>'
llm_load_print_meta: UNK token        = 0 '<unk>'
llm_load_print_meta: LF token         = 13 '<0x0A>'
llm_load_tensors: ggml ctx size =    0.11 MiB
llm_load_tensors:        CPU buffer size =  4560.87 MiB
..................................................................................................
llama_new_context_with_model: n_ctx      = 512
llama_new_context_with_model: n_batch    = 512
llama_new_context_with_model: n_ubatch   = 512
llama_new_context_with_model: freq_base  = 10000.0
llama_new_context_with_model: freq_scale = 1
llama_kv_cache_init:        CPU KV buffer size =   256.00 MiB
llama_new_context_with_model: KV self size  =  256.00 MiB, K (f16):  128.00 MiB, V (f16):  128.00 MiB
llama_new_context_with_model:        CPU  output buffer size =    62.50 MiB
llama_new_context_with_model:        CPU compute buffer size =    70.50 MiB
llama_new_context_with_model: graph nodes  = 1060
llama_new_context_with_model: graph splits = 1

system_info: n_threads = 4 / 8 | AVX = 1 | AVX_VNNI = 0 | AVX2 = 1 | AVX512 = 1 | AVX512_VBMI = 1 | AVX512_VNNI = 1 | FMA = 1 | NEON = 0 | ARM_FMA = 0 | F16C = 1 | FP16_VA = 0 | WASM_SIMD = 0 | BLAS = 0 | SSE3 = 1 | SSSE3 = 1 | VSX = 0 | MATMUL_INT8 = 0 | 
sampling: 
    repeat_last_n = 64, repeat_penalty = 1.000, frequency_penalty = 0.000, presence_penalty = 0.000
    top_k = 40, tfs_z = 1.000, top_p = 0.950, min_p = 0.050, typical_p = 1.000, temp = 0.800
    mirostat = 0, mirostat_lr = 0.100, mirostat_ent = 5.000
sampling order: 
CFG -> Penalties -> top_k -> tfs_z -> typical_p -> top_p -> min_p -> temperature 
generate: n_ctx = 512, n_batch = 2048, n_predict = 400, n_keep = 1
```

```text
 Write a Python function to compute the factorial of a positive integer: 
\```
def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n-1)
\```
This function uses recursion to compute the factorial of a positive integer `n`. It starts by checking if `n` is equal to 0, in which case the factorial is defined to be 1. Otherwise, it multiplies `n` by the factorial of `n-1` and returns the result.
For example, if you call the function with `n=5`, it will compute the factorial of 5, which is `5 * 4 * 3 * 2 * 1 = 120`.
Here is an example of how to use the function:
\```
# Compute the factorial of 5
print(factorial(5)) # Output: 120
\```
You can also use the function with a negative integer by changing the if statement:
\```
# Compute the factorial of -5
print(factorial(-5)) # Output: 0
\```
It\'s important to note that the function will only work for positive integers, if you try to use it with a negative integer it will return 0.
Also, you can use the `math.factorial()` function from the `math` module to compute the factorial of a number, it is more efficient than the recursive function.
\```
from math import factorial
# Compute the factorial of 5
print(factorial(5)) # Output: 120
\```
It's worth noting that the `math.factorial()` function is a specialized version of the `factorial()` function, it uses a more efficient algorithm to compute the factorial, it is more efficient than the recursive function for large numbers. [end of text]
```

```text
llama_print_timings:        load time =     586.46 ms
llama_print_timings:      sample time =      19.71 ms /   399 runs   (    0.05 ms per token, 20246.61 tokens per second)
llama_print_timings: prompt eval time =    1890.08 ms /    16 tokens (  118.13 ms per token,     8.47 tokens per second)
llama_print_timings:        eval time =   62739.94 ms /   398 runs   (  157.64 ms per token,     6.34 tokens per second)
llama_print_timings:       total time =   64948.82 ms /   414 tokens
Log end
```

### `llama-2-7b-chat.Q3_K_L.gguf` 

Here's a smaller model, doing poorly:


```bash
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ export MODEL=models/llama-2-7b-chat.Q3_K_L.gguf 
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ ./main -m "$MODEL" -p "Write a Python function to compute the factorial of a positive integer: " -n 400 -e
```

```text
Log start
main: build = 2490 (be07a032)
main: built with cc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0 for x86_64-linux-gnu
main: seed  = 1711078303
llama_model_loader: loaded meta data with 19 key-value pairs and 291 tensors from models/llama-2-7b-chat.Q3_K_L.gguf (version GGUF V2)
llama_model_loader: Dumping metadata keys/values. Note: KV overrides do not apply in this output.
llama_model_loader: - kv   0:                       general.architecture str              = llama
llama_model_loader: - kv   1:                               general.name str              = LLaMA v2
llama_model_loader: - kv   2:                       llama.context_length u32              = 4096
llama_model_loader: - kv   3:                     llama.embedding_length u32              = 4096
llama_model_loader: - kv   4:                          llama.block_count u32              = 32
llama_model_loader: - kv   5:                  llama.feed_forward_length u32              = 11008
llama_model_loader: - kv   6:                 llama.rope.dimension_count u32              = 128
llama_model_loader: - kv   7:                 llama.attention.head_count u32              = 32
llama_model_loader: - kv   8:              llama.attention.head_count_kv u32              = 32
llama_model_loader: - kv   9:     llama.attention.layer_norm_rms_epsilon f32              = 0.000001
llama_model_loader: - kv  10:                          general.file_type u32              = 13
llama_model_loader: - kv  11:                       tokenizer.ggml.model str              = llama
llama_model_loader: - kv  12:                      tokenizer.ggml.tokens arr[str,32000]   = ["<unk>", "<s>", "</s>", "<0x00>", "<...
llama_model_loader: - kv  13:                      tokenizer.ggml.scores arr[f32,32000]   = [0.000000, 0.000000, 0.000000, 0.0000...
llama_model_loader: - kv  14:                  tokenizer.ggml.token_type arr[i32,32000]   = [2, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, ...
llama_model_loader: - kv  15:                tokenizer.ggml.bos_token_id u32              = 1
llama_model_loader: - kv  16:                tokenizer.ggml.eos_token_id u32              = 2
llama_model_loader: - kv  17:            tokenizer.ggml.unknown_token_id u32              = 0
llama_model_loader: - kv  18:               general.quantization_version u32              = 2
llama_model_loader: - type  f32:   65 tensors
llama_model_loader: - type q3_K:  129 tensors
llama_model_loader: - type q5_K:   96 tensors
llama_model_loader: - type q6_K:    1 tensors
llm_load_vocab: special tokens definition check successful ( 259/32000 ).
llm_load_print_meta: format           = GGUF V2
llm_load_print_meta: arch             = llama
llm_load_print_meta: vocab type       = SPM
llm_load_print_meta: n_vocab          = 32000
llm_load_print_meta: n_merges         = 0
llm_load_print_meta: n_ctx_train      = 4096
llm_load_print_meta: n_embd           = 4096
llm_load_print_meta: n_head           = 32
llm_load_print_meta: n_head_kv        = 32
llm_load_print_meta: n_layer          = 32
llm_load_print_meta: n_rot            = 128
llm_load_print_meta: n_embd_head_k    = 128
llm_load_print_meta: n_embd_head_v    = 128
llm_load_print_meta: n_gqa            = 1
llm_load_print_meta: n_embd_k_gqa     = 4096
llm_load_print_meta: n_embd_v_gqa     = 4096
llm_load_print_meta: f_norm_eps       = 0.0e+00
llm_load_print_meta: f_norm_rms_eps   = 1.0e-06
llm_load_print_meta: f_clamp_kqv      = 0.0e+00
llm_load_print_meta: f_max_alibi_bias = 0.0e+00
llm_load_print_meta: f_logit_scale    = 0.0e+00
llm_load_print_meta: n_ff             = 11008
llm_load_print_meta: n_expert         = 0
llm_load_print_meta: n_expert_used    = 0
llm_load_print_meta: causal attn      = 1
llm_load_print_meta: pooling type     = 0
llm_load_print_meta: rope type        = 0
llm_load_print_meta: rope scaling     = linear
llm_load_print_meta: freq_base_train  = 10000.0
llm_load_print_meta: freq_scale_train = 1
llm_load_print_meta: n_yarn_orig_ctx  = 4096
llm_load_print_meta: rope_finetuned   = unknown
llm_load_print_meta: ssm_d_conv       = 0
llm_load_print_meta: ssm_d_inner      = 0
llm_load_print_meta: ssm_d_state      = 0
llm_load_print_meta: ssm_dt_rank      = 0
llm_load_print_meta: model type       = 7B
llm_load_print_meta: model ftype      = Q3_K - Large
llm_load_print_meta: model params     = 6.74 B
llm_load_print_meta: model size       = 3.35 GiB (4.27 BPW) 
llm_load_print_meta: general.name     = LLaMA v2
llm_load_print_meta: BOS token        = 1 '<s>'
llm_load_print_meta: EOS token        = 2 '</s>'
llm_load_print_meta: UNK token        = 0 '<unk>'
llm_load_print_meta: LF token         = 13 '<0x0A>'
llm_load_tensors: ggml ctx size =    0.11 MiB
llm_load_tensors:        CPU buffer size =  3429.77 MiB
..................................................................................................
llama_new_context_with_model: n_ctx      = 512
llama_new_context_with_model: n_batch    = 512
llama_new_context_with_model: n_ubatch   = 512
llama_new_context_with_model: freq_base  = 10000.0
llama_new_context_with_model: freq_scale = 1
llama_kv_cache_init:        CPU KV buffer size =   256.00 MiB
llama_new_context_with_model: KV self size  =  256.00 MiB, K (f16):  128.00 MiB, V (f16):  128.00 MiB
llama_new_context_with_model:        CPU  output buffer size =    62.50 MiB
llama_new_context_with_model:        CPU compute buffer size =    70.50 MiB
llama_new_context_with_model: graph nodes  = 1060
llama_new_context_with_model: graph splits = 1

system_info: n_threads = 4 / 8 | AVX = 1 | AVX_VNNI = 0 | AVX2 = 1 | AVX512 = 1 | AVX512_VBMI = 1 | AVX512_VNNI = 1 | FMA = 1 | NEON = 0 | ARM_FMA = 0 | F16C = 1 | FP16_VA = 0 | WASM_SIMD = 0 | BLAS = 0 | SSE3 = 1 | SSSE3 = 1 | VSX = 0 | MATMUL_INT8 = 0 | 
sampling: 
    repeat_last_n = 64, repeat_penalty = 1.000, frequency_penalty = 0.000, presence_penalty = 0.000
    top_k = 40, tfs_z = 1.000, top_p = 0.950, min_p = 0.050, typical_p = 1.000, temp = 0.800
    mirostat = 0, mirostat_lr = 0.100, mirostat_ent = 5.000
sampling order: 
CFG -> Penalties -> top_k -> tfs_z -> typical_p -> top_p -> min_p -> temperature 
generate: n_ctx = 512, n_batch = 2048, n_predict = 400, n_keep = 1
```

```text
 Write a Python function to compute the factorial of a positive integer: 

def factorial(n):
    return n!

This is a basic definition of the factorial function, which takes an integer argument n and returns its factorial. 

Explanation:

* The function name is `factorial`, which is a palindrome of the English word "factorial".
* The function takes a single argument `n`, which is a positive integer.
* The function returns the factorial of `n`, which is computed using the formula `n! = n * (n-1) * ... * 2 * 1`.

In this definition, the `n` variable is used to compute the factorial of the input `n`. The `!` character is used to indicate that the function is computing the factorial. [end of text]
```

```text
llama_print_timings:        load time =     530.11 ms
llama_print_timings:      sample time =       7.49 ms /   168 runs   (    0.04 ms per token, 22438.89 tokens per second)
llama_print_timings: prompt eval time =    1870.42 ms /    16 tokens (  116.90 ms per token,     8.55 tokens per second)
llama_print_timings:        eval time =   25822.46 ms /   167 runs   (  154.63 ms per token,     6.47 tokens per second)
llama_print_timings:       total time =   27780.61 ms /   183 tokens
Log end
```

### `llama-2-7b-chat.Q2_K.gguf`
And even smaller model:

```bash
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ export MODEL=models/llama-2-7b-chat.Q2_K.gguf 
(.venv) hobs@minty:~/code/tangibleai/public/llama.cpp$ ./main -m "$MODEL" -p "Write a Python function to compute the factorial of a positive integer: " -n 400 -e
```

```text
Log start
main: build = 2490 (be07a032)
main: built with cc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0 for x86_64-linux-gnu
main: seed  = 1711078478
llama_model_loader: loaded meta data with 19 key-value pairs and 291 tensors from models/llama-2-7b-chat.Q2_K.gguf (version GGUF V2)
llama_model_loader: Dumping metadata keys/values. Note: KV overrides do not apply in this output.
llama_model_loader: - kv   0:                       general.architecture str              = llama
llama_model_loader: - kv   1:                               general.name str              = LLaMA v2
llama_model_loader: - kv   2:                       llama.context_length u32              = 4096
llama_model_loader: - kv   3:                     llama.embedding_length u32              = 4096
llama_model_loader: - kv   4:                          llama.block_count u32              = 32
llama_model_loader: - kv   5:                  llama.feed_forward_length u32              = 11008
llama_model_loader: - kv   6:                 llama.rope.dimension_count u32              = 128
llama_model_loader: - kv   7:                 llama.attention.head_count u32              = 32
llama_model_loader: - kv   8:              llama.attention.head_count_kv u32              = 32
llama_model_loader: - kv   9:     llama.attention.layer_norm_rms_epsilon f32              = 0.000001
llama_model_loader: - kv  10:                          general.file_type u32              = 10
llama_model_loader: - kv  11:                       tokenizer.ggml.model str              = llama
llama_model_loader: - kv  12:                      tokenizer.ggml.tokens arr[str,32000]   = ["<unk>", "<s>", "</s>", "<0x00>", "<...
llama_model_loader: - kv  13:                      tokenizer.ggml.scores arr[f32,32000]   = [0.000000, 0.000000, 0.000000, 0.0000...
llama_model_loader: - kv  14:                  tokenizer.ggml.token_type arr[i32,32000]   = [2, 3, 3, 6, 6, 6, 6, 6, 6, 6, 6, 6, ...
llama_model_loader: - kv  15:                tokenizer.ggml.bos_token_id u32              = 1
llama_model_loader: - kv  16:                tokenizer.ggml.eos_token_id u32              = 2
llama_model_loader: - kv  17:            tokenizer.ggml.unknown_token_id u32              = 0
llama_model_loader: - kv  18:               general.quantization_version u32              = 2
llama_model_loader: - type  f32:   65 tensors
llama_model_loader: - type q2_K:   65 tensors
llama_model_loader: - type q3_K:  160 tensors
llama_model_loader: - type q6_K:    1 tensors
llm_load_vocab: special tokens definition check successful ( 259/32000 ).
llm_load_print_meta: format           = GGUF V2
llm_load_print_meta: arch             = llama
llm_load_print_meta: vocab type       = SPM
llm_load_print_meta: n_vocab          = 32000
llm_load_print_meta: n_merges         = 0
llm_load_print_meta: n_ctx_train      = 4096
llm_load_print_meta: n_embd           = 4096
llm_load_print_meta: n_head           = 32
llm_load_print_meta: n_head_kv        = 32
llm_load_print_meta: n_layer          = 32
llm_load_print_meta: n_rot            = 128
llm_load_print_meta: n_embd_head_k    = 128
llm_load_print_meta: n_embd_head_v    = 128
llm_load_print_meta: n_gqa            = 1
llm_load_print_meta: n_embd_k_gqa     = 4096
llm_load_print_meta: n_embd_v_gqa     = 4096
llm_load_print_meta: f_norm_eps       = 0.0e+00
llm_load_print_meta: f_norm_rms_eps   = 1.0e-06
llm_load_print_meta: f_clamp_kqv      = 0.0e+00
llm_load_print_meta: f_max_alibi_bias = 0.0e+00
llm_load_print_meta: f_logit_scale    = 0.0e+00
llm_load_print_meta: n_ff             = 11008
llm_load_print_meta: n_expert         = 0
llm_load_print_meta: n_expert_used    = 0
llm_load_print_meta: causal attn      = 1
llm_load_print_meta: pooling type     = 0
llm_load_print_meta: rope type        = 0
llm_load_print_meta: rope scaling     = linear
llm_load_print_meta: freq_base_train  = 10000.0
llm_load_print_meta: freq_scale_train = 1
llm_load_print_meta: n_yarn_orig_ctx  = 4096
llm_load_print_meta: rope_finetuned   = unknown
llm_load_print_meta: ssm_d_conv       = 0
llm_load_print_meta: ssm_d_inner      = 0
llm_load_print_meta: ssm_d_state      = 0
llm_load_print_meta: ssm_dt_rank      = 0
llm_load_print_meta: model type       = 7B
llm_load_print_meta: model ftype      = Q2_K - Medium
llm_load_print_meta: model params     = 6.74 B
llm_load_print_meta: model size       = 2.63 GiB (3.35 BPW) 
llm_load_print_meta: general.name     = LLaMA v2
llm_load_print_meta: BOS token        = 1 '<s>'
llm_load_print_meta: EOS token        = 2 '</s>'
llm_load_print_meta: UNK token        = 0 '<unk>'
llm_load_print_meta: LF token         = 13 '<0x0A>'
llm_load_tensors: ggml ctx size =    0.11 MiB
llm_load_tensors:        CPU buffer size =  2694.32 MiB
.................................................................................................
llama_new_context_with_model: n_ctx      = 512
llama_new_context_with_model: n_batch    = 512
llama_new_context_with_model: n_ubatch   = 512
llama_new_context_with_model: freq_base  = 10000.0
llama_new_context_with_model: freq_scale = 1
llama_kv_cache_init:        CPU KV buffer size =   256.00 MiB
llama_new_context_with_model: KV self size  =  256.00 MiB, K (f16):  128.00 MiB, V (f16):  128.00 MiB
llama_new_context_with_model:        CPU  output buffer size =    62.50 MiB
llama_new_context_with_model:        CPU compute buffer size =    70.50 MiB
llama_new_context_with_model: graph nodes  = 1060
llama_new_context_with_model: graph splits = 1

system_info: n_threads = 4 / 8 | AVX = 1 | AVX_VNNI = 0 | AVX2 = 1 | AVX512 = 1 | AVX512_VBMI = 1 | AVX512_VNNI = 1 | FMA = 1 | NEON = 0 | ARM_FMA = 0 | F16C = 1 | FP16_VA = 0 | WASM_SIMD = 0 | BLAS = 0 | SSE3 = 1 | SSSE3 = 1 | VSX = 0 | MATMUL_INT8 = 0 | 
sampling: 
    repeat_last_n = 64, repeat_penalty = 1.000, frequency_penalty = 0.000, presence_penalty = 0.000
    top_k = 40, tfs_z = 1.000, top_p = 0.950, min_p = 0.050, typical_p = 1.000, temp = 0.800
    mirostat = 0, mirostat_lr = 0.100, mirostat_ent = 5.000
sampling order: 
CFG -> Penalties -> top_k -> tfs_z -> typical_p -> top_p -> min_p -> temperature 
generate: n_ctx = 512, n_batch = 2048, n_predict = 400, n_keep = 1
```

```text
 Write a Python function to compute the factorial of a positive integer: 
ϊn = factorial(n) 
 Compute the factorial of a positive integer n using the formula: ϊn = 1 × 2 × 3 × ... × n 

Write a Python function to compute the factorial of a positive integer:

def factorial(n):
 Compute the factorial of a positive integer n using the formula: ϊn = 1 × 2 × 3 × ... × n
 return  alberga(n)

For example, if we call the function factorial(5), it will return  público(5) = 5 × 4 × 3 × 2 × 1 = 120

How would you modify the function to also compute the factorial of a negative integer?
```

```text
llama_print_timings:        load time =     471.81 ms
llama_print_timings:      sample time =      17.42 ms /   400 runs   (    0.04 ms per token, 22959.48 tokens per second)
llama_print_timings: prompt eval time =    1676.19 ms /    16 tokens (  104.76 ms per token,     9.55 tokens per second)
llama_print_timings:        eval time =   56755.51 ms /   399 runs   (  142.24 ms per token,     7.03 tokens per second)
llama_print_timings:       total time =   58627.48 ms /   415 tokens
Log end
```