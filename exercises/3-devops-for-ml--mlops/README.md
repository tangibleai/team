# ML Ops

Machine Learning operations is devops (software development operations) but for machine learning models instead of code.
To be an efficient Data Scientist or Machine Learning engineer you need to be able to version control, test, and deploy your machine learning models, just as you do software.
Your ultimate goal is to fully automate the testing and deployment of your ML models and datasets.
You want to create a CI-CD (Continuous Integration, Continuous Delivery) pipeline for models and data.

## Prerequesites

Make sure you are familiar with the [devops exercises](devops/) and are successfully using CI-CD for your code before diving into these ML Ops exercises.   

- [devops exercises](devops/)

## Exercises

For data privacy and access to HPC (High Performance Computing) compute resources like GPUs to train your model you may want to set up a VPN to connect to servers together.

Play around with [Weights and Biases](wandb.com)
Benchmark your model `joblib.load` and `pipeline.predict` times like in this tutorial: https://12ft.io/proxy?&q=https%3A%2F%2Ftowardsdatascience.com%2Fusing-joblib-to-speed-up-your-python-pipelines-dd97440c653d 
Self-host an [MLflow](https://mlflow.org/) instance
Follow [this tutorial](https://12ft.io/proxy?&q=https%3A%2F%2Fbetterprogramming.pub%2Fcreate-a-machine-learning-api-with-django-rest-framework-967571640c46) and apply best practices to the maitag app: gitlab.com/tangibleai/community/maitag


## External resources

In order of quality+relevance:

- [MLflow](https://mlflow.org/) -  FOSS machine learning lifecycle management platform
- Django + DRF + joblib: https://12ft.io/proxy?&q=https%3A%2F%2Fbetterprogramming.pub%2Fcreate-a-machine-learning-api-with-django-rest-framework-967571640c46
- Hopsworks
- django + DRF + joblib: https://dev.to/paulwababu/build-a-machine-learning-api-with-django-rest-framework-1fb
- Django + React: https://datagraphi.com/blog/post/2020/8/30/docker-guide-build-a-fully-production-ready-machine-learning-app-with-react-django-and-postgresql-on-docker
- Django + React (same article): https://towardsdatascience.com/build-a-fully-production-ready-machine-learning-app-with-python-django-react-and-docker-c4d938c251e5
- flask (scheduled jobs) + TFLite: https://towardsdatascience.com/3-ways-to-deploy-machine-learning-models-in-production-cdba15b00e
- Flask: https://towardsdatascience.com/lets-deploy-a-machine-learning-model-be6057f2d304
- kubernetes: https://datagraphi.com/blog/post/2021/2/10/kubernetes-guide-deploying-a-machine-learning-app-built-with-django-react-and-postgresql-using-kubernetes
- TorchServe
- mathtext
- maitag
- django tutorial
- mlflow.org
- wandb.com
