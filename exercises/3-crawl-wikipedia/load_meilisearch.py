import meilisearch
import dotenv
from pathlib import Path
import pandas as pd
import nlpia2_wikipedia as wiki
from tqdm import tqdm
import time

dotenv.load_dotenv()
CONFIG = dotenv.dotenv_values()

client = meilisearch.Client(
    'https://meilisearch-wi9t.onrender.com:7700',
    CONFIG.get('MASTER_KEY', 'MEILI_MASTER_KEY')
)

fin = next(Path('search/data').glob('*.csv')).open()
titles = pd.read_csv(fin, index_col=0)


# An index is where the documents are stored.
index = client.index('wikipedia')


def normalize_keys(obj):
    obj2 = {str(k).strip().strip('_').strip(): obj[k] for k in obj}
    obj2['id'] = obj2.get('pageid')
    if obj2['id'] is None:
        obj2['id'] = id(obj2)
        print(f'!!!!!!!!!!! WARNING: Created synthetic ID: {obj2["id"]} !!!!!!!!!!!!!')
    obj2['id'] = int(obj2['id'])
    return obj2


if __name__ == '__main__':
    pages = []
    for t in tqdm(titles['0']):
        try:
            page = vars(wiki.page(t, auto_suggest=False, preload=True))
        except wiki.DisambiguationError:
            print(f'skipped {t}')
            page = None
        if page is not None:
            page = normalize_keys(page)
            pages.append(page)
            if len(pages) >= 10:
                index.add_documents(pages)
                pages = []
                time.sleep(2)
            print(page.get('title', '<<<BLANK TITLE>>>'))
            print(page.get('content', '<<<BLANK CONTENT>>>')[:80])
            print()
            time.sleep(.51)
