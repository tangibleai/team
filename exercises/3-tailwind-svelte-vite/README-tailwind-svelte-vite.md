# Tailwind CSS framework

### 1. Find a Django project

Find an existing Django project that you want to improve the UX for.
Here's how I did it for ConvoHub (replace `convohub` with your project):

```bash
$ git clone git@gitlab.com:tangibleai/community/convohub.git
$ cd convohub
```

You should now see your `.git` directory and `manage.py` as well as your django staticfiles directories.

#### _`ls -hal`_
```bash
faceofqary/
.git/
.gitignore
hub/
manage.py
pyproject.toml
README.md
scripts/
static/
staticfiles/
templates/
tests/
```

### 2. Create a vite project named "frontend"

Next, make sure the Node Package Manager (`npm`) is up-to-date and create a frontend project.
It's OK if you already have another frontend project.
For example, the `faceofqary/` project in ConvoHub is a chat widget that uses the Svelte framework.
Choose a javascript framework  that is compatible with the rest of your app.
For the ConvoHub project the `faceofqary` Svelte widgets (`App.js`, `Door.js`, `Room.js`) will eventually be merged into this new `frontend/` project, so the command uses `--template svelte`.
Other options include `vue` and `react`.[^2]

```bash
$ npm install -g npm@latest
$ npm create vite@latest frontend -- --template svelte
```

**IMPORTANT:** IGNORE THE "Done. Now run:" INSTRUCTIONS.

Inside the `frontend` directory you should see your `package.json` that defines all your javascript dependencies, like `vite` and `svelte` (notice that `tailwindcss` is NOT installed yet.

#### _`tree -d -L 2 -I node_modules`_
```bash
$ cd frontend
$ tree -L 2 -I node_modules  # output edited for clarity
.
├── index.html
├── jsconfig.json
├── package.json
├── public
│   └── vite.svg
├── README.md
├── src
│   ├── app.css
│   ├── App.svelte
│   ├── assets
│   ├── lib
│   ├── main.js
│   └── vite-env.d.ts
├── svelte.config.js
└── vite.config.js
```

The `frontend/src/` directory contains a new boilerplate Svelte app.

### 3. Add tailwindcss as a dependency

The `vite.config.js` file is where you will configure your build process by telling vite where to put your javascript and CSS bundles.
The `vite build` command is analogous to the `python manage.py collectstatic` command in Django.
When you are ready to deploy a production bundle you will run both `collectstatic` and `vite build`, but for now, you want to focus on your dev JIT (just in time) `vite` frontend (js & CSS) compiler.
Every time you save a frontend file, vite will automatically compile it and refresh your app in the browser.
But before you launch the dev server with `npm run dev` you need to add `tailwindcss` to your vite JIT compiler.

Do **not** use `npm install tailwindcss` to install TailwindCSS and add it to `package.json`.[^8]
You want to use `svelte-add` rather than directly installing the `tailwindcss` preprocessor and compiler.
Instead use `npx svelte-add@latest tailwindcss` to integrate TailwindCSS into the Svelte build process and JIT compiler.
Svelte-add will create the correct `tailwind.config.cjs` file so you can use Tailwind classes in Svelte files.
The reason you need to use `npx` rather than `npm` is because TailwindCSS uses the `.jsx` language rather than vanilla `.js`.
The JSX (JavaScript + XML) language extends JavaScript to allow HTML (and XML) inside of JavaScript files with the extension `.jsx`.
The `.svelte` syntax goes the other way around, allowing you to put JavaScript code inside of HTML and XML.

The `tailwind.config.cjs` is where you will configure your Tailwind CSS build process and `app.pcss` (notice the `p` in `.pcss`) is where you will define custom CSS styles.[^1]

```bash
$ npx svelte-add@latest tailwindcss
$ npm install
```

Some tutorials use `npm add` with the `--save-dev` (`-D`)[^10] option, but this tells `npm` to save these javascript package names in the "devDependencies" section of your `package.json` rather than your main production build dependencies.
And these tutorials also manually install the `postcss` and `autoprefixer` build-time tools that run in `node`.
But the `npx svelte-add` command does this automatically for you, and it configures them to play nicely with Svelte and Vite.
This ensures that your `tailwind.config.cjs` file has the correct Svelte filename patterns in `tailwind.config.cjs` (NOTE the **`c`** in `.cjs`):

#### _`cat tailwind.config.cjs`_
```js
/** @type {import('tailwindcss').Config}*/
const config = {
  content: ["./src/**/*.{html,js,svelte,ts}"],
  theme: {
    extend: {},
  },
  plugins: [],
};

module.exports = config;
```

### 4. Import tailwind in your `app.css`

Your `src/app.pcss` file (notice the PostCSS extenssion `.pcss`) should now contain both the example Svelte App styles as well as imports for the Tailwind base class names at the top of that file.
The `@tailwind components` and `@tailwind utilities` classes should be at the bottom of that `app.pcss` file.

#### _`cat src/app.pcss`_
```css
/* Write your global styles here, in PostCSS syntax */
@tailwind base;

:root {
  font-family: Inter, system-ui, Avenir, Helvetica, Arial, sans-serif;
  line-height: 1.5;
  font-weight: 400;

// ...
}

@tailwind components;

@tailwind utilities;
```
Your `src/main.js` should already have an app.pcss import at the top of the file:

#### _`cat src/main.js`_
```js
import "./app.pcss";
import App from "./App.svelte";

const app = new App({
  target: document.getElementById("app"),
});

export default app;
```

And your `svelte.config.js` file only needs to include the vite plugin.
Vite will take care of running the TailwindCSS compiler.

#### _`cat svelte.config.js`_
```js
import adapter from '@sveltejs/adapter-auto';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';
/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter()
  },
  preprocess: vitePreprocess()
};
export default config;
```

### 5. Test the svelte app

Before you move on, make sure the Svelte app still works

```bash
# in the convohub/frontend/ directory
$ npm run dev
```

You should see left-justified and resized images plus new fonts in your example `Count.svelte` widget:

<img src="svelte-example-with-tailwind.png" alt="drawing" width="256"/>

### 6. Test the TailwindCSS class names

In your IDE or text editor, add some TailwindCSS class names to your App.svelte file to see if you can improve the style of the Example Vite+Svelte app.
For example, try adding the tailwind class name `"columns-2"` to the div that encloses the two logo `<a href>` tags so that the images appear in two columns side-by-side.

#### _`cat src/App.svelte`_
```html
  <!-- These are NEW TailwindCSS classes!!! Add them one at a time and save to see their effect. -->
  <div class="columns-2 bg-violet-200 border-violet-600 border-4 p-4 m-4 rounded" >  
    <a href="https://vitejs.dev" target="_blank" rel="noreferrer">
      <img src={viteLogo} class="logo vite" alt="Vite Logo" />
    </a>
    <a href="https://svelte.dev" target="_blank" rel="noreferrer">
      <img src={svelteLogo} class="logo svelte" alt="Svelte Logo" />
    </a>
  </div>
```

If your dev server is not running, launch it again.
```bash
$ npm run dev
```

<img src="svelte-example-with-tailwind-border.png" alt="drawing" width="256"/>

Add and remove classes and save with each edit to see if you can learn the effect each one has.
Hint: `p` usually means `pad` and `m` usually means `margin` in Tailwind world.
Find a good TailwindCSS tutorial like [this one](https://tsh.io/blog/tailwind-css-tutorial/) to help you get up to speed on Tailwind's vocabulary and syntax.

## References

For more information check out these tutorials:

[^1]: [Use `npx svelte-add@latest tailwindcss` !!!](https://blog.logrocket.com/how-to-use-tailwind-css-svelte/)
[^2]: [Install Tailwind with Vite](https://tailwindcss.com/docs/guides/vite#svelte)
[^3]: [TailwindCSS + Sveltekit docs (recommended by FreeCodeCamp)](https://tailwindcss.com/docs/guides/sveltekit)
[^10]: [`npm install` documentation (`-D` and `-O` flags)](https://docs.npmjs.com/cli/v10/commands/npm-install)
[^4]: [Install Tailwind with Svelte+Vite](https://dev.to/altierii/how-to-install-tailwind-css-with-svelte-vite-1kj6)
[^5]: [Intro to CSS and the Tailwind CSS framework](https://www.freecodecamp.org/news/what-is-tailwind-css-a-beginners-guide/)
[^6]: [Getting to know the tailwind "language" using Tailwind Play](https://www.codeinwp.com/blog/tailwind-css-tutorial/)
[^7]: [Svelte Vite TailwindCSS setup by Muhammad Dwiky Cahyo Wicaksono](https://medium.com/@mdwikycahyo/how-to-set-up-svelte-using-vite-and-tailwind-css-617040ebccec)
[^8]: [SvelteKit may become just a Vite plugin](https://github.com/svelte-add/svelte-add#-support-for-svelte-with-webpack-or-rollup)
[^9]: [TailwindCSS tutorial](https://tsh.io/blog/tailwind-css-tutorial/)

