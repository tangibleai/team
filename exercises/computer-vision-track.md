# Computer vision track

If you are more interested in computer vision than Natural Language Processing, here's a lesson plan that may work for you.

## Problem statement: Detect checked check-boxes

A friend wanted to build an app to detect the filled in bubbles on a notecard-sized score card.
Others may want to count check boxes on camera photos of completed paper forms.
This is the kind of thing that a scantron machine could do with 99% accuracy back in the 80's, and voting machines are able to do this with virtually 100% accuracy WITHOUT machine learning or computer vision.
But computer vision and machine learning mean you do not need to spend thousands on equiment and you don't need to constrain the form shape and size.
If you need to detect the presence or absence of objects in an image, this tutorial should help.

It will even work for counting flies in real-world photos.
So don't lose hope, even if your objects are numerous and oddly shaped and the background in your image is impossible to control.
A client wanted to count the number of flies in an automated insect protein farm container and this approached worked really well for them.

## Lesson plan

Read the first two paragraphs of #1 on this list, and if any of it doesn't make sense or you can't get the code to work, move on to the next one on this list. Eventually you will reach one you understand, then you can work your way back up to the top. 1. is closest to your problem and after you understand it and reproduce it for yourself you will see how to simplify your labelbox dataset, and turn your task into an object-detection problem that runs directly on your images (not subimages of individual sections of your card). This approach would give you the best possible accuracy with the least amount of labeled data (99% accuracy is not achievable without cheating on the metrics you report to your client by cherrypicking the images users take with their phone):

1. Fine-tuning object detection with PyTorch: https://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
2. Doing inference (object detection on your data without any training) using an existing model : https://pyimagesearch.com/2021/08/02/pytorch-object-detection-with-pre-trained-networks/
3. Transfer learning and embeddings: https://pytorch.org/tutorials/beginner/transfer_learning_tutorial.html
4. Deep academic explanation of transfer learning: https://teaching.pages.centralesupelec.fr/deeplearning-lectures-build/01-pytorch-object-detection.html
5. TangibleAI's Intro to ML (linear regression) on tabular data (scroll to the bottom for 2 "getting started" projects): https://gitlab.com/tangibleai/community/team/-/blob/main/exercises/machine-learning-track.md