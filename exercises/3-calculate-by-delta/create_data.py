import random
from datetime import datetime

import pandas as pd
from faker import Faker

fake = Faker()
Faker.seed(0)

dates = []

user_ids = []
for i in range(99999):
    user_ids.append(fake.ssn())

messages = []

for i in range(1, 199999):
    for j in range(random.randint(1, 100)):
        messages.append(
            {
                "id": i,
                "text": fake.sentence(5),
                "date": fake.date_time_between_dates(
                    datetime_start=datetime(2023, 1, 1, 0, 0, 0), datetime_end=datetime(2023, 6, 1, 23, 59, 59)
                ),
                "user_id": random.choice(user_ids),
            }
        )
    print(i)
df = pd.DataFrame.from_records(messages)
df.to_csv("messages.csv", index=False)
