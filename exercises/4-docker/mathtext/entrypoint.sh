#!/bin/bash

echo "Downloading models"
python -m spacy download en_core_web_md
echo "Finished downloading en_core_web_md for SpaCy"
python -c 'from sentence_transformers import SentenceTransformer; sbert = SentenceTransformer("paraphrase-MiniLM-L6-v2"); print(sbert)'
echo "Finished downloading SBERT for sentence_transformers"

# Dev
#uvicorn app:app --host 0.0.0.0 --port 8080 --reload

# Production
uvicorn app:app --host 0.0.0.0 --port 8080
