# Counting active users

Fourteen days of messages from users a, b, c, d, e, f, g, h, and i:

```python
>>> import pandas as pd
>>> from scipy import sparse
>>> from collections import Counter
>>> messages_per_day = []
... for row in 'aaaabbccc', 'bbbbbbeeeeeee', 'gggggffff', '', 'd', '', 'bbbb', 'ccccffffffff', '', '', '', 'hhhhhh', 'j', 'iiikkkkkk':
...     messages_per_day.append(Counter(row))
... df = pd.DataFrame(messages_per_day).fillna(0).astype(int)
>>> df
    a  b  c  e  g  f  d  h  j  i  k
0   4  2  3  0  0  0  0  0  0  0  0
1   0  6  0  7  0  0  0  0  0  0  0
2   0  0  0  0  5  4  0  0  0  0  0
3   0  0  0  0  0  0  0  0  0  0  0
4   0  0  0  0  0  0  1  0  0  0  0
5   0  0  0  0  0  0  0  0  0  0  0
6   0  4  0  0  0  0  0  0  0  0  0
7   0  0  4  0  0  8  0  0  0  0  0
8   0  0  0  0  0  0  0  0  0  0  0
9   0  0  0  0  0  0  0  0  0  0  0
10  0  0  0  0  0  0  0  0  0  0  0
11  0  0  0  0  0  0  0  6  0  0  0
12  0  0  0  0  0  0  0  0  1  0  0
13  0  0  0  0  0  0  0  0  0  3  6
```

Each row represents a new day, each column represents a user, and the value in a particular cell is the number of messages sent by that user on that day.
Now lets create a sparse matrix so that we can store a matrix containing the messages counts for millions of users for hundreds of days.


```python
>>> spmat = sparse.coo_matrix(df)
>>> spmat
<14x11 sparse matrix of type '<class 'numpy.float64'>'
  with 15 stored elements in COOrdinate format>
```

By storing only the nonzero, nonnan values of the matrix, this sparse matrix only requires RAM for 15 floating point values (along with their integer coordinates).
A dense verson of this matrix would require 10 times as much RAM because it would contain 154 (`14*11`) floating point values.

A nice feature of count matrices is that you can quickly sum the columns (messages per user) or rows (messages per day).
To sum up the message counts for each user use the sum function along axis 0 (columns):

```python
df.sum()
>>> df.sum()
a     4.0
b    12.0
c     7.0
e     7.0
g     5.0
f    12.0
d     1.0
h     6.0
j     1.0
i     3.0
k     6.0
dtype: float64
```

To sum up the messages per day use the sum method along axis 1 (rows):

```python
>>> df.sum(axis=1)
0      9.0
1     13.0
2      9.0
3      0.0
4      1.0
5      0.0
6      4.0
7     12.0
8      0.0
9      0.0
10     0.0
11     6.0
12     1.0
13     9.0
dtype: float64
```

The `.transpose()` method or `.T` property lets you flip the columns and rows around so you can sum along axis 1 (rows) to get the counts per day by first transposing and then summing along axis 0 (the default axis):

```python
>>> df.T.sum()
0      9.0
1     13.0
2      9.0
3      0.0
4      1.0
5      0.0
6      4.0
7     12.0
8      0.0
9      0.0
10     0.0
11     6.0
12     1.0
13     9.0
dtype: float64
```
