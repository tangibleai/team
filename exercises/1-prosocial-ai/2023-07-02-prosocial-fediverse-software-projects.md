## bright pattern resources

- https://cf.farside.link/
- alternative frontends
- librewolf
- lemmy
- mastodon
- matrix
- zulip
- https://github.com/techgaun/active-forks
- https://github.com/rasbt/rubrix (open fork of argilla)
- sourcehut sr.ht (private software repos & projects for hackers)
- https://addons.mozilla.org/en-US/firefox/addon/hypothes-is-bookmarklet/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search
