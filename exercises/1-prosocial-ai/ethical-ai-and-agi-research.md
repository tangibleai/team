# Human level artificial general intelligence research

## AGI Researchers

-   **Ben Goertzel –** [Inference Meta-Learning: The Core of Recursively Self-improving AGI?](https://www.youtube.com/watch?v=kElIY3SM0ZU&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=2) 
-   **Dileep George –** [Building machines that work like the brain](https://www.youtube.com/watch?v=8nDjROcLAh0&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=3)
-   **Brenden Lake –** [Ingredients of Intelligence](https://www.youtube.com/watch?v=9PSQduoSNo0&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=4) 
-   **Vladimir Red’ko –** [Modelling of Cognitive Evolution](https://www.youtube.com/watch?v=iOMUm0rIWmo&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=5)
-   **Hava Siegelmann –** [Lifelong learning machines](https://www.youtube.com/watch?v=NchQH9E1eJc&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=6) 
-   **Paul Smolensky –** [Embedding symbolic computation within neural computation for AI & NLP](https://www.youtube.com/watch?v=yFuEQIeX7VQ&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=7)
  - tags: grounding, knowledge graph, information extraction, knowledge extraction, symbolic reasoning
-   **Tomas Mikolov –** [A Roadmap Towards Machine Intelligence](https://www.youtube.com/watch?v=Rc9e1WWWo5M&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=8)
  - tags: embeddings, knowledge graph, AI, AGI
-   **Thomas Parr –** [Uncertainty and active inference](https://www.youtube.com/watch?v=pHOAg9FZYeA&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=9) 
-   **Ben Goertzel –** [Cognitive Networks of Cognitive Networks](https://www.youtube.com/watch?v=DS4W2ljJybA&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=10)
-   **Tomas Mikolov –** [When Shall We Achieve Human-Level AI?](https://www.youtube.com/watch?v=taj28eiBEfY&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=11)
-   **Irakli Beridze –** [Maximizing the Chances of Global Prosperity](https://www.youtube.com/watch?v=BO9dCSTJjOM&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=12) 
-   **Seán Ó hÉigeartaigh –** [Connecting the near and long-term challenges of Beneficial AI](https://www.youtube.com/watch?v=zXM_bZpUYHY&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=13)
-   **John Langford –** [Interactive Learning in the Real World](https://www.youtube.com/watch?v=GfdZVXvGanU&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=14)
-   **Pavel Kordik –** [AI powered by AI](https://www.youtube.com/watch?v=8ypUY2g4SIo&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=15)
-   **Kenneth Stanley –** [On Creativity, Objectives, and Open-Endedness](https://www.youtube.com/watch?v=y2I4E_UINRo&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=16)
-   **Ryota Kanai –** [Consciousness and AI](https://www.youtube.com/watch?v=NdxUZdIG2Hw&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=17)
-   **Irina Higgins –** [Unsupervised Disentanglement Or How to Transfer Skills & Imagine Things](https://www.youtube.com/watch?v=o8GUrX0IiZg&list=PL9poXcROwgaytoKUkKJ-oOS7Ti9AL-dEb&index=18)

## Roles in FOSS Ethical AI (Ethical AI)

### 2023-07

- PyTorch founder now at Meta?
- x.AI (TruthGPT) - probably not ethical and more Based AI
- Vish and Michael's list of research papers in Grounding and AGI?
- xyntopia.com - FOSS pydoxtools and vector storage database that runs in the browser


## 20190720 AGI Reddit post

Perhaps the op is talking about academic research in a philosophical rather than engineering setting. Because these ideas don't seem new to me. They are widely appreciated and employed in industry:

1. There are many systems that incorprate multisensory, high dimensionality data. For example self driving cars, have audio, visual, radar, vehicle telemetry, and a voice (language) interface.
2. That sounds like hyperparameter turning and transfer learning, which is core to the more advanced approaches for AI and deep learning.
3. Consciousness: No. Very few researchers are distracted by the debate about consciousness.  Researchers are instead focused on giving systems a "theory of mind" that can predict the goals and behaviors of other systems (animals, machines, humans) that it must interact with. Self driving cars have to guess whether you're going to bolt across the street or slam on the brakes, or roll through that 4way stop.
4. This sounds like reinforcement learning, and that is indeed an important part of most attempts at agi.
5. Google Maps and Google search has  emloyed goal-based reasoning very effectively for at least a decade. Most successful dialog engines use goal-based reasoning.
6. Most AI and AGI startups (which is most startups period) are focused on near-term incremental development of a machine learning system with ever more general and broad set of capabilities.
