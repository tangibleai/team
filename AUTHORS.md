# Team

## 2020-2022

[Billy Horn🚀](https://gitlab.com/billy-horn)
[Constance Mlagami](https://gitlab.com/mlagamic)
[Copeland Royall](https://gitlab.com/coayer)
[Dwayne Negron](https://gitlab.com/dwayne.negron1)
[Greg Thompson](https://gitlab.com/thompsgj)
[Hanna Seyoum](https://gitlab.com/hmseyoum)
[Hobson Lane](https://gitlab.com/hobs)
[Jess C.](https://gitlab.com/cassjes.m)
[John May](https://gitlab.com/jmayjr)
[Jon Sundin](https://gitlab.com/jonsundin)
[Kalika Kay Curry](https://gitlab.com/KalikaKay)
[Kevin Sia](https://gitlab.com/kevin.sia)
[Makeda Gebremedhin](https://gitlab.com/makedag)
[Man Wai Yeung🏓](https://gitlab.com/dcvionwinnie)
[Maria Dyshel💬](https://gitlab.com/maria_tangibleai)
[Martha Gavidia](https://gitlab.com/marsgav)
[Maya Hecht](https://gitlab.com/MayaHe)
[Meijke](https://gitlab.com/meiji163)
[Uzo Ikwuakor](https://gitlab.com/Ikwuakor)
[Vishvesh Bhat](https://gitlab.com/vbhat1)
[Wamani Jacob🐒](https://gitlab.com/nicobwan)
[Winston](https://gitlab.com/winstonoakley)
[kazumaendo](https://gitlab.com/kazumaendo)
[Rochdi Khalid👽](https://gitlab.com/rochdikhalid)

## 2023

Cetin Cakir: @cetincakirtr
Chukwudi Ibe: @nitrospawn
Daniel Lantsevich: @digitalbraining
Greg Thompson: [@thompsgj](gitlab.com/thompsgj)
Hobson Lane: [@hobs](gitlab.com/hobs)
John May: [@jmayjr](https://gitlab.com/jmayjr)
Maria Dyshel: [@maria_tangibleai](gitlab.com/maria_tangibleai)
Rochdi Khalid @rochdikhalid
Rumman Rahib @RummanRahib
Sarah Sanger: [@ssanger](gitlab.com/ssanger) 
Sebastian Larson @catasaurus
Vlad Snisar: [@sviddo](gitlab.com/sviddo)
Maria Dyshel @maria_tangibleai

## References

[Black Hat Python](https://www.dropbox.com/s/98zlcm7axe3vhlm/BlackHatPython2E.pdf) by Justin Seitz and Tim Arnold 
[Django Tutorial nvidious videos](https://yewtu.be/playlist?list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p) by Corey Schafer
   339	Hobson Lane <hobs@totalgood.com>
   137	hobs <hobs@totalgood.com>
    88	hobs <dhlane@ucsd.edu>
    81	Sebastian Larson <sebastianlarson22@gmail.com>
    56	rochdikhalid <rochdib.khalid@gmail.com>
    55	Hobson Lane <hobson@tangibleai.com>
    25	Maria Dyshel <maria@WINDOWS-S1ACKUQ.mit.edu>
    17	Maria Dyshel <maria@tangibleai.com>
    16	Uzodinma Ikwuakor <uzo.ikwuakor@gmail.com>
    15	Vishvesh Bhat <vbhat@ucsd.edu>
    14	Aditi Maheshwari <aditimaheshwari1999@gmail.com>
    14	Greg Thompson <thompsgj@yahoo.com>
    14	Vlad Snisar <snisarvlad04@gmail.com>
    12	John May <jmay526@gmail.com>
     8	IzicTemi <iomolayo2541@gmail.com>
     8	sviddo <snisarvlad04@gmail.com>
     8	unachka <una.bayasgalan@gmail.com>
     7	Dwayne Negron <dwayne.negron1@gmail.com>
     7	Rochdi Khalid <rochdib.khalid@gmail.com>
     6	Ashyrgeldi Atayev <asyrgeldiatayev@gmail.com>
     6	Billy Horn <billyjhorn@gmail.com>
     6	Hobson Lane <hobs.git@totalgood.com>
     5	Cetin CAKIR <cetincakirtr@gmail.com>
     5	Constance Mlagami <cmlagami@gmail.com>
     4	Jon Sundin <sundinjon@gmail.com>
     4	Martha Gavidia <gavidiams@gmail.com>
     3	Ayobami <froshofficial@gmail.com>
     3	Kalika Kay Curry <kalika_kay@hotmail.com>
     3	KazumaE <kazuen926@gmail.com>
     3	Una Bayasgalan <una.bayasgalan@gmail.com>
     2	Camille Dunning <dunningcamille@gmail.com>
     2	Cetin Cakir <cetincakirtr@gmail.com>
     2	Hanna Seyoum <hmseyoum@gmail.com>
     2	Josh Johnson <caellwyn@gmail.com>
     2	Winston <winston.oakley@gmail.com>
     2	dwayne.negron1 <dwayne.negron1@gmail.com>
     1	Bete Fulle <bete3738@gmail.com>
     1	Desiree Junfijiah <desjunfijiah1@gmail.com>
     1	Hobson Lane <hobs+gitlab@totalgood.com>
     1	Isaac Omolayo <iomolayo2541@gmail.com>
     1	John  May <jmay526@gmail.com>
     1	Kevin Sia <kevinsia92@gmail.com>
     1	Makeda Gebremedhin <makedag@berkeley.edu>
     1	Maya Hecht <mayahcht@gmail.com>
     1	Sarah Sanger <ssanger2020@gmail.com>
     1	billy-horn <billyjhorn@gmail.com>
     1	constancemlagami <cmlagami@gmail.com>
     1	copeland <copeyroyall@mac.com>
     1	diana oks <sheanan.oks@gmail.com>
     1	jakobwamani <nicobwan@gmail.com>
     1	marsgav <gavidiams@gmail.com>
